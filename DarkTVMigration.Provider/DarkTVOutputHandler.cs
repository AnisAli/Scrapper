﻿using CsvHelper;
using CsvHelper.Configuration;
using Serilog;
using SimpleWebScrapper.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkTVMigration.Provider.Configuration;
using DarkTVMigration.DataModel;

namespace DarkTVMigration.Provider
{
    class DarkTVOutputHandler : IEngineOutputHandler
    {
        private readonly string _sucessFile;
        private readonly string _failureFile;
        private readonly ILogger _logger;
        private string _oraConnection;

        #region ctor
        public DarkTVOutputHandler(ILogger logger)
        {
            _logger = logger;
            var config = ConfigurationManager.GetSection("darkTVProviderSetting") as ProviderConfigurationSection;
            if (config != null)
            {

                _oraConnection = config.OutputSetting.ConnectionSetting;
                var uname = config.OutputSetting.Username;
                var pass = config.OutputSetting.Password;

                //open oracle 

                //if (config.OutputSetting.DateFileFormat != null)
                //{

                //    var format = DateTime.Now.ToString(config.OutputSetting.DateFileFormat);

                //    _sucessFile = config.OutputSetting.SuccessFilePath + "success_" + format +
                //                 ".txt";
                //    _failureFile = config.OutputSetting.FailureFilePath + "failure" + format +
                //                  ".txt";
                //}
                //else
                //{
                //    _sucessFile = config.OutputSetting.SuccessFilePath + "success.txt";
                //    _failureFile = config.OutputSetting.FailureFilePath + "failure.txt";
                //}

            }
            else
            {
                throw new Exception("Provider Setting is missing , Please add setting in AppSetting for section 'pbw28ProviderSetting'");
            }

        }
        #endregion

        public bool Sucess(Customer customer)
        {
             var oracle = OracleDb.GetInstance(_oraConnection);
             return oracle.UpdateInformation(customer, "SUCCESS", "");
          //  return true;
        }

        public bool Sucess(Customer customer, string note)
        {
              var oracle = OracleDb.GetInstance(_oraConnection);
             return oracle.UpdateInformation(customer, "SUCCESS", note);
           // return true;
        }


        public bool Failure(Customer customer, string reason)
        {
            //return true;
              var oracle = OracleDb.GetInstance(_oraConnection);
             return oracle.UpdateInformation(customer, "ERROR", reason);

        }

        public bool Failure(Customer customer, Exception exception)
        {
            //return true;
             var oracle = OracleDb.GetInstance(_oraConnection);
               return oracle.UpdateInformation(customer, "ERROR", exception.Message);
        }

        public List<Customer> GetSuccessCustomerList()
        {
            return ReadCustomerList(_sucessFile);
        }

        public List<Customer> GetFailureCustomerList()
        {
            return ReadCustomerList(_failureFile);
        }

        #region private methods
        private List<Customer> ReadCustomerList(string fileName)
        {
            try
            {

                if (!File.Exists(fileName))
                    return null;

                var customerList = new List<Customer>();
                using (var fs = File.OpenRead(fileName))
                using (var reader = new StreamReader(fs))
                {

                    CsvConfiguration config = new CsvConfiguration { HasHeaderRecord = false };
                    var csv = new CsvReader(reader, config);
                    while (csv.Read())
                    {
                        var customerId = csv.GetField<string>(0); //cusotmerId
                        var tempCust = new Customer
                        {
                            CustomerB1Number = customerId.Trim()
                        };
                        customerList.Add(tempCust);
                    }
                }
                return customerList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Load Input File Error {0}", fileName);
                throw new Exception("OutputHandler - Read Error");
            }
        }

        public bool LogHtmlFile(Customer customer, string HtmlPage)
        {

            var dateformat = DateTime.Now.ToString("mmddyyyy");
            string filename =  customer.CustomerB1Number + "_" + customer.scenerioType +"_"+ dateformat +  ".html";

            try
            {
                using (var txtWriter = new StreamWriter(filename))
                {
                    txtWriter.WriteLine(HtmlPage);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Not able to write Html Log - " + ex.Message);

            }
        }


        #endregion
    }
}


