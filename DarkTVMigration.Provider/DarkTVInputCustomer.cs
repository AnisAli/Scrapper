﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleWebScrapper.Common;
using System.Configuration;
using DarkTVMigration.Provider.Configuration;
using Serilog;
using DarkTVMigration.DataModel;

namespace DarkTVMigration.Provider
{
    class DarkTVInputCustomer : CustomerRepository
    {

        private  string _oraConnection ;
        private string _username;
        private string _password;
    

        public DarkTVInputCustomer(ILogger logger) : base(logger)
        {
            var config = ConfigurationManager.GetSection("darkTVProviderSetting") as ProviderConfigurationSection;
            if (config != null)
            {
                _oraConnection = config.RepositorySetting.OracleConnection;
                _username = config.RepositorySetting.UserName;
                _password = config.RepositorySetting.Password;
            }
            else
            {
                throw new Exception("Provider Setting is missing , Please add setting in AppSetting for section 'pbw28ProviderSetting'");
            }
            //  _logger = logger;
            Initialize();


        }


        public override bool IsAllowToProcess(Customer cust)
        {
            try
            {
                var oracle = OracleDb.GetInstance(_oraConnection);
                return oracle.IsAllowToContinue(cust);
            }
            catch (Exception ex)
            {
                _logger.Error("Oracle Not Connected");
                return true;
            }
        }

        public override bool ReadyToRead(Customer cust)
        {
            try
            {
                var oracle = OracleDb.GetInstance(_oraConnection);
                return oracle.MarkReadFlag(cust);
            }
            catch (Exception ex)
            {
                _logger.Error("Oracle Not Connected");
                return true;
            }
        }

        public List<Customer> CustomerList;

        public override List<Customer> Customers => CustomerList;

        public override void Initialize()
        {

           

            //this.CustomerList = new List<Customer>();
            //this.CustomerList.Add(new Customer() { CustomerB1Number = "b1nenr95)", scenerioType = 2 });
            //this.CustomerList.Add(new Customer() { CustomerB1Number = "b1awts57", scenerioType = 1 });

            try
            {
                var oracle = OracleDb.GetInstance(_oraConnection);
                this.CustomerList = oracle.GetAllCustomer();
            }
            catch (Exception ex)
            {
                _logger.Error("Oracle Not Connected");
                this.CustomerList = new List<Customer>();
                this.CustomerList.Add(new Customer() { CustomerB1Number = "b1ajut65", scenerioType = 2 });
            }

        }




    }
}
