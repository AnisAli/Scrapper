﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;
using SimpleWebScrapper.Common;

namespace SimpleWebScrapper.Common
{
   public abstract class CustomerRepository : ICustomerRepository
   {
       protected ILogger _logger;

       protected CustomerRepository(ILogger logger)
       {
           _logger = logger;
       }

        public abstract List<Customer> Customers { get; }
        public abstract void Initialize();

        public virtual bool IsAllowToProcess(Customer cust)
        {
            return true;
        }

        public virtual bool ReadyToRead(Customer cust)
        {
            return true;
        }
    }
}
