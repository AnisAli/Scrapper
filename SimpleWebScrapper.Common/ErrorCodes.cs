﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleWebScrapper.Common

{
   public static class ErrorCodes
   {
       public static int NoError = 0;

       public static int CustomerNotFound = -1;

        public static int SessionExpired = -2;

        public static int UnExpectedPage = -3;

        public static int ProductNotFound = -4;

        public static int EditisDisabled = -5;

        public static int InvalidScenerioType = -6;

        public static int InvalidPriceStructure = -7;

        public static int SpeedProductChangeNotAllowed = -8;

        public static int EngineExpectedError = -500;


  
    }
}
