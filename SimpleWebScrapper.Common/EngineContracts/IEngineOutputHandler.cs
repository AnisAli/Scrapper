﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleWebScrapper.Common
{
    public interface IEngineOutputHandler
    {
        bool Sucess(Customer customer);
        bool Sucess(Customer customer,string note);
        bool Failure(Customer customer, string reason);
        bool Failure(Customer customer, Exception exception);
        bool LogHtmlFile(Customer customer, string HtmlPage);
        List<Customer> GetSuccessCustomerList();
        List<Customer> GetFailureCustomerList();
    }
}
