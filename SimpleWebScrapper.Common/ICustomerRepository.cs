﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;

namespace SimpleWebScrapper.Common
{
    public interface ICustomerRepository
    {
        List<Customer> Customers { get; }

        bool ReadyToRead(Customer cust);

        bool IsAllowToProcess(Customer cust);

        void Initialize();

    }
}
