﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleWebScrapper.Engine;
using SimpleWebScrapper.Engine.Contracts;

namespace SimpleVasTest
{

    [TestClass]
    public class HtmlPageParserTest
    {
       

        [TestMethod]
        public void SearchNotFoundParsing()
        {
           // Ilogger
            SimpleBellPageParser bellPageParser = new SimpleBellPageParser(null);
            var result = bellPageParser.IsCustomerFound_SearhPageParse(HtmlFileReader.SearchNotFoundPage());
            Assert.IsFalse(result);
        }
        [TestMethod]
        public void SearchFoundParsing()
        {
            // Ilogger
            SimpleBellPageParser bellPageParser = new SimpleBellPageParser(null);
            var result = bellPageParser.IsCustomerFound_SearhPageParse(HtmlFileReader.SearchFoundPage());
            Assert.IsTrue(result);
        }
        [TestMethod]
        public void GetCustomerIdentifier()
        {
            // Ilogger
            SimpleBellPageParser bellPageParser = new SimpleBellPageParser(null);
            var result = bellPageParser.ParseCustomerInfoPage(HtmlFileReader.CustomerViewSearchResult());
            Assert.AreEqual("HISHAM BARAKAT (b1nhfq77)", result.CustomerIdentifier);
        }
        [TestMethod]
        public void GetUnlimitedUsageDetails()
        {
            // Ilogger
            SimpleBellPageParser bellPageParser = new SimpleBellPageParser(null);
            var result = bellPageParser.ProductDetailParser.ParseUnlimitedProductDetails(HtmlFileReader.ProductSubMenu_UnlimiteddUsage());
            Assert.AreEqual("PBW28", result.PromotionCode);
        }
        [TestMethod]
        public void GetProductUnlimitedProductServices()
        {
            // Ilogger
            SimpleBellPageParser bellPageParser = new SimpleBellPageParser(null);
            var result = bellPageParser.ParseProductPage(HtmlFileReader.ProductViewPage());
            if (result.Any(product => product.ProductName == "Unlimited Usage Plan" && product.ProductUrl != null))
            {
                Assert.IsTrue(true);
                return;
            }
            Assert.IsTrue(false);
        }

        [TestMethod]
        public void IsEditButtonEnable()
        {
            // Ilogger
            SimpleBellPageParser bellPageParser = new SimpleBellPageParser(null);
            var result = bellPageParser._valueAddedServiceParser.EditButtonEnable(HtmlFileReader.ValueEditPageWithEditEnable());
            Assert.IsTrue(result);
        }


        [TestMethod]
        public void UnlimitedPlanUnchecked()
        {
            // Ilogger
            SimpleBellPageParser bellPageParser = new SimpleBellPageParser(null);
            var result = bellPageParser._valueEditPageParser.IsUnlimitedUsagePlanExist(HtmlFileReader.UnlimitedPlanUncheked());
            Assert.IsFalse(result);
        }

    }
}
