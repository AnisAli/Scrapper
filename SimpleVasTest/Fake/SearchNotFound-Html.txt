﻿
<!-- Application: Pegasus -->
<!-- Page: accounts_SearchResults -->
<!-- Generated: Fri Jul 14 11:13:43 EDT 2017 -->
<html lang='en' xml:lang='en' xmlns='http://www.w3.org/1999/xhtml'>
<head>
<meta name='generator' content='Tapestry Application Framework, version 4.1.6-SNAPSHOT' />
<title>Welcome to SIMPLe</title>
<META Http-Equiv='X-UA-Compatible' Content='IE=EmulateIE7' />
<META Http-Equiv='author' Content='Tim Chen' />
<script type='text/javascript'>djConfig = {'baseRelativePath':'/pegasus?service=asset&path=%2Fdojo-0.4.3-custom-4.1.6%2F','preventBackButtonFix':false,'parseWidgets':false,'locale':'en'} </script>

<script type='text/javascript' src='/pegasus?service=asset&path=%2Fdojo-0.4.3-custom-4.1.6%2Fdojo.js'></script><script type='text/javascript' src='/pegasus?service=asset&path=%2Fdojo-0.4.3-custom-4.1.6%2Fdojo2.js'></script>
<script type='text/javascript'>
dojo.registerModulePath('tapestry', '/pegasus?service=asset&path=%2Ftapestry-4.1.6%2F');
</script>
<script type='text/javascript' src='/pegasus?service=asset&path=%2Ftapestry-4.1.6%2Fcore.js'></script>
<script type='text/javascript'>
dojo.require('tapestry.namespace');
tapestry.requestEncoding='UTF-8';
</script>
</head>
<head>

    <link href='./css/layout.css' type='text/css' rel='stylesheet' id='cssLinkTag'></link>	
    <link href='./css/font_level0.css' type='text/css' rel='stylesheet' id='cssFontSizeLinkTag'></link>
    <link href='./css/jquery-ui-1.8.11.custom.css' type='text/css' rel='stylesheet' id='cssJQueryEffection'></link>
    <META Name='SIMPLe-Server-Name' Content='SIMPLe-P3B1' id='serverNameMetaTag'></META>
	<META Http-Equiv='Cache-Control' Content='no-cache'>
	<META Http-Equiv='Pragma' Content='no-cache'>
	<META Http-Equiv='Expires' Content='0'>
	<META Http-Equiv='Content-Type' content='text/html;charset=iso-8859-1'>	
		
	<script language='javascript' src='javascript/common.js' type='text/javascript'/>
	<script language='javascript' type='text/javascript'>
		function confirmMsg(msg){
			return confirm(msg);
		}

		function isHidden(id){
			if(document.getElementById(id) == null ||
				document.getElementById(id).className == 'hidden'){
		
				return true;
			}
			return false;
		}
	</script>
	<!-- 29.08.2006, KYLE: modifications to this snippet will have to be synced with PageLink / DirectLink / LinkSubmit updated Tapestry components.  -->
	<script language='javascript' type='text/javascript'>
		var pleaseWaitCounter = 0;
		var prefixMessage = 'Please wait';
		var stopPleaseWait = false;
		
		attachOnloadEvent(disableBackButton);
		
		function disableBackButton(){
			if (getCookie('userName')== null){
				if(typeof(history.forward()) != 'undefined'){
					location.replace(history.forward());		
				}
			}
		}	
		
		function displayPleaseWait(){
			var suffixMessage = '';
			
			if(pleaseWaitCounter > 5) pleaseWaitCounter = 0;
			
			for(var i = 0; i < pleaseWaitCounter; i++){
				suffixMessage += ' . ';
			}
			
			document.getElementById('pleaseWaitMessage').innerHTML = prefixMessage + suffixMessage;
			
			pleaseWaitCounter++;
			if (stopPleaseWait) {
				stopPleaseWait = false;
				return true;
			}
			triggerPleaseWait();
			return true;
		}
		function triggerPleaseWait(){
			setTimeout('displayPleaseWait()', 500);
		}
			
	</script>
	<script language='javascript' src='javascript/jquery-1.5.1.js' type='text/javascript' ></script>
	
	<!-- BELLSYMPATICOSIMPLE-715: Using this div was the only way to force an  -->
	<!-- alignment on the 3rd party applet, to prevent it from being easily visible -->
	<!--div style='position: absolute; top: 0px; left: 0px;'>
	<script language='javascript'>
		var jqueryUrl = '<span jwcid='pcidtsJQUERYURL'></span>';
		document.write('<script src='' + jqueryUrl + ''><\/script>');
		var jsonUrl = '<span jwcid='pcidtsJSONURL'></span>';
		document.write('<script src='' + jsonUrl + ''><\/script>');
		var datetime = new Date();
		var pluginUrl = '<span jwcid='pcidtsPluginURL'></span>';
		document.write('<script src='' + pluginUrl +'?datetime.getTime()'><\/script>');
	</script>
	</div-->
	
	<!--script src='javascript/PCIDTSApi.js'></script-->
	
	<script type='text/javascript'>
		var keepAliveInterval = parseInt('600000');
		var keepAliveTimeout = parseInt('3600000');
	</script>
	<script type='text/javascript' src='javascript/idle-timer.js'></script>
	<script type='text/javascript' src='javascript/keepalive.js'></script>
</head>


<body id='Body'>
<script type='text/javascript'><!--
dojo.require('tapestry.event');
// --></script>

  	<script src='javascript/ui/jquery.effects.core.js'></script>
  	<script src='javascript/ui/jquery.effects.bounce.js'></script> 
	<script src='javascript/ui/jquery.effects.explode.js'></script>
	<script src='javascript/ui/jquery.effects.scale.js'></script>
	<script src='javascript/ui/jquery.effects.slide.js'></script>
	<script src='javascript/ui/jquery.effects.drop.js'></script>
	<script src='javascript/ui/jquery.effects.fold.js'></script>
	<script src='javascript/ui/jquery.effects.highlight.js'></script>
	<script src='javascript/ui/jquery.effects.shake.js'></script>
	<script src='javascript/ui/jquery.effects.fade.js'></script>
	
	<script language='javascript' type='text/javascript'>
		function trigger(value){		
			return;
		}
		
		function clearPleaseWaitMessage() {
			document.getElementById('pleaseWaitMessage').innerHTML = '';
		}
		
		dojo.event.topic.subscribe('font', function(msg) {
		 	stopPleaseWait = true; 
		 	setTimeout('clearPleaseWaitMessage()', 500);
		 });
		
	</script>

<div id='header'>
	<div id='navLevel1'>
		<ul>
			<li><a id='languageLink' onMouseUp='javascript:displayPleaseWait();' href='/pegasus?component=%24DefaultLayout.languageLink&amp;page=accounts_SearchResults&amp;service=direct&amp;session=T'>Francais</a></li>
			<li><a id='linkToHome' onClick='javascript:displayPleaseWait();' href='/Home.html'>Home</a></li>
			<li></li>
			<li><a id='linkToPrefsHome' onClick='javascript:displayPleaseWait();' href='/prefs_Home.html'>Preferences</a></li>
			<li><a id='logout' onMouseUp='javascript:displayPleaseWait();' onClick='location.replace(this.href);return false;' href='/pegasus?component=%24DefaultLayout.logout&amp;page=accounts_SearchResults&amp;service=direct&amp;session=T'>Logout</a></li>
		</ul>
	</div>

	<div  style='display: none;'>
		<img id='image_alive' width='1' height='1' src='images/blank.gif?' alt=''/>
	</div>

	<span id='allowStyleChange'>
		<div id='fontSizeSelection'>
			<a id='decreaseFont' onclick='javascript:trigger('0'); displayPleaseWait();' class='smaller' href='#'></a>
			<a id='resetFont' onclick='javascript:trigger('1'); displayPleaseWait();' class='reset' href='#'></a>
			<a id='increaseFont' onclick='javascript:trigger('2'); displayPleaseWait();' class='larger' href='#'></a>
		</div>
	</span>
	<div id='pleaseWaitMessage'></div>
	
	<div id='isHomeSection'>
		<div id='navLevel2'>
			<ul>
				<li class='active' id='manageAccounts'>
					<a id='linkToManageAccounts' onClick='javascript:displayPleaseWait();' href='/accounts_LandingPage.html'>Manage Accounts</a>
				</li>
				<li class='' id='resources'>
					<a id='linkToManageOrders' onClick='javascript:displayPleaseWait();' href='/queues_LandingPage.html'>Manage Orders</a>
				</li>
	
				<li class='' id='createAccount'>
					<a id='linkToCreateAccount' onClick='javascript:displayPleaseWait();' href='/newOrder_LandingPage.html'>Create Account</a>
				</li>
				<li class='' id='manageParentAccounts'>
					
				</li>			
			</ul>
		</div>
	</div>
	
	

	
</div>

<div id='mainContent' class='simple-en'>
	   
		<form method='post' action='/pegasus' id='searchForm'>
<div style='display:none;' id='searchFormhidden'><input type='hidden' name='formids' value='pcidtsResultCodeAccntSrch,pcidtsTokenAccntSrch,pcidtsLogNoteAccntSrch,hasMessages_0,hasMessages_2,criteriaSelect,searchField,searchLink,showBackButton_0' />
<input type='hidden' name='seedids' value='ZH4sIAAAAAAAAAF1RUW7CMAxFSLsJn3xwg4mxoQ3BxlYuYKWvJSJxqiRlKmfa1XaHOQ10wE9iP9vvPSc/v6OH02g0no8fpwH+qBUms2lDdbqUs41jcOxjjqQZXuKAELTjFDVyPKOi1sQ1da5NrWEPY9JICGvNhx3VOVkKRaFPuAFXny1891JVUPHMKTbg38lig0i58QA0ZPQRbxylTOYa22mLrPzkyk4uQ1y3skISSmnSc6/OYki2HlW4IK7O02SM+y5iZ7DYC0OqlVAeFJCsS+oREM+x5puS7umKYQtLLA7mSrmWYxiEN/ewULrWK9y3fPgSPoFJJV4mhqbFHZrltuTlt67YdZiXVvO/LbGpyxI3SP8Y1841L2Sp/O9fYDFyfljKzAXIq33/VSlYOm8nsz+H2GVlSgIAAA==' />
<input type='hidden' name='component' value='accountSearch.searchForm' />
<input type='hidden' name='page' value='accounts_SearchResults' />
<input type='hidden' name='service' value='direct' />
<input type='hidden' name='session' value='T' />
<input type='hidden' name='submitmode' value='' />
<input type='hidden' name='submitname' value='' />
<input type='hidden' name='pcidtsResultCodeAccntSrch' id='pcidtsResultCodeAccntSrch' value='empty' />
<input type='hidden' name='pcidtsTokenAccntSrch' id='pcidtsTokenAccntSrch' value='' />
<input type='hidden' name='pcidtsLogNoteAccntSrch' id='pcidtsLogNoteAccntSrch' value='' />
<input type='hidden' name='hasMessages_0' value='F' />
<input type='hidden' name='hasMessages_2' value='F' />
<input type='hidden' name='showBackButton_0' value='F' />
</div>
	
	
	
	

	

	<script language='javascript' src='javascript/jquery-1.5.1.js' type='text/javascript' ></script>
	
	<!-- BELLSYMPATICOSIMPLE-715: Using this div was the only way to force an  -->
	<!-- alignment on the 3rd party applet, to prevent it from being easily visible -->
	<div style='position: absolute; top: 0px; left: 0px;'>
	<script language='javascript'>
		var jqueryUrl = 'https://pcidts.int.bell.ca/dtsApp/dtsOnline/scripts/jquery-1.6.2.min.js';
		document.write('<script src='' + jqueryUrl + ''><\/script>');
		var jsonUrl = 'https://pcidts.int.bell.ca/dtsApp/dtsOnline/scripts/jquery.jsonp-2.1.4.min.js';
		document.write('<script src='' + jsonUrl + ''><\/script>');
		var datetime = new Date();
		var pluginUrl = 'https://pcidts.int.bell.ca/dtsApp/dtsOnline/scripts/dtsPluginEnc_v2.js';
		document.write('<script src='' + pluginUrl +'?datetime.getTime()'><\/script>');
	</script>
	</div>
	
	<script src='javascript/PCIDTSApi.js'></script>
	
	<table id='searchBox'>
		<tr>
			<td class='inlineTitle1'>Search for</td>
			<td>
				<select name='criteriaSelect' id='criteriaSelect' onchange='refreshForm('searchForm')'>
<option value='0' selected='selected'>B1 Number</option>
<option value='1'>Order Number</option>
<option value='2'>Service Phone</option>
<option value='3'>Name</option>
<option value='4'>Billing Account Number(BAN)</option>
<option value='5'>Email</option>
<option value='6'>Credit Card</option>
<option value='7'>PAP Number</option>
<option value='8'>Equipment Serial Number</option>
<option value='9'>Group Key</option>
<option value='10'>MAC Address</option>
<option value='11'>HSE Order Number</option>
</select>
			</td>
			<td><input type='text' name='searchField' value='b1rtzs20' id='searchField' /></td>
			<td>
				<ul class='actionButtons'>
					<li><a href='javascript:displayPleaseWait(); tapestry.form.submit('searchForm', 'searchLink');' id='searchLink' onClick='return checkTokenize('pcidtsLogNoteAccntSrch', 'pcidtsConsumerID', 'pcidtsApplicationID', 'pcidtsSystemTransactionID', 'pcidtsPassKey', 'pcidtsTimeout', 'searchField', 'searchForm', 'pcidtsResultCodeAccntSrch', 'pcidtsTokenAccntSrch', 'criteriaSelect');'>Search</a></li>
					<!-- <li><a href='' jwcid='searchLink'><span key='COMPONENTS-ACCOUNTS-ACCOUNTSEARCH-SEARCH'>Search</span></a></li> -->
					
					<span>
						<li><a id='clearLink' onClick='javascript:displayPleaseWait();' href='/accounts_LandingPage.html'>Clear</a></li>
					</span>
					<li><a id='advancedSearchLink' onClick='javascript:displayPleaseWait();' href='/accounts_AdvancedSearchResults.html'>Advanced</a></li>				
				</ul>
				<span class='pcidtshiddenhtml'>
					<span id='pcidtsConsumerID'>simple</span>
					<span id='pcidtsApplicationID'>simple</span>
					<span id='pcidtsTimeout'>9000000</span>
					<span id='pcidtsPassKey'>B3Om4RglIjsn34V/wtXwM09ZPkh69wHuu3UGys8YzWxpv1aajnmpQshBufea5K6f</span>
					<span id='pcidtsSystemTransactionID'>20170714151343adad6697</span>
				</span>
			</td>
		</tr>
	</table>

</form>

       
    <div class='borderedContent'>
		<h1 class='firstHeader'>Search Results</h1>
    	    
    	<table cellpadding='3' cellspacing='0' class='listing'>
			<tr>
				<th nowrap='nowrap' width='15%'><a id='userIdHeaderLink' onMouseUp='javascript:displayPleaseWait();' href='/pegasus?component=userIdHeaderLink&amp;page=accounts_SearchResults&amp;service=direct&amp;session=T&amp;sp=ZH4sIAAAAAAAAAFvzloG1rojBLzk%2FVy8pNSdHryA1PbG4tFgPKFCQWJRYkl9UrOdcWlySn5ta5JmXlu8MF1fBLhxSWZDKAAVCDAwVRQx8WYlliXo5iXnpeq55pbnIkgUlDEyeLgAYvXKYhQAAAA%3D%3D'>User Id</a></th>
				<th nowrap='nowrap' width='20%'><a id='firstNameHeaderLink' onMouseUp='javascript:displayPleaseWait();' href='/pegasus?component=firstNameHeaderLink&amp;page=accounts_SearchResults&amp;service=direct&amp;session=T&amp;sp=ZH4sIAAAAAAAAAFvzloG1rojBLzk%2FVy8pNSdHryA1PbG4tFgPKFCQWJRYkl9UrOdcWlySn5ta5JmXlu8MF1fBLhxSWZDKAAVCDAwVRQx8WYlliXo5iXnpeq55pbnIkgUlDFxunkHBIfF%2Bjr6uANcfVfeNAAAA'>First Name</a></th>
				<th nowrap='nowrap' width='20%'><a id='lastNameHeaderLink' onMouseUp='javascript:displayPleaseWait();' href='/pegasus?component=lastNameHeaderLink&amp;page=accounts_SearchResults&amp;service=direct&amp;session=T&amp;sp=ZH4sIAAAAAAAAAFvzloG1rojBLzk%2FVy8pNSdHryA1PbG4tFgPKFCQWJRYkl9UrOdcWlySn5ta5JmXlu8MF1fBLhxSWZDKAAVCDAwVRQx8WYlliXo5iXnpeq55pbnIkgUlDJw%2BjsEh8X6Ovq4AO2rrGIwAAAA%3D'>Last Name</a></th>
				<th nowrap='nowrap' width='15%'><a id='parentHeaderLink' onMouseUp='javascript:displayPleaseWait();' href='/pegasus?component=parentHeaderLink&amp;page=accounts_SearchResults&amp;service=direct&amp;session=T&amp;sp=ZH4sIAAAAAAAAAFvzloG1rojBLzk%2FVy8pNSdHryA1PbG4tFgPKFCQWJRYkl9UrOdcWlySn5ta5JmXlu8MF1fBLhxSWZDKAAVCDAwVRQx8WYlliXo5iXnpeq55pbnIkgUlDJwBjkGufiHxni4A9XMVW4wAAAA%3D'>Is a Parent</a></th>
				<th nowrap='nowrap' width='15%'>Parent Account </th>	
				<th nowrap='nowrap' width='15%'><a id='statusHeaderLink' onMouseUp='javascript:displayPleaseWait();' href='/pegasus?component=statusHeaderLink&amp;page=accounts_SearchResults&amp;service=direct&amp;session=T&amp;sp=ZH4sIAAAAAAAAAFvzloG1rojBLzk%2FVy8pNSdHryA1PbG4tFgPKFCQWJRYkl9UrOdcWlySn5ta5JmXlu8MF1fBLhxSWZDKAAVCDAwVRQx8WYlliXo5iXnpeq55pbnIkgUlDGzBIY4hocEAKk%2Fj0YkAAAA%3D'>Status</a></th>	
				
			</tr>
			<span id='hasCustomers'>
				<span id='noRecordFound'>
					<tr>
						<td colspan='6'>No records found.</td>
					</tr>		
				</span>
				
				
			</span>				
			
		</table>
    </div>

</div>

<p align='left'>
	

</p>





<script type='text/javascript'><!--
tapestry.addOnLoad(function(e) {
tapestry.cleanConnect('decreaseFont', 'trigger', 'event697358517');
                tapestry.event697358517=function( event ){
                    
                    var content={beventname:'trigger', bcomponentidpath:'accounts_SearchResults/$DefaultLayout.decreaseFont', bcomponentid:'decreaseFont'};
                   
                    tapestry.event.buildEventProperties( event, content, arguments);
                    if (!content['beventtarget.id']) {
                    	content['beventtarget.id']='decreaseFont';
                    }                                 
                    
                    tapestry.bind('/pegasus?component=%24DefaultLayout.decreaseFont&page=accounts_SearchResults&service=directevent&session=T', content);
                };
                tapestry.connect('decreaseFont', 'trigger', 'event697358517');
tapestry.cleanConnect('resetFont', 'trigger', 'event431250171');
                tapestry.event431250171=function( event ){
                    
                    var content={beventname:'trigger', bcomponentidpath:'accounts_SearchResults/$DefaultLayout.resetFont', bcomponentid:'resetFont'};
                   
                    tapestry.event.buildEventProperties( event, content, arguments);
                    if (!content['beventtarget.id']) {
                    	content['beventtarget.id']='resetFont';
                    }                                 
                    
                    tapestry.bind('/pegasus?component=%24DefaultLayout.resetFont&page=accounts_SearchResults&service=directevent&session=T', content);
                };
                tapestry.connect('resetFont', 'trigger', 'event431250171');
tapestry.cleanConnect('increaseFont', 'trigger', 'event1755160906');
                tapestry.event1755160906=function( event ){
                    
                    var content={beventname:'trigger', bcomponentidpath:'accounts_SearchResults/$DefaultLayout.increaseFont', bcomponentid:'increaseFont'};
                   
                    tapestry.event.buildEventProperties( event, content, arguments);
                    if (!content['beventtarget.id']) {
                    	content['beventtarget.id']='increaseFont';
                    }                                 
                    
                    tapestry.bind('/pegasus?component=%24DefaultLayout.increaseFont&page=accounts_SearchResults&service=directevent&session=T', content);
                };
                tapestry.connect('increaseFont', 'trigger', 'event1755160906');
dojo.require('tapestry.form');tapestry.form.registerForm('searchForm');

tapestry.form.focusField('criteriaSelect');});
// --></script></body>
</html>
<!-- Render time: ~ 7 ms -->

