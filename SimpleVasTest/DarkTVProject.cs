﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleWebScrapper.Engine;

namespace SimpleVasTest
{
    [TestClass]
    public class DarkTVProject
    {
        [TestMethod]
        public void QualifyClickReturnPageSucess()
        {
            // Ilogger
            SimpleBellPageParser bellPageParser = new SimpleBellPageParser(null);
            var result = bellPageParser._darktvProductPageParser.IsQualifyButtonOperationSucess(DarkTVHtmlPages.GetQualifyReturnPage());
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void QualifyClickReturnPageFail()
        {
            // Ilogger
            SimpleBellPageParser bellPageParser = new SimpleBellPageParser(null);
            var result = bellPageParser._darktvProductPageParser.IsQualifyButtonOperationSucess(DarkTVHtmlPages.GetQualifyReturnPageFailCase());
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void VerifyFormIdandHiddenFieldExtractBody()
        {
            // Ilogger
             string Body = "formids=requalify%2CmaxTabsLoop_1%2CsubTabsLoop_0%2CisVisible_0%2CisLinkSubmit_0%2CsubTabLinkSubmit%2CisVisible_2%2CisLinkSubmit_2%2CsubTabLinkSubmit_0%2CisVisible_4%2CisLinkSubmit_4%2CsubTabLinkSubmit_1%2CisVisible_6%2CisLinkSubmit_6%2CsubTabLinkSubmit_2%2CoutputBodyIf_0%2CdslQualifyButton%2ChasMessages_5%2CqualificationRadioGroup%2CstnField%2ChasAlternativeAddress_0%2Ccollapsed_0%2CstreetNumber%2CstreetName%2CstreetType%2CstreetDirection%2CappartmentNumber%2CfloorNumber%2Ccity%2Cprovince%2CpostalCode%2CgroupKeyVisible_0%2CdslGroupKey%2CrestrictOverride_0%2CoverrideButton%2CuseLMS_0%2ChasLMSListData_0%2CaddressInfoListIf_0%2CcircuitIdListIf_0%2Caddress_0%2CrangeInfoListIf_0%2CifNextRangePageAvailable_0%2CifLMSNextPageAvailable_0%2ChasPASData_0%2CisExistUploadVASCapacity_0%2CisExistWorkIndicatorForVideo_0%2CisExistFutureServiceDateForVideo_0%2CisExistTransmissionIndicatorForVideo_0%2CisExistLineTroubleForVideo_0%2CisExistClliForVideo_0%2CisExistLineTypeForVideo_0%2CisExistClassOfServiceForVideo_0%2CisExistInfoForVideo_0%2CisExistNetworkTypeForVideo_0%2CisServiceTypeExistForVideo_0%2CisExistUploadVASCapacityForVideo_0%2CdialQualifyButton%2CtelcoList%2CregionList%2Csn%2CdialGroupKey%2CproductInfo_0%2CfinalButtonCopy1%2CisServiceAssuranceMigrationVisible_0%2CserviceAssuranceMigrationCheckbox%2CIf_0%2CIf_2%2CproductSelect%2CshowWarningMessage_0%2CshowProductBin_0%2CIf_4%2CIf_6%2CpricingStructureSelect%2CshowPricingBin_0%2CIf_8%2CIf_10%2CpromotionSelect%2CshowPromotionBin_0%2CisShowPromotionContinueButton_0%2CfinalButton%2CisShowOttoInternetActivationSection_0&seedids=ZH4sIAAAAAAAAAG1VzW7jNhAOil2gD5KjD3VR9O7ITtdbx3Ei7wJ7nJAjiwhFEvxxqn2mvlrfoUNSpuSfiz3zzXD%2BZ%2FTvf3eff97dfbr79ZeZQ3sUDO9%2Fmxk4xD%2BmO6MVKp9o5UEotEQ7dE5oFSlDP0tsIEi%2FgV6HqOpalDI%2BcW4j1PseDpl5JBO1%2BIln4NeXgLZfNQ0yP9ikMNBuocMn9JAV3xENSHHEtfIkBjnF9qLD7PlB857%2BJKhDoBSio8hGf%2FqL7rAwC94JJZy3MDjN8M5i406K%2BpCNgpT6o%2Fa9xKolw1HGkVkEhzEjYi069AMt1JlIJHN1Sa4DRYEtGNNBeVccP13CZFIHy%2FBS5dlytBGMXvzpRVGqLtDsbgeWmjixLlwqwBgWhSk4xzMkFWMauVAVJZXH4RUVBZLqPZ8NlmsEy9rUwUg8atvFWWKCe%2FeKjkak0jwGR6pZMcv2%2Bh3VNbzRh632N%2FRX1mr7RDNIqbkkJlEL7gTd%2FzHDiUp59%2FXl2%2Br1x7fXzYjUz9spv5PhIFRGmBU0agJqlFSDMSuBkhdumDDX6o8HYO8PwXsq1nz2RswgW0mH93%2FOgB9BMeT19F32SlV1oUO7XhZoYYwULA3nBB0HfQgXnPsb%2B8LXvfPY7S0oB6y8PXUndB3YPk1v2tdKSwnG4RJc%2B6bTEQAbU2uClHH9iAy0jGs%2BZLjR2ux7g%2BtmHl1azQPzUTGymjJfoqOqmdNKDerzicl5sTm%2FMjrajGNy7uHaQaVNP3ES00p5UAplDmiKm7VqpDi0Pi1O8f57mvBqtVA8L0e6Gu7kqxG20EiDH8%2FNUjgjoa%2FzkVxwTivqUtR0RhD9NnRvaOvNc3WuMiqQvZvia3BhPI3%2BloqWHFwrPFLedqpitPMgY%2BFuumDC9zcFVOMjnazbr5b1Rtv1bv9dNMMBoFkiUU6VoKMWpvbgQ9SGPGdDNwauSNlqvEqDrJihTrRC8lGBPjoPgjJT1Kwp34x0Nrykazd8MihywqwfINE8G1Snrnt4cymFf%2FZExZmLliTf03KblkzmWL6DDJgl9aSp1zC1cgIuDPntUF2r5z5dohV148LEbuhD7OAULm09gWk0XwJ9%2BJo%2B6%2FxldTD5DMSF2uWtGc9WwgQT6nCB6U7HTSpooz8%2BxZP9P2IbXqIUCAAA&component=form&page=product_change_Home&service=direct&session=T&submitmode=&submitname=&requalify=F&maxTabsLoop_1=V0&subTabsLoop_0=P3&isVisible_0=T&isLinkSubmit_0=T&subTabsLoop_0=P3&isVisible_2=T&isLinkSubmit_2=T&subTabsLoop_0=P3&isVisible_4=T&isLinkSubmit_4=T&subTabsLoop_0=P3&isVisible_6=T&isLinkSubmit_6=T&outputBodyIf_0=F&hasMessages_5=F&hasAlternativeAddress_0=F&collapsed_0=T&groupKeyVisible_0=T&restrictOverride_0=F&useLMS_0=T&hasLMSListData_0=T&addressInfoListIf_0=F&circuitIdListIf_0=T&address_0=P0&address_0=P0&address_0=P0&address_0=P0&address_0=P0&address_0=P0&address_0=P0&address_0=P0&address_0=P0&address_0=P0&rangeInfoListIf_0=F&ifNextRangePageAvailable_0=F&ifLMSNextPageAvailable_0=T&hasPASData_0=T&isExistUploadVASCapacity_0=T&isExistWorkIndicatorForVideo_0=T&isExistFutureServiceDateForVideo_0=F&isExistTransmissionIndicatorForVideo_0=T&isExistLineTroubleForVideo_0=T&isExistClliForVideo_0=T&isExistLineTypeForVideo_0=T&isExistClassOfServiceForVideo_0=T&isExistInfoForVideo_0=F&isExistNetworkTypeForVideo_0=T&isServiceTypeExistForVideo_0=T&isExistUploadVASCapacityForVideo_0=T&productInfo_0=T&isServiceAssuranceMigrationVisible_0=T&If_0=T&If_2=T&showWarningMessage_0=F&showProductBin_0=F&If_4=T&If_6=T&showPricingBin_0=F&If_8=T&If_10=T&showPromotionBin_0=F&isShowPromotionContinueButton_0=T&isShowOttoInternetActivationSection_0=F&";
            SimpleBellPageParser bellPageParser = new SimpleBellPageParser(null);
            var result = bellPageParser._darktvProductPageParser.ExtractPostMessageForSubmmitProduct(DarkTVHtmlPages.GetQualifyReturnPage(), "OIBTH");
            Assert.AreEqual(Body, result);
        }

        //[TestMethod]
        //public void VerifySelectProductPostExtractBody()
        //{
        //    string Body = "formids=requalify%2CmaxTabsLoop_1%2CsubTabsLoop_0%2CisVisible_0%2CisLinkSubmit_0%2CsubTabLinkSubmit%2CisVisible_2%2CisLinkSubmit_2%2CsubTabLinkSubmit_0%2CisVisible_4%2CisLinkSubmit_4%2CsubTabLinkSubmit_1%2CisVisible_6%2CisLinkSubmit_6%2CsubTabLinkSubmit_2%2CoutputBodyIf_0%2CdslQualifyButton%2ChasMessages_5%2CqualificationRadioGroup%2CstnField%2ChasAlternativeAddress_0%2Ccollapsed_0%2CstreetNumber%2CstreetName%2CstreetType%2CstreetDirection%2CappartmentNumber%2CfloorNumber%2Ccity%2Cprovince%2CpostalCode%2CgroupKeyVisible_0%2CdslGroupKey%2CrestrictOverride_0%2CoverrideButton%2CuseLMS_0%2ChasLMSListData_0%2CaddressInfoListIf_0%2CcircuitIdListIf_0%2Caddress_0%2CrangeInfoListIf_0%2CifNextRangePageAvailable_0%2CifLMSNextPageAvailable_0%2ChasPASData_0%2CisExistUploadVASCapacity_0%2CisExistWorkIndicatorForVideo_0%2CisExistFutureServiceDateForVideo_0%2CisExistTransmissionIndicatorForVideo_0%2CisExistLineTroubleForVideo_0%2CisExistClliForVideo_0%2CisExistLineTypeForVideo_0%2CisExistClassOfServiceForVideo_0%2CisExistInfoForVideo_0%2CisExistNetworkTypeForVideo_0%2CisServiceTypeExistForVideo_0%2CisExistUploadVASCapacityForVideo_0%2CdialQualifyButton%2CtelcoList%2CregionList%2Csn%2CdialGroupKey%2CproductInfo_0%2CfinalButtonCopy1%2CisServiceAssuranceMigrationVisible_0%2CserviceAssuranceMigrationCheckbox%2CIf_0%2CIf_2%2CproductSelect%2CshowWarningMessage_0%2CshowProductBin_0%2CIf_4%2CIf_6%2CpricingStructureSelect%2CshowPricingBin_0%2CIf_8%2CIf_10%2CpromotionSelect%2CshowPromotionBin_0%2CisShowPromotionContinueButton_0%2CfinalButton%2CisShowOttoInternetActivationSection_0&seedids=ZH4sIAAAAAAAAAG1VzW7jNhAOil2gD5KjD3VR9O7ITtdbx3Ei7wJ7nJAjiwhFEvxxqn2mvlrfoUNSpuSfiz3zzXD%2BZ%2FTvf3eff97dfbr79ZeZQ3sUDO9%2Fmxk4xD%2BmO6MVKp9o5UEotEQ7dE5oFSlDP0tsIEi%2FgV6HqOpalDI%2BcW4j1PseDpl5JBO1%2BIln4NeXgLZfNQ0yP9ikMNBuocMn9JAV3xENSHHEtfIkBjnF9qLD7PlB857%2BJKhDoBSio8hGf%2FqL7rAwC94JJZy3MDjN8M5i406K%2BpCNgpT6o%2Fa9xKolw1HGkVkEhzEjYi069AMt1JlIJHN1Sa4DRYEtGNNBeVccP13CZFIHy%2FBS5dlytBGMXvzpRVGqLtDsbgeWmjixLlwqwBgWhSk4xzMkFWMauVAVJZXH4RUVBZLqPZ8NlmsEy9rUwUg8atvFWWKCe%2FeKjkak0jwGR6pZMcv2%2Bh3VNbzRh632N%2FRX1mr7RDNIqbkkJlEL7gTd%2FzHDiUp59%2FXl2%2Br1x7fXzYjUz9spv5PhIFRGmBU0agJqlFSDMSuBkhdumDDX6o8HYO8PwXsq1nz2RswgW0mH93%2FOgB9BMeT19F32SlV1oUO7XhZoYYwULA3nBB0HfQgXnPsb%2B8LXvfPY7S0oB6y8PXUndB3YPk1v2tdKSwnG4RJc%2B6bTEQAbU2uClHH9iAy0jGs%2BZLjR2ux7g%2BtmHl1azQPzUTGymjJfoqOqmdNKDerzicl5sTm%2FMjrajGNy7uHaQaVNP3ES00p5UAplDmiKm7VqpDi0Pi1O8f57mvBqtVA8L0e6Gu7kqxG20EiDH8%2FNUjgjoa%2FzkVxwTivqUtR0RhD9NnRvaOvNc3WuMiqQvZvia3BhPI3%2BloqWHFwrPFLedqpitPMgY%2BFuumDC9zcFVOMjnazbr5b1Rtv1bv9dNMMBoFkiUU6VoKMWpvbgQ9SGPGdDNwauSNlqvEqDrJihTrRC8lGBPjoPgjJT1Kwp34x0Nrykazd8MihywqwfINE8G1Snrnt4cymFf%2FZExZmLliTf03KblkzmWL6DDJgl9aSp1zC1cgIuDPntUF2r5z5dohV148LEbuhD7OAULm09gWk0XwJ9%2BJo%2B6%2FxldTD5DMSF2uWtGc9WwgQT6nCB6U7HTSpooz8%2BxZP9P2IbXqIUCAAA&component=form&page=product_change_Home&service=direct&session=T&submitmode=&submitname=&requalify=F&maxTabsLoop_1=V0&subTabsLoop_0=P3&isVisible_0=T&isLinkSubmit_0=T&subTabsLoop_0=P3&isVisible_2=T&isLinkSubmit_2=T&subTabsLoop_0=P3&isVisible_4=T&isLinkSubmit_4=T&subTabsLoop_0=P3&isVisible_6=T&isLinkSubmit_6=T&outputBodyIf_0=F&hasMessages_5=F&hasAlternativeAddress_0=F&collapsed_0=T&groupKeyVisible_0=T&restrictOverride_0=F&useLMS_0=T&hasLMSListData_0=T&addressInfoListIf_0=F&circuitIdListIf_0=T&address_0=P0&address_0=P0&address_0=P0&address_0=P0&address_0=P0&address_0=P0&address_0=P0&address_0=P0&address_0=P0&address_0=P0&rangeInfoListIf_0=F&ifNextRangePageAvailable_0=F&ifLMSNextPageAvailable_0=T&hasPASData_0=T&isExistUploadVASCapacity_0=T&isExistWorkIndicatorForVideo_0=T&isExistFutureServiceDateForVideo_0=F&isExistTransmissionIndicatorForVideo_0=T&isExistLineTroubleForVideo_0=T&isExistClliForVideo_0=T&isExistLineTypeForVideo_0=T&isExistClassOfServiceForVideo_0=T&isExistInfoForVideo_0=F&isExistNetworkTypeForVideo_0=T&isServiceTypeExistForVideo_0=T&isExistUploadVASCapacityForVideo_0=T&productInfo_0=T&isServiceAssuranceMigrationVisible_0=T&If_0=T&If_2=T&showWarningMessage_0=F&showProductBin_0=F&If_4=T&If_6=T&showPricingBin_0=F&If_8=T&If_10=T&showPromotionBin_0=F&isShowPromotionContinueButton_0=T&isShowOttoInternetActivationSection_0=F&";
        //    SimpleBellPageParser bellPageParser = new SimpleBellPageParser(null);
        //    var result = bellPageParser._darktvProductPageParser.ExtractPostMessageForSubmmitProduct(DarkTVHtmlPages.GetQualifyReturnPage(), "OIBTH");
        //    Assert.AreEqual(Body, result);
        //}


        [TestMethod]
        public void SelectProeductReturnPageSucess()
        {
            // Ilogger
            SimpleBellPageParser bellPageParser = new SimpleBellPageParser(null);
            var result = bellPageParser._darktvProductPageParser.IsSelectProductOperationSuccess(DarkTVHtmlPages.GetPageAfterSelectDarkTVProduct());
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void SelectProeductReturnPageFail()
        {
            // Ilogger
            SimpleBellPageParser bellPageParser = new SimpleBellPageParser(null);
            var result = bellPageParser._darktvProductPageParser.IsSelectProductOperationSuccess(DarkTVHtmlPages.GetQualifyReturnPage());
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void SelectPricingAndVOIPReturnPageSucess()
        {
            // Ilogger
            SimpleBellPageParser bellPageParser = new SimpleBellPageParser(null);
            var result = bellPageParser._darktvProductPageParser.IsPricingandVoipSubmitSuccess(DarkTVHtmlPages.GetPageAfterSelectingPriceandVoip());
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void SelectPricingAndVOIPReturnPageFail()
        {
            // Ilogger
            SimpleBellPageParser bellPageParser = new SimpleBellPageParser(null);
            var result = bellPageParser._darktvProductPageParser.IsPricingandVoipSubmitSuccess(DarkTVHtmlPages.GetQualifyReturnPage());
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void VerifyExtractBodyFromAdditonalServicePage()
        {
            string Body = "formids=maxTabsLoop_1%2CsubTabsLoop_0%2CisVisible_0%2CisVisible_2%2CisLinkSubmit_0%2CsubTabLinkSubmit%2CisVisible_4%2CisLinkSubmit_2%2CsubTabLinkSubmit_0%2CisVisible_6%2CisLinkSubmit_4%2CsubTabLinkSubmit_1%2CoutputBodyIf_0%2CshowPrevious_0%2CpreviousButton%2CshowCancel_0%2CshowNext_0%2CnextButton%2CshowSubmit_0%2ChasMessages_5%2CisEmailRequired_0%2CVASLoop2_0%2CVASLoop1_0%2CcurrentVasCheckedIf_0%2CcurrentVasCheckedIf_2%2CcurrentVasCheckedIf_4%2CcurrentVasCheckedIf_6%2CcurrentVasCheckedIf_8%2CcurrentVasCheckedIf_10%2CcurrentVasCheckedIf_12%2CcurrentVasCheckedIf_14%2CcurrentVasCheckedIf_16%2CVASLoop3_0%2CshowWarningIf_0%2CshowSpecialPromotionsIf_0%2CshowBillingPurposeOnlyCheckboxIf_0%2CVASLoop_0%2CshowHeaderIf_0%2CterminateHeaderIf_0%2CopenRequiredChoice1If_0%2CcloseRequiredChoice1If_0%2CPOD05%2CemailServiceNameNotUnlimited_0%2CdisplayVasPromotionsIf_0%2CshowMessageIf_0%2CoutputServiceCharDiv_0%2CserviceCharLoop_0%2CshowBillingPurposeOnlyAcknowledgementIf_0%2CshowHeaderIf_2%2CopenRequiredChoice1If_2%2CcloseRequiredChoice1If_2%2CPOD01%2CemailServiceNameNotUnlimited_2%2CdisplayVasPromotionsIf_2%2CshowMessageIf_2%2CoutputServiceCharDiv_2%2CserviceCharLoop_2%2CnotHiddenSC_0%2CshowCharCodeIf_0%2CifShowDescription_0%2CshowLabel_0%2CisConfiguredValue_0%2CisEditMode_0%2CisDistinct_0%2CisDistinctBool_0%2CPOD01_PQTY_1%2CisCSCAddress_0%2CisMainAccessNumber_0%2CisCusAccessNumber_0%2CisUserAccountNumber_0%2CisExtCarrier_0%2CisShowEffectiveDate_0%2CshowBillingPurposeOnlyAcknowledgementIf_2%2CshowHeaderIf_4%2CterminateHeaderIf_2%2CopenRequiredChoice1If_4%2CcloseRequiredChoice1If_4%2CINETC%2CemailServiceNameNotUnlimited_4%2CdisplayVasPromotionsIf_4%2Cpromotions_INETC%2CshowMessageIf_4%2CoutputServiceCharDiv_4%2CshowBillingPurposeOnlyAcknowledgementIf_4%2CshowHeaderIf_6%2CopenRequiredChoice1If_6%2CcloseRequiredChoice1If_6%2CMCFBS%2CemailServiceNameNotUnlimited_6%2CdisplayVasPromotionsIf_6%2CshowMessageIf_6%2CoutputServiceCharDiv_6%2CserviceCharLoop_5%2CshowBillingPurposeOnlyAcknowledgementIf_6%2CshowHeaderIf_8%2CopenRequiredChoice1If_8%2CcloseRequiredChoice1If_8%2CMCFBT%2CemailServiceNameNotUnlimited_8%2CdisplayVasPromotionsIf_8%2CshowMessageIf_8%2CoutputServiceCharDiv_8%2CserviceCharLoop_7%2CshowBillingPurposeOnlyAcknowledgementIf_8%2CshowHeaderIf_10%2CopenRequiredChoice1If_10%2CcloseRequiredChoice1If_10%2CMCFGD%2CemailServiceNameNotUnlimited_10%2CdisplayVasPromotionsIf_10%2CshowMessageIf_10%2CoutputServiceCharDiv_10%2CshowBillingPurposeOnlyAcknowledgementIf_10%2CshowHeaderIf_12%2CopenRequiredChoice1If_12%2CcloseRequiredChoice1If_12%2CPWEB%2CemailServiceNameNotUnlimited_12%2CdisplayVasPromotionsIf_12%2CshowMessageIf_12%2CoutputServiceCharDiv_12%2CshowBillingPurposeOnlyAcknowledgementIf_12%2CshowHeaderIf_14%2CterminateHeaderIf_4%2CopenRequiredChoice1If_14%2CcloseRequiredChoice1If_14%2CBWGBB%2CemailServiceNameNotUnlimited_14%2CdisplayVasPromotionsIf_14%2Cpromotions_BWGBB%2CshowMessageIf_14%2CoutputServiceCharDiv_14%2CserviceCharLoop_9%2CnotHiddenSC_2%2CshowCharCodeIf_2%2CifShowDescription_2%2CshowLabel_2%2CisConfiguredValue_2%2CisEditMode_2%2CisDistinct_2%2CisDistinctBool_2%2CBWGBB_BWQTY_1%2CisCSCAddress_2%2CisMainAccessNumber_2%2CisCusAccessNumber_2%2CisUserAccountNumber_2%2CisExtCarrier_2%2CisShowEffectiveDate_2%2CshowBillingPurposeOnlyAcknowledgementIf_14%2CshowHeaderIf_16%2CopenRequiredChoice1If_16%2CcloseRequiredChoice1If_16%2CBWUNL%2CemailServiceNameNotUnlimited_16%2CdisplayVasPromotionsIf_16%2Cpromotions_BWUNL%2CshowMessageIf_16%2CoutputServiceCharDiv_16%2CserviceCharLoop_12%2CshowBillingPurposeOnlyAcknowledgementIf_16%2ChasMessages_7&seedids=ZH4sIAAAAAAAAAG1VzXLaMBBmOm2fJEcOpe0LgCFTUgIEk870uEhr0CBLGkkmdZ6pr9ZLpw%2FQlWRsE7jA7rer%2FdOn9e8%2Fgw%2Bvg8H7wce%2FQ4f2JBjefRoa2Ic%2FpkujFSofZeVBKLQkO3ROaBUkQz9TLKCSfgG1roKrO6CU4YhzC6GOW9gn5Z5C5OIVL8CHpwptPSsKZL6JSWWgXUKJj%2BghOR4RDUhxwrnyZAbZx7aixJR5onlNfxLUvqIWQqKghnz6my6xVca8FEo4b6FJmuC1xcKdHfU%2BBQUp9Uvua4nZgQIHG0dmERyGjki16NA3slAXJhHD5W1zJSgqbMyYrpR3beLHtzCF1JVl%2BNZlZTnaAIYs%2FnyidcreoCndGixdYi%2B6cHEAXVlUpuAcL5A4jH7lQmXUVKLDBhUVEuc9GjaRcwTLDvEGg3CvbRm4xAT3boOOKJJpHooj1%2BSYbFt9RHUNL%2FR%2Bqf0N%2F5m12j4SB6k1F81kOoA7Q3dfh9hzac89PD3PNj%2BfN4sOyVfLvr6W1V6ohDAriGoCcpQ0g64rgZK3WsMwd9AvE2DHSeU9DWs03JHS2GbS4d2XIfATKIY8759LWWmqrirRzqctNDZGChbJ2UM7ojflgnPfsW71vHYey60F5YC1Z8%2B3U5Ul2DqyN77XTEsJxuEU3GGnaQm8%2Bwc2tFZUUobnR2JFj3HOmw4XWpttbXBejEJKq3nFfHAMqqbOp%2Bhoaub8pBr3US%2FkqI05ugraxQw0ucxwnSDTpu4lCW3FPqiFlgfE4mKuCin2Bx8fTpv9c2R4Nhsrnh5H3BrunKsQtpWRiB%2FWzVQ4I6HO05Icc05P1MWqaY0g%2BmVV7tDmi1V26dI5ULyb5mtwbDxRf0lDiwmuHe6pb9t3Mdp5kGFwN1Mw4eubBprxiVbW7VPTfKHtfL39IYpmARCXyJRaJeikhck9%2BCp4Q%2BJZcxuN1lrZrNtKja0NQzdxEJJ3DvTRmQjqTNFl9fWik1PgKW275pNBlRNmfQOJYmVQnW%2Fdw87FFn5tSQqcC7ccF9R%2FUwLAnwIHAAA%3D&component=form&page=product_change_VASEntry&service=direct&session=T&submitmode=&submitname=nextButton&maxTabsLoop_1=V0&subTabsLoop_0=P0&isVisible_0=F&subTabsLoop_0=P0&isVisible_2=T&isLinkSubmit_0=T&subTabsLoop_0=P0&isVisible_4=T&isLinkSubmit_2=T&subTabsLoop_0=P0&isVisible_6=T&isLinkSubmit_4=T&outputBodyIf_0=F&showPrevious_0=T&showCancel_0=T&showNext_0=T&showSubmit_0=F&hasMessages_5=F&isEmailRequired_0=F&VASLoop2_0=PSPOD05&VASLoop2_0=PSPOD01&VASLoop2_0=PSINETC&VASLoop2_0=PSMCFBS&VASLoop2_0=PSMCFBT&VASLoop2_0=PSMCFGD&VASLoop2_0=PSPWEB&VASLoop2_0=PSBWGBB&VASLoop2_0=PSBWUNL&VASLoop1_0=PSPOD05&currentVasCheckedIf_0=F&VASLoop1_0=PSPOD01&currentVasCheckedIf_2=F&VASLoop1_0=PSINETC&currentVasCheckedIf_4=F&VASLoop1_0=PSMCFBS&currentVasCheckedIf_6=F&VASLoop1_0=PSMCFBT&currentVasCheckedIf_8=F&VASLoop1_0=PSMCFGD&currentVasCheckedIf_10=T&VASLoop1_0=PSPWEB&currentVasCheckedIf_12=F&VASLoop1_0=PSBWGBB&currentVasCheckedIf_14=F&VASLoop1_0=PSBWUNL&currentVasCheckedIf_16=F&VASLoop3_0=PSPOD05&VASLoop3_0=PSPOD01&VASLoop3_0=PSINETC&VASLoop3_0=PSMCFBS&VASLoop3_0=PSMCFBT&VASLoop3_0=PSMCFGD&VASLoop3_0=PSPWEB&VASLoop3_0=PSBWGBB&VASLoop3_0=PSBWUNL&showWarningIf_0=T&showSpecialPromotionsIf_0=F&showBillingPurposeOnlyCheckboxIf_0=F&VASLoop_0=PSPOD05&showHeaderIf_0=T&terminateHeaderIf_0=F&openRequiredChoice1If_0=F&closeRequiredChoice1If_0=F&emailServiceNameNotUnlimited_0=T&displayVasPromotionsIf_0=F&showMessageIf_0=F&outputServiceCharDiv_0=T&showBillingPurposeOnlyAcknowledgementIf_0=F&VASLoop_0=PSPOD01&showHeaderIf_2=F&openRequiredChoice1If_2=F&closeRequiredChoice1If_2=F&emailServiceNameNotUnlimited_2=T&displayVasPromotionsIf_2=F&showMessageIf_2=F&outputServiceCharDiv_2=T&serviceCharLoop_2=V0&notHiddenSC_0=T&showCharCodeIf_0=F&ifShowDescription_0=F&showLabel_0=F&isConfiguredValue_0=T&isEditMode_0=T&isDistinct_0=T&isDistinctBool_0=F&isCSCAddress_0=F&isMainAccessNumber_0=F&isCusAccessNumber_0=F&isUserAccountNumber_0=F&isExtCarrier_0=F&isShowEffectiveDate_0=F&showBillingPurposeOnlyAcknowledgementIf_2=F&VASLoop_0=PSINETC&showHeaderIf_4=T&terminateHeaderIf_2=T&openRequiredChoice1If_4=F&closeRequiredChoice1If_4=F&emailServiceNameNotUnlimited_4=T&displayVasPromotionsIf_4=T&showMessageIf_4=F&outputServiceCharDiv_4=F&showBillingPurposeOnlyAcknowledgementIf_4=F&VASLoop_0=PSMCFBS&showHeaderIf_6=F&openRequiredChoice1If_6=F&closeRequiredChoice1If_6=F&emailServiceNameNotUnlimited_6=T&displayVasPromotionsIf_6=F&showMessageIf_6=F&outputServiceCharDiv_6=T&showBillingPurposeOnlyAcknowledgementIf_6=F&VASLoop_0=PSMCFBT&showHeaderIf_8=F&openRequiredChoice1If_8=F&closeRequiredChoice1If_8=F&emailServiceNameNotUnlimited_8=T&displayVasPromotionsIf_8=F&showMessageIf_8=F&outputServiceCharDiv_8=T&showBillingPurposeOnlyAcknowledgementIf_8=F&VASLoop_0=PSMCFGD&showHeaderIf_10=F&openRequiredChoice1If_10=F&closeRequiredChoice1If_10=F&emailServiceNameNotUnlimited_10=T&displayVasPromotionsIf_10=F&showMessageIf_10=F&outputServiceCharDiv_10=F&showBillingPurposeOnlyAcknowledgementIf_10=F&VASLoop_0=PSPWEB&showHeaderIf_12=F&openRequiredChoice1If_12=F&closeRequiredChoice1If_12=F&emailServiceNameNotUnlimited_12=T&displayVasPromotionsIf_12=F&showMessageIf_12=F&outputServiceCharDiv_12=F&showBillingPurposeOnlyAcknowledgementIf_12=F&VASLoop_0=PSBWGBB&showHeaderIf_14=T&terminateHeaderIf_4=T&openRequiredChoice1If_14=F&closeRequiredChoice1If_14=F&emailServiceNameNotUnlimited_14=T&displayVasPromotionsIf_14=T&showMessageIf_14=F&outputServiceCharDiv_14=T&serviceCharLoop_9=V0&notHiddenSC_2=T&showCharCodeIf_2=F&ifShowDescription_2=F&showLabel_2=F&isConfiguredValue_2=T&isEditMode_2=T&isDistinct_2=T&isDistinctBool_2=F&isCSCAddress_2=F&isMainAccessNumber_2=F&isCusAccessNumber_2=F&isUserAccountNumber_2=F&isExtCarrier_2=F&isShowEffectiveDate_2=F&showBillingPurposeOnlyAcknowledgementIf_14=F&VASLoop_0=PSBWUNL&showHeaderIf_16=F&openRequiredChoice1If_16=F&closeRequiredChoice1If_16=F&emailServiceNameNotUnlimited_16=F&displayVasPromotionsIf_16=T&showMessageIf_16=F&outputServiceCharDiv_16=T&showBillingPurposeOnlyAcknowledgementIf_16=F&hasMessages_7=F&POD01_PQTY_1=0&MCFGD=on&BWGBB_BWQTY_1=0";
            SimpleBellPageParser bellPageParser = new SimpleBellPageParser(null);
            var result = bellPageParser._darktvProductPageParser.ExtractMessageBodyFromAddditonalServicePage(DarkTVHtmlPages.GetAddtionalServicePage());//,"", "shippingFormhidden");
            Assert.AreEqual(Body, result);
        }



        [TestMethod]
        public void VerifyExtractFinalSubmitScenerio1Body()
        {
            string Body = "formids=maxTabsLoop_1%2CsubTabsLoop_0%2CisVisible_0%2CisVisible_2%2CisLinkSubmit_0%2CsubTabLinkSubmit%2CisVisible_4%2CisLinkSubmit_2%2CsubTabLinkSubmit_0%2CisVisible_6%2CisLinkSubmit_4%2CsubTabLinkSubmit_1%2CoutputBodyIf_0%2CshowPrevious_0%2CpreviousButton%2CshowCancel_0%2CshowNext_0%2CshowSubmit_0%2CsubmitButton%2ChasMessages_5%2ChasMessages_7%2CagentId%2CdealerId%2CifOverrideQualification_0%2CifAddressQualification_0%2CifPhoneQualification_0%2CifParentMergeIn_0%2ChasPromotion_0%2ChasSpecialPromotions_0%2CisShowPriceDetail_0%2CshowParts_0%2CshowMtrOtrfInformation_0%2CisShowMtrPromoDropdown_0%2CisShowMtrPromoText_0%2CisShowOtrfPromoDropdown_0%2CisShowOtrfPromoText_0%2CshowOneTimeCharges_0%2CisFeeEnabled_0%2CisFeeEnabledCopy_0%2CisShowCreditDropdown_0%2ChasEmail_0%2CdisplayVasIf_0%2CvasLoop_0%2CemailServiceNameNotUnlimited2_0%2ChistoryModeIf_0%2CshowPaymentInfo_0%2CcollapseIf_0%2CshowTypeIf_0%2CshippingProcessCompleted_0%2CifShowInstallation_0%2CpaymentProcessCompleted_0&seedids=ZH4sIAAAAAAAAAG1VzXLaMBBmOm2fJEcOpe0LgCFTUgIEk870uEhr0CBLGkkmdZ6pr9ZLpw%2FQlWRsE7jA7rer%2FdOn9e8%2Fgw%2Bvg8H7wce%2FQ4f2JBjefRoa2Ic%2FpkujFSofZeVBKLQkO3ROaBUkQz9TLKCSfgG1roKrO6CU4YhzC6GOW9gn5Z5C5OIVL8CHpwptPSsKZL6JSWWgXUKJj%2BghOR4RDUhxwrnyZAbZx7aixJR5onlNfxLUvqIWQqKghnz6my6xVca8FEo4b6FJmuC1xcKdHfU%2BBQUp9Uvua4nZgQIHG0dmERyGjki16NA3slAXJhHD5W1zJSgqbMyYrpR3beLHtzCF1JVl%2BNZlZTnaAIYs%2FnyidcreoCndGixdYi%2B6cHEAXVlUpuAcL5A4jH7lQmXUVKLDBhUVEuc9GjaRcwTLDvEGg3CvbRm4xAT3boOOKJJpHooj1%2BSYbFt9RHUNL%2FR%2Bqf0N%2F5m12j4SB6k1F81kOoA7Q3dfh9hzac89PD3PNj%2BfN4sOyVfLvr6W1V6ohDAriGoCcpQ0g64rgZK3WsMwd9AvE2DHSeU9DWs03JHS2GbS4d2XIfATKIY8759LWWmqrirRzqctNDZGChbJ2UM7ojflgnPfsW71vHYey60F5YC1Z8%2B3U5Ul2DqyN77XTEsJxuEU3GGnaQm8%2Bwc2tFZUUobnR2JFj3HOmw4XWpttbXBejEJKq3nFfHAMqqbOp%2Bhoaub8pBr3US%2FkqI05ugraxQw0ucxwnSDTpu4lCW3FPqiFlgfE4mKuCin2Bx8fTpv9c2R4Nhsrnh5H3BrunKsQtpWRiB%2FWzVQ4I6HO05Icc05P1MWqaY0g%2BmVV7tDmi1V26dI5ULyb5mtwbDxRf0lDiwmuHe6pb9t3Mdp5kGFwN1Mw4eubBprxiVbW7VPTfKHtfL39IYpmARCXyJRaJeikhck9%2BCp4Q%2BJZcxuN1lrZrNtKja0NQzdxEJJ3DvTRmQjqTNFl9fWik1PgKW275pNBlRNmfQOJYmVQnW%2Fdw87FFn5tSQqcC7ccF9R%2FUwLAnwIHAAA%3D&component=form&page=product_change_Confirmation&service=direct&session=T&submitmode=&submitname=submitButton&maxTabsLoop_1=V0&subTabsLoop_0=P0&isVisible_0=F&subTabsLoop_0=P0&isVisible_2=T&isLinkSubmit_0=T&subTabsLoop_0=P0&isVisible_4=T&isLinkSubmit_2=T&subTabsLoop_0=P0&isVisible_6=T&isLinkSubmit_4=T&outputBodyIf_0=F&showPrevious_0=T&showCancel_0=T&showNext_0=F&showSubmit_0=T&hasMessages_5=F&hasMessages_7=F&ifOverrideQualification_0=F&ifAddressQualification_0=F&ifPhoneQualification_0=T&ifParentMergeIn_0=F&hasPromotion_0=F&hasSpecialPromotions_0=F&isShowPriceDetail_0=T&showParts_0=F&showMtrOtrfInformation_0=T&isShowMtrPromoDropdown_0=F&isShowMtrPromoText_0=T&isShowOtrfPromoDropdown_0=F&isShowOtrfPromoText_0=T&showOneTimeCharges_0=T&isFeeEnabled_0=T&isFeeEnabledCopy_0=T&isShowCreditDropdown_0=F&hasEmail_0=F&displayVasIf_0=T&vasLoop_0=VSMCFGD&emailServiceNameNotUnlimited2_0=T&historyModeIf_0=F&showPaymentInfo_0=F&collapseIf_0=F&showTypeIf_0=F&shippingProcessCompleted_0=T&ifShowInstallation_0=T&paymentProcessCompleted_0=F&agentId=hisc1832&dealerId=HSCSPL";
            SimpleBellPageParser bellPageParser = new SimpleBellPageParser(null);
            var result = bellPageParser._darktvProductPageParser.ExtractFinalSubmitScenerio1Body(DarkTVHtmlPages.GetConfirmationDetailPage());//,"", "shippingFormhidden");
            Assert.AreEqual(Body, result);
        }

        [TestMethod]
        public void SuccessfulSubmitScenerio1()
        {
            
            SimpleBellPageParser bellPageParser = new SimpleBellPageParser(null);
            var result = bellPageParser._darktvProductPageParser.IsOrderSuccessfullySubmitted(DarkTVHtmlPages.SuccessPageAfterScenerioSubmittion());//,"", "shippingFormhidden");
           Assert.IsTrue(result);
        }


        [TestMethod]
        public void IsPromotionExistInSceerio2Success()
        {

            SimpleBellPageParser bellPageParser = new SimpleBellPageParser(null);
            var result =  bellPageParser._darktvProductPageParser.isExisting0FibeTVExist(DarkTVHtmlPages.GetScenerio2EditPage());//,"", "shippingFormhidden");
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void GetPromotionCodeFromSelectBox()
        {

            SimpleBellPageParser bellPageParser = new SimpleBellPageParser(null);
            var result = bellPageParser._darktvProductPageParser.GetExistingFibeTVNetworkAccessCode(DarkTVHtmlPages.GetScenerio2EditPage());//,"", "shippingFormhidden");
            Assert.AreEqual("PS114",result);
        }


        [TestMethod]
        public void VerifyExtractFinalSubmitScenerio2Body()
        {
            string Body = "formids=displayEditButtonsIf_0%2CisEditMode_0%2CsubmitButton%2CisEditMode1_0%2ChasMessages_6%2CpricingStructureDropDown%2CpromotionDropDown%2CshowSpecialPromotionsIf_0&seedids=ZH4sIAAAAAAAAAG1WS3PbNhD2dNpLf4ePOoSKk0yOenmqVK%2BYTDo9dSBgKWIMAhwAlMP8pv61%2FocuHgIhyz5Y2N0P%2B8I%2B%2BO9%2Fd7%2F9vLv79e737cSAPnMK9%2B8mHTm5H6raTkmQ1p%2BlJVyCxrMBY7iS7tThvyXUpBd2QwbVO6hpQAh3xZgNl88VOQXiEVWU%2FCdcMb987UEPq7oGaqNOdAP0jrSwBUsC8BmgI4KfYS0tionIeRVvIVieKzbgjyDy1GMIzpAjnT31h2ohETPWcsmN1SQaDeyDhtpcgOoUlBIh1EtpBwGLBhU7GQOqgRhwESGpwYCNZy6vRNyrK1NwLZHo2IxS1UtrkuHtazaqVL2m8Bqy1wy0Yzor9nIjgRavuMHcgWh8xEw7Nz4Bo1voJmcMrjg%2BGbnnXC4wqFAOTyDREZ%2Fv6SRqLoFo2vgXdIdHpVtXS5Qza57AYIksFHPOITQAg6xSzyBv2Rt12in7Bn6ltdJbrEEMzXgxihpiLqz7DxPIIOnel6%2FfVk9%2Ff3vajJxyv8vpg%2BhPXAYO1RxLjZMSBOZgjIqDYImKFWYa9TIn9HneW4vJKiZHJKJsJQzcFw8Tws5EUmBlfjGYxbSavgW9XibWrOsEp746M%2B5Y6dFfYsyfMCS6HIyFttJEGkLT3cvz9G1L9ODL1zfsQglBOgNLYpqjHwKKaBdb3Qvh%2Bg%2BPPXbjmsUQN0p11dDBui6cSa1YT60DFlgECkNfgsG0dZeeivAiU1kkncWN0lGnq5NrC7cGFqobMiMuLBeHCyEVApZxvZa14KfG%2Bs5J1qe%2BxBermWShO%2FzYMDaGXXOdzoCV7%2BbNkptOkKEMU3LGGPao8V7jHAGwu749gi43%2B8U1ZASgvjfFt8xZZ7H2d5g0b%2BAW8Ihx6xzSKWOJcIl70wTldnhTgDk%2B48x6%2B9ay3Ci9PlTfeR0nANYSikKoyDor3pWW2N6hSaiz%2BBqRSlK6GsdSlCU1%2BBINF2wE4NaZc4xM4mPldD2eg%2BIljru4M9Bz5GkbWbzedyAvr27J0fgQflR4cjWHFSXJeQNnEO%2Bdhv6YJEWBHn3nhh8F3H%2BOImfPUx2RfhvgmfsFV%2FbHltsE9K2dCHIEgRR2bddbNy8xhiJZfnDxeNEh1Lr5qyG2UksVpnTMk7IVaBzY6AHzoc2JZC%2FcNm5y4OrBBbWSBL1lWQ%2F5ZRXnTKvOUKb17tquxAFeAW32vpl8Ytc1DoFf9mgttcT1w8SWw57CXd5AdNmpDw23wY3q5YveWFx7uuKgbxyLyX8X%2F6bvHz5OH6Yf%2FoEfmFmWIytuBVQM8xUZKw8Z6VADOSJMa6Vzns9BMaG9dlEdciAwjNa%2FSKwgDEx7Bz9fOBcnPl0YTl0xkl4%2BksmlV5yMHl280olUdLHMgRcXP91mjMYBnr96ZGFEr%2BAf8wTfyHJVJtqNmcoH8xK%2FyLgInxArxu0W583Uiznl8oQ3WlX6Ce2GqP8A%2BB9ysqtiYgoAAA%3D%3D&component=productDetails.form&page=accounts_ViewProductInfo&service=direct&session=T&submitmode=&submitname=submitButton&displayEditButtonsIf_0=T&isEditMode_0=T&isEditMode1_0=T&hasMessages_6=F&showSpecialPromotionsIf_0=F&pricingStructureDropDown=PS114&promotionDropDown=";
            SimpleBellPageParser bellPageParser = new SimpleBellPageParser(null);
            var result = bellPageParser._darktvProductPageParser.ExtractFinalSubmitScenerio2Body(DarkTVHtmlPages.GetScenerio2EditPage());//,"", "shippingFormhidden");
            Assert.AreEqual(Body, result);
        }


        [TestMethod]
         public void IsMigrationProductExistSuccess()
        {
          // string Body = "formids=displayEditButtonsIf_0%2CisEditMode_0%2CsubmitButton%2CisEditMode1_0%2ChasMessages_6%2CpricingStructureDropDown%2CpromotionDropDown%2CshowSpecialPromotionsIf_0&seedids=ZH4sIAAAAAAAAAG1WS3PbNhD2dNpLf4ePOoSKk0yOenmqVK%2BYTDo9dSBgKWIMAhwAlMP8pv61%2FocuHgIhyz5Y2N0P%2B8I%2B%2BO9%2Fd7%2F9vLv79e737cSAPnMK9%2B8mHTm5H6raTkmQ1p%2BlJVyCxrMBY7iS7tThvyXUpBd2QwbVO6hpQAh3xZgNl88VOQXiEVWU%2FCdcMb987UEPq7oGaqNOdAP0jrSwBUsC8BmgI4KfYS0tionIeRVvIVieKzbgjyDy1GMIzpAjnT31h2ohETPWcsmN1SQaDeyDhtpcgOoUlBIh1EtpBwGLBhU7GQOqgRhwESGpwYCNZy6vRNyrK1NwLZHo2IxS1UtrkuHtazaqVL2m8Bqy1wy0Yzor9nIjgRavuMHcgWh8xEw7Nz4Bo1voJmcMrjg%2BGbnnXC4wqFAOTyDREZ%2Fv6SRqLoFo2vgXdIdHpVtXS5Qza57AYIksFHPOITQAg6xSzyBv2Rt12in7Bn6ltdJbrEEMzXgxihpiLqz7DxPIIOnel6%2FfVk9%2Ff3vajJxyv8vpg%2BhPXAYO1RxLjZMSBOZgjIqDYImKFWYa9TIn9HneW4vJKiZHJKJsJQzcFw8Tws5EUmBlfjGYxbSavgW9XibWrOsEp746M%2B5Y6dFfYsyfMCS6HIyFttJEGkLT3cvz9G1L9ODL1zfsQglBOgNLYpqjHwKKaBdb3Qvh%2Bg%2BPPXbjmsUQN0p11dDBui6cSa1YT60DFlgECkNfgsG0dZeeivAiU1kkncWN0lGnq5NrC7cGFqobMiMuLBeHCyEVApZxvZa14KfG%2Bs5J1qe%2BxBermWShO%2FzYMDaGXXOdzoCV7%2BbNkptOkKEMU3LGGPao8V7jHAGwu749gi43%2B8U1ZASgvjfFt8xZZ7H2d5g0b%2BAW8Ihx6xzSKWOJcIl70wTldnhTgDk%2B48x6%2B9ay3Ci9PlTfeR0nANYSikKoyDor3pWW2N6hSaiz%2BBqRSlK6GsdSlCU1%2BBINF2wE4NaZc4xM4mPldD2eg%2BIljru4M9Bz5GkbWbzedyAvr27J0fgQflR4cjWHFSXJeQNnEO%2Bdhv6YJEWBHn3nhh8F3H%2BOImfPUx2RfhvgmfsFV%2FbHltsE9K2dCHIEgRR2bddbNy8xhiJZfnDxeNEh1Lr5qyG2UksVpnTMk7IVaBzY6AHzoc2JZC%2FcNm5y4OrBBbWSBL1lWQ%2F5ZRXnTKvOUKb17tquxAFeAW32vpl8Ytc1DoFf9mgttcT1w8SWw57CXd5AdNmpDw23wY3q5YveWFx7uuKgbxyLyX8X%2F6bvHz5OH6Yf%2FoEfmFmWIytuBVQM8xUZKw8Z6VADOSJMa6Vzns9BMaG9dlEdciAwjNa%2FSKwgDEx7Bz9fOBcnPl0YTl0xkl4%2BksmlV5yMHl280olUdLHMgRcXP91mjMYBnr96ZGFEr%2BAf8wTfyHJVJtqNmcoH8xK%2FyLgInxArxu0W583Uiznl8oQ3WlX6Ce2GqP8A%2BB9ysqtiYgoAAA%3D%3D&component=productDetails.form&page=accounts_ViewProductInfo&service=direct&session=T&submitmode=&submitname=submitButton&displayEditButtonsIf_0=T&isEditMode_0=T&isEditMode1_0=T&hasMessages_6=F&showSpecialPromotionsIf_0=F&pricingStructureDropDown=PS114&promotionDropDown=";
            SimpleBellPageParser bellPageParser = new SimpleBellPageParser(null);
            var result = bellPageParser._darktvProductPageParser.IsMigrationProductExist(DarkTVHtmlPages.GetQualifyReturnPage());//,"", "shippingFormhidden");
            Assert.IsTrue(result);
        }


        [TestMethod]
        public void IsMigrationProductExistFailure()
        {
            // string Body = "formids=displayEditButtonsIf_0%2CisEditMode_0%2CsubmitButton%2CisEditMode1_0%2ChasMessages_6%2CpricingStructureDropDown%2CpromotionDropDown%2CshowSpecialPromotionsIf_0&seedids=ZH4sIAAAAAAAAAG1WS3PbNhD2dNpLf4ePOoSKk0yOenmqVK%2BYTDo9dSBgKWIMAhwAlMP8pv61%2FocuHgIhyz5Y2N0P%2B8I%2B%2BO9%2Fd7%2F9vLv79e737cSAPnMK9%2B8mHTm5H6raTkmQ1p%2BlJVyCxrMBY7iS7tThvyXUpBd2QwbVO6hpQAh3xZgNl88VOQXiEVWU%2FCdcMb987UEPq7oGaqNOdAP0jrSwBUsC8BmgI4KfYS0tionIeRVvIVieKzbgjyDy1GMIzpAjnT31h2ohETPWcsmN1SQaDeyDhtpcgOoUlBIh1EtpBwGLBhU7GQOqgRhwESGpwYCNZy6vRNyrK1NwLZHo2IxS1UtrkuHtazaqVL2m8Bqy1wy0Yzor9nIjgRavuMHcgWh8xEw7Nz4Bo1voJmcMrjg%2BGbnnXC4wqFAOTyDREZ%2Fv6SRqLoFo2vgXdIdHpVtXS5Qza57AYIksFHPOITQAg6xSzyBv2Rt12in7Bn6ltdJbrEEMzXgxihpiLqz7DxPIIOnel6%2FfVk9%2Ff3vajJxyv8vpg%2BhPXAYO1RxLjZMSBOZgjIqDYImKFWYa9TIn9HneW4vJKiZHJKJsJQzcFw8Tws5EUmBlfjGYxbSavgW9XibWrOsEp746M%2B5Y6dFfYsyfMCS6HIyFttJEGkLT3cvz9G1L9ODL1zfsQglBOgNLYpqjHwKKaBdb3Qvh%2Bg%2BPPXbjmsUQN0p11dDBui6cSa1YT60DFlgECkNfgsG0dZeeivAiU1kkncWN0lGnq5NrC7cGFqobMiMuLBeHCyEVApZxvZa14KfG%2Bs5J1qe%2BxBermWShO%2FzYMDaGXXOdzoCV7%2BbNkptOkKEMU3LGGPao8V7jHAGwu749gi43%2B8U1ZASgvjfFt8xZZ7H2d5g0b%2BAW8Ihx6xzSKWOJcIl70wTldnhTgDk%2B48x6%2B9ay3Ci9PlTfeR0nANYSikKoyDor3pWW2N6hSaiz%2BBqRSlK6GsdSlCU1%2BBINF2wE4NaZc4xM4mPldD2eg%2BIljru4M9Bz5GkbWbzedyAvr27J0fgQflR4cjWHFSXJeQNnEO%2Bdhv6YJEWBHn3nhh8F3H%2BOImfPUx2RfhvgmfsFV%2FbHltsE9K2dCHIEgRR2bddbNy8xhiJZfnDxeNEh1Lr5qyG2UksVpnTMk7IVaBzY6AHzoc2JZC%2FcNm5y4OrBBbWSBL1lWQ%2F5ZRXnTKvOUKb17tquxAFeAW32vpl8Ytc1DoFf9mgttcT1w8SWw57CXd5AdNmpDw23wY3q5YveWFx7uuKgbxyLyX8X%2F6bvHz5OH6Yf%2FoEfmFmWIytuBVQM8xUZKw8Z6VADOSJMa6Vzns9BMaG9dlEdciAwjNa%2FSKwgDEx7Bz9fOBcnPl0YTl0xkl4%2BksmlV5yMHl280olUdLHMgRcXP91mjMYBnr96ZGFEr%2BAf8wTfyHJVJtqNmcoH8xK%2FyLgInxArxu0W583Uiznl8oQ3WlX6Ce2GqP8A%2BB9ysqtiYgoAAA%3D%3D&component=productDetails.form&page=accounts_ViewProductInfo&service=direct&session=T&submitmode=&submitname=submitButton&displayEditButtonsIf_0=T&isEditMode_0=T&isEditMode1_0=T&hasMessages_6=F&showSpecialPromotionsIf_0=F&pricingStructureDropDown=PS114&promotionDropDown=";
            SimpleBellPageParser bellPageParser = new SimpleBellPageParser(null);
            var result = bellPageParser._darktvProductPageParser.IsMigrationProductExist(DarkTVHtmlPages.GetQualifyReturnPageFailCase());//,"", "shippingFormhidden");
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void SuccessfulSubmitScenerio2()
        {

            SimpleBellPageParser bellPageParser = new SimpleBellPageParser(null);
            var result = bellPageParser._darktvProductPageParser.IsOrderSuccessfullySubmittedForScenerio2(DarkTVHtmlPages.SuccessPageAfterScenerio2Submittion());//,"", "shippingFormhidden");
            Assert.IsTrue(result);
        }

    }
}
