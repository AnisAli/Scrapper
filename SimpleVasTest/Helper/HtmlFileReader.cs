﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleVasTest
{
    public static class HtmlFileReader
    {

        public static string SearchNotFoundPage()
        {
            return readFile("SearchNotFound-Html.txt");
        }

        public static string CustomerViewSearchResult()
        {
            return readFile("ViewCustomer-SeachResultRedirect.txt");
        }
        public static string SearchFoundPage()
        {
            return readFile("Found-SearchCustomer.txt");
        }
        public static string ProductSubMenu_UnlimiteddUsage()
        {
            return readFile("ProductViewSubMenuLink.txt");
        }
        public static string ProductViewPage()
        {
            return readFile("ProductViewPage.txt");
        }
        public static string ValueEditPageWithEditEnable()
        {
            return readFile("ValueEditServiceEditEnable.txt");
        }
        public static string UnlimitedPlanUncheked()
        {
            return readFile("UnlimitedPlanUnchecked.txt");
        }
        public static string readFile(string fileName)
        {
            string file = "../../Fake/" + fileName;
            string htmlPage = "";
            using (var fs = File.OpenRead(file))
            using (var reader = new StreamReader(fs))
            {
                htmlPage = reader.ReadToEnd();
            }

            return htmlPage;
        }


    }
}
