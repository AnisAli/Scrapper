﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleVasTest
{
   public static class DarkTVHtmlPages 
    {

        public static string GetQualifyReturnPage()
        {
            var FileName = "QualifyClickReturnPage.txt";
            return HtmlFileReader.readFile("DarkTVPages/"+FileName);
        }

        public static string GetQualifyReturnPageFailCase ()
        {
            var FileName = "QualifyReturnWithNoOptionFail.txt";
            return HtmlFileReader.readFile("DarkTVPages/" + FileName);
        }

        public static string GetPageAfterSelectDarkTVProduct()
        {
            var FileName = "SelectProductPostReturn.txt";
            return HtmlFileReader.readFile("DarkTVPages/" + FileName);
        }

        public static string GetPageAfterSelectingPriceandVoip()
        {
            var FileName = "SelectPricingAndVoipPostrReturnPage.txt";
            return HtmlFileReader.readFile("DarkTVPages/" + FileName);
        }

        public static string GetContinueClickReturnPage()
        {
            var FileName = "ContinueClickReturnPage.txt";
            return HtmlFileReader.readFile("DarkTVPages/" + FileName);
        }

        public static string GetAddtionalServicePage()
        {
            var FileName = "AdditonalServicePageAfterselfTypePost.txt";
            return HtmlFileReader.readFile("DarkTVPages/" + FileName);
        }
        
       public static string GetConfirmationDetailPage()
        {
            var FileName = "AfterSubmittingAddtionalServicePage.txt";
            return HtmlFileReader.readFile("DarkTVPages/" + FileName);
        }

        public static string SuccessPageAfterScenerioSubmittion()
        {
            var FileName = "Scenerio1FinalSubmitSuccess.txt";
            return HtmlFileReader.readFile("DarkTVPages/" + FileName);
        }

        public static string GetScenerio2EditPage()
        {
            var FileName = "Scnerio-2-EditPage.txt";
            return HtmlFileReader.readFile("DarkTVPages/" + FileName);
        }

        
              public static string SuccessPageAfterScenerio2Submittion()
        {
            var FileName = "Scenerio2-SuccessOrder.txt";
            return HtmlFileReader.readFile("DarkTVPages/" + FileName);
        }

    }
}
