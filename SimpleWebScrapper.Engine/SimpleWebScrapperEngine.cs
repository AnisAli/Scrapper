﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using Serilog;
using SimpleWebScrapper.Common;
using SimpleWebScrapper.Engine.Configuration;
using SimpleWebScrapper.Engine.Contracts;
using SimpleWebScrapper.Engine.Engine;


namespace SimpleWebScrapper.Engine
{
    public class SimpleWebScrapperEngine : ISimpleWebScrapperEngine
    {
        private IUnityContainer _container;
        private readonly ILogger _logger;
        public SimpleWebScrapperEngine(IUnityContainer container, ILogger logger)
        {
            _container = container;
            _logger = logger;
            Init();
        }
        private void Init()
        {
            _container.RegisterType<IEngineInputHandler, EngineInputHandler>()
                .RegisterType<ISimpleBellPageParser, SimpleBellPageParser>()
                .RegisterType<ISimpleBellHttpCaller, SimpleBellHttpCaller>()
                //  .RegisterType<ISimpleBellHttpCaller, S>()
                .RegisterType<IEngineConfiguration, EngineConfiguration>();
        }


        public void GetCustomerPromotionStatus()
        {

            var input = _container.Resolve<IEngineInputHandler>();

            var customerList = input.GetCustomerList();

            SimpleBellHttpCaller simpleBellWebService = new SimpleBellHttpCaller(_logger, _container.Resolve<IConfigurationFactory>().GetEngineConfiguration(),
                _container.Resolve<ISimpleBellPageParser>());
            var outputHandler = _container.Resolve<IConfigurationFactory>().GetOutputHandler();

            var sessionId = simpleBellWebService.GetSessionId();

            IProcessingEngine promotionFetchProcessingEngine = new PromotionStatusEngine(_container.Resolve<ISimpleBellHttpCaller>(), _container.Resolve<ISimpleBellPageParser>());
            foreach (var customer in customerList)
            {
                try
                {
                    var result = promotionFetchProcessingEngine.ProcessCustomer(customer, sessionId);
                    if (result.ErrorCode == ErrorCodes.SessionExpired)
                    {
                        // get Session again and Repean CustomerAcct
                    }

                    if (result.ErrorCode == ErrorCodes.NoError)
                        outputHandler.Sucess(customer);
                    else
                    {
                        outputHandler.Failure(customer, result.ErrorDetail);
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error("GetCustomerPromotionStatus error", ex);
                }

            }
        }

        public void UncheckUnlimitedVas()
        {
            var input = _container.Resolve<IEngineInputHandler>();
            var customerList = input.GetCustomerList();
            SimpleBellHttpCaller simpleBellWebService = new SimpleBellHttpCaller(_logger, _container.Resolve<IConfigurationFactory>().GetEngineConfiguration(),
                _container.Resolve<ISimpleBellPageParser>());
            var outputHandler = _container.Resolve<IConfigurationFactory>().GetOutputHandler();
            var sessionId = simpleBellWebService.GetSessionId();

            IProcessingEngine unlimitedVasRemovalEngine = new UnlimitedVasRemovalEngine(_container.Resolve<ISimpleBellHttpCaller>(), _container.Resolve<ISimpleBellPageParser>());
            foreach (var customer in customerList)
            {
                try
                {

                    _logger.Information("UniqueId=[{0}] CustomerID[{1}]", customer.uniqueId, customer.CustomerB1Number);
                    var result = unlimitedVasRemovalEngine.ProcessCustomer(customer, sessionId);
                    _logger.Information("ProcessCustomer Finished");
                    _logger.Debug(result.ErrorCode.ToString());
                    _logger.Debug(result.ErrorDetail.ToString());
                    if (result.ErrorCode == ErrorCodes.SessionExpired)
                    {
                        sessionId = simpleBellWebService.GetSessionId();
                        result = unlimitedVasRemovalEngine.ProcessCustomer(customer, sessionId);
                        if (result.ErrorCode == ErrorCodes.SessionExpired)
                        {
                            outputHandler.Failure(customer, result.ErrorDetail);
                        }
                    }

                    if (result.ErrorCode == ErrorCodes.NoError)
                        outputHandler.Sucess(customer, result.ErrorDetail);
                    else
                    {
                        outputHandler.Failure(customer, result.ErrorDetail);
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex.Message);
                    _logger.Error("UncheckUnlimitedVas error", ex);
                }

            }
            return;
        }

        public void ProcessDarkTVMigration()
        {
            var input = _container.Resolve<IEngineInputHandler>();
            var customerList = input.GetCustomerList();
            SimpleBellHttpCaller simpleBellWebService = new SimpleBellHttpCaller(_logger, _container.Resolve<IConfigurationFactory>().GetEngineConfiguration(),
                _container.Resolve<ISimpleBellPageParser>());
            var outputHandler = _container.Resolve<IConfigurationFactory>().GetOutputHandler();
            var sessionId = simpleBellWebService.GetSessionId();

            IProcessingEngine darkTvMigrationEngine = new DarkTVMigrationEngine(_container.Resolve<ISimpleBellHttpCaller>(), _container.Resolve<ISimpleBellPageParser>(), _logger);
            foreach (var customer in customerList)
            {
                if (input.IsAllowToProcess(customer))
                {
                    _logger.Information("UniqueId=[{0}] CustomerID[{1}] Allow <-- Ready To Read", customer.uniqueId, customer.CustomerB1Number);
                    input.ReadyToRead(customer);
                }
                else
                {
                    _logger.Information("UniqueId=[{0}] CustomerID[{1}] Not Allow <-- Processed by Other Automation", customer.uniqueId, customer.CustomerB1Number);
                    continue;
                }
                try
                {

                    _logger.Information("UniqueId=[{0}] CustomerID[{1}]", customer.uniqueId, customer.CustomerB1Number);
                    var result = darkTvMigrationEngine.ProcessCustomer(customer, sessionId);
                    _logger.Information("ProcessCustomer Finished");
                    _logger.Information(result.ErrorCode.ToString());
                    _logger.Information(result.ErrorDetail.ToString());
                    if (result.ErrorCode == ErrorCodes.SessionExpired)
                    {
                        sessionId = simpleBellWebService.GetSessionId();
                        result = darkTvMigrationEngine.ProcessCustomer(customer, sessionId);
                        if (result.ErrorCode == ErrorCodes.SessionExpired)
                        {
                            outputHandler.Failure(customer, result.ErrorDetail);
                        }
                    }

                    if (result.ErrorCode == ErrorCodes.NoError)
                        outputHandler.Sucess(customer, result.ErrorDetail);
                    else
                    {
                        outputHandler.Failure(customer, result.ErrorDetail);
                        outputHandler.LogHtmlFile(customer,result.ErrorPage);
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex.Message);
                    _logger.Error("UncheckUnlimitedVas error", ex);
                }

            }
            return;
        }
    }
}
