﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleWebScrapper.Engine.Models
{
   public class UnlimitedUsageProductDetails
    {
        public  string PromotionCode { get; set; }

        public string PromotionName { get; set; }
    }
}
