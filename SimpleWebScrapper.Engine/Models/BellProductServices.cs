﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleWebScrapper.Engine.Models
{
    public class BellProductService
    {
        public string ProductName { get; set; }

        public string ProductUrl { get; set; }
    }
}
