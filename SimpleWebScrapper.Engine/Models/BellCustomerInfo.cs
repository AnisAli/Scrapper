﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleWebScrapper.Engine.Models
{
    public class BellCustomerInfo
    {
        public int CustomerId { get; set; }

        public string CustomerIdentifier { get; set; }

        public IList<BellProductService> ProductServices { get; set; }
    }
}
