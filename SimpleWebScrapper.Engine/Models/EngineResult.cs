﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleWebScrapper.Engine.Contracts;

namespace SimpleWebScrapper.Engine.Models
{
    public class EngineResult:IEngineResult
    {
        public string CustomerId { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorDetail { get; set; }
        public string ErrorPage { get; set; }
    }
}
