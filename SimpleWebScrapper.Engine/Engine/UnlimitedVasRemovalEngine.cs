﻿using SimpleWebScrapper.Engine.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleWebScrapper.Common;
using SimpleWebScrapper.Engine.Models;

namespace SimpleWebScrapper.Engine.Engine
{
    public class UnlimitedVasRemovalEngine : IProcessingEngine
    {
        private readonly ISimpleBellHttpCaller _bellWebService;
        private readonly ISimpleBellPageParser _pageParser;

        public UnlimitedVasRemovalEngine(ISimpleBellHttpCaller bellWebService, ISimpleBellPageParser pageParser)
        {
            _bellWebService = bellWebService;
            _pageParser = pageParser;
        }


        public IEngineResult ProcessCustomer(Customer customer, string SessionId)
        {
            // 1- Search Customer ^done
            // 2- go Edit Page (if Edit is disbable, we send failure to output handler) (done)
            // 3- check is UnilimitedVas checked or not (if not then we success) (done)
            // 4- if it is checked then we will make it uncheck

            try
            {

                
                var searchResult = this._bellWebService.SearchCustomerPage(customer.CustomerB1Number, SessionId);
                if (this._pageParser.IsSessionExpired(searchResult))
                //throw new Exception("Session Expired");{
                {
                    return new EngineResult()
                    {
                        ErrorCode = ErrorCodes.SessionExpired,
                        ErrorDetail = "Session Expired"
                    };
                }

                if (!_pageParser.IsCustomerFound_SearhPageParse(searchResult)) // if customer not found
                    throw new Exception("Customer not Found");

                if (!this._bellWebService.IsAdditionalServiceEditAllow(SessionId))  //edit disabled
                {
                    return new EngineResult()
                    {
                        ErrorCode = ErrorCodes.EditisDisabled,
                        ErrorDetail = "Edit button is not enable"
                    };
                }

      


                var editPage = this._bellWebService.GetValueEditPage(SessionId);

               /////////////////////////////
                  // Case1: Unlimited Plan Exist then Uncheck
                  // Case2: Unlimited Plan Not exist but 20 Gb plan Exist then Uncheck
                  // case3: Both unlimited and 20gb extra not exist then successful return without submit any request to server
         
                /////////////

                if (!this._pageParser.ValueEditPageParser.IsUnlimitedUsagePlanExist(editPage) && 
                        !this._pageParser.ValueEditPageParser.Is20GbExtraUsagePlanExist(editPage)) // if unlimited plan is already unchecked
                {

                    return new EngineResult()
                    {
                        ErrorCode = ErrorCodes.NoError,
                        ErrorDetail = "Already unchecked"
                    };
                }


                var result = this._bellWebService.Submit_UnlimitedPlan_Uncheck(SessionId); // submit request by removing unlimited plan varibale in post message plus 20G extra plan variable

                if(result == false)
                {
                    return new EngineResult()
                    {
                        ErrorCode = ErrorCodes.UnExpectedPage,
                        ErrorDetail = "Submit Unlimited Plan uncheck not done"
                    };
                }


                //Temp: Remove this after post changes
                return new EngineResult()
                {
                    ErrorCode = ErrorCodes.NoError,
                    ErrorDetail = "Successfully Unchecked"
                };


            }
            catch (Exception ex)
            {
                return new EngineResult()
                {
                    ErrorCode = ErrorCodes.UnExpectedPage,
                    ErrorDetail = ex.Message
                };
            }


        }
    }
}
