﻿using SimpleWebScrapper.Engine.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleWebScrapper.Common;
using SimpleWebScrapper.Engine.Models;
using Serilog;

namespace SimpleWebScrapper.Engine.Engine
{
    public class DarkTVMigrationEngine : IProcessingEngine
    {
        private readonly ISimpleBellHttpCaller _bellWebService;
        private readonly ISimpleBellPageParser _pageParser;
        private readonly ILogger _logger;

        public DarkTVMigrationEngine(ISimpleBellHttpCaller bellWebService, ISimpleBellPageParser pageParser, ILogger logger)
        {
            _bellWebService = bellWebService;
            _pageParser = pageParser;
            _logger = logger;
        }


        public IEngineResult ProcessCustomer(Customer customer, string SessionId)
        {

            try
            {

                #region Search Customer
                /// ******************** Search Customer *****************************
                _logger.Debug("Process Customer Started");
                _logger.Debug("Search Customer {0} for Scenerio {1}", customer.CustomerB1Number, customer.scenerioType);

                var searchResult = this._bellWebService.SearchCustomerPage(customer.CustomerB1Number, SessionId);

                if (this._pageParser.IsSessionExpired(searchResult))
                {
                    return new EngineResult()
                    {
                        ErrorCode = ErrorCodes.SessionExpired,
                        ErrorDetail = "Session Expired"
                    };
                }
                
                
                var result =  _pageParser.ParseCustomerInfoPage(searchResult);
                _logger.Information("Result [" + customer.CustomerB1Number + "] + Name["+ result.CustomerIdentifier  +"]");



                if (!_pageParser.IsCustomerFound_SearhPageParse(searchResult)) // if customer not found
                    throw new Exception("Customer not Found");

                #endregion

                #region Product list Parsing
                //step1: Open Product Page
                //setp2: read the list for Ontario or Quebec FibeTV 50 (FTTH) and grap the hrep and read Ontrio or Qubec

                var productPage = _bellWebService.GetProductViewInfoPage(SessionId);

                var province = _pageParser.DarktvProductPageParser.GetProvince(productPage);

                var url = _pageParser.DarktvProductPageParser.GetFirstElementUrl(productPage);

                _logger.Debug("Customer Province = {0}", province);

                //string url = "/pegasus?component=productLink&amp;page=accounts_ViewProductInfo&amp;service=direct&amp;session=T&amp;sp=S0000000003681747";

                #endregion


                /// ******************** Identified Scenerio Type **********************
                if (customer.scenerioType == 1)
                    return this.ProcessScenerio1(customer, SessionId, province, productPage);
                else if (customer.scenerioType == 2)
                    return this.ProcessScenerio2(customer, SessionId, url);
                else
                {
                    //Temp: Remove this after post changes
                    return new EngineResult()
                    {
                        ErrorCode = ErrorCodes.InvalidScenerioType,
                        ErrorDetail = "Invalid Scenerio Type" + customer.scenerioType
                    };
                }

            }
            catch (Exception ex)
            {
                return new EngineResult()
                {
                    ErrorCode = ErrorCodes.UnExpectedPage,
                    ErrorDetail = ex.Message
                };
            }
        }

        private IEngineResult ProcessScenerio1(Customer customer, string sessionId, string Province, string ProductPage)  //Province = ON or QC
        {

            // Step1: Go to Product/SpeedChange by parsing URL on ProductPage

            var SpeedChangeURL = this._pageParser.DarktvProductPageParser.GetSpeedProductChangeURL(ProductPage);

            if (SpeedChangeURL == "")
            {
                return new EngineResult()
                {
                    ErrorCode = ErrorCodes.SpeedProductChangeNotAllowed,
                    ErrorDetail = "SpeedProduct Change not allowed",
                    ErrorPage = ProductPage
                };
            }
                

            var ProductSpeedChangePage = this._bellWebService.GetProductSpeedChangePage(SpeedChangeURL, sessionId);
      
            // Step2: Post using (Qualify Method) /// *** critical **
           
           var PageAfterQualifyPress = this._bellWebService.PressQualifyButton(ProductSpeedChangePage,sessionId);

            if (!this._pageParser.DarktvProductPageParser.IsQualifyButtonOperationSucess(PageAfterQualifyPress))
            {
                return new EngineResult()
                {
                    ErrorCode = ErrorCodes.UnExpectedPage,
                    ErrorDetail = "Return Page is Invalid, after Qualify Button pressed",
                    ErrorPage = PageAfterQualifyPress
 
                };
            }

            // Check Ontario FibeTV 15 or Quebec FibeTV15 Product exist in the dropdown List
            if (!this._pageParser.DarktvProductPageParser.IsMigrationProductExist(PageAfterQualifyPress))
            {
                return new EngineResult()
                {
                    ErrorCode = ErrorCodes.UnExpectedPage,
                    ErrorDetail = "Ontario or Quebec Fibe TV 15 product not found, after Qualify Button pressed",
                    ErrorPage = PageAfterQualifyPress

                };
            }

            // Select Product either Ontario or Qubec Fibe TV Product
            var PageAfterProductSelect = this._bellWebService.SelectDarkTVProduct(PageAfterQualifyPress, sessionId, Province);

            if (!this._pageParser.DarktvProductPageParser.IsSelectProductOperationSuccess(PageAfterProductSelect))
            {
                return new EngineResult()
                {
                    ErrorCode = ErrorCodes.UnExpectedPage,
                    ErrorDetail = "Return Page is Invalid, after Selecting product",
                    ErrorPage = PageAfterProductSelect

                };
            }

            // Check mark VOIP , readme BCRIS and Select Price Structure
            var PageAfterPricingStructureSelect = this._bellWebService.SelectPricingWithVOIPIndicator(PageAfterProductSelect, sessionId, Province);

            if (!this._pageParser.DarktvProductPageParser.IsPricingandVoipSubmitSuccess(PageAfterPricingStructureSelect))
            {
                return new EngineResult()
                {
                    ErrorCode = ErrorCodes.UnExpectedPage,
                    ErrorDetail = "Return Page Invalid, After Checked the checkbox for “I have read the messages ..",
                    ErrorPage = PageAfterPricingStructureSelect

                };
            }

            // Press Continue Button
            var PageAfterContinueButtonPressPost = this._bellWebService.PressContinueButtonPressAfterPriceandProductChange(PageAfterPricingStructureSelect, sessionId, Province);

            if (!this._pageParser.DarktvProductPageParser.IsContinueRetunPageSuccess(PageAfterContinueButtonPressPost))
            {
                return new EngineResult()
                {
                    ErrorCode = ErrorCodes.UnExpectedPage,
                    ErrorDetail = "Existing - $0/m Fibe TV network access is not available for this customer",
                    ErrorPage = PageAfterContinueButtonPressPost

                };
            }

            // Click SelfInstall 
            var ModemPageReturnByPostingSelfInstall = this._bellWebService.SelectSelfInstallOptionWithNoBattery(PageAfterContinueButtonPressPost, sessionId);

            if (!this._pageParser.DarktvProductPageParser.IsSelfInstallReturnPageSuccess(ModemPageReturnByPostingSelfInstall))
            {
                return new EngineResult()
                {
                    ErrorCode = ErrorCodes.UnExpectedPage,
                    ErrorDetail = "Return Page is Invalid, after selecting SelfInstall Type",
                    ErrorPage = ModemPageReturnByPostingSelfInstall

                };
            }

            // Press Next
            var AdditionalServicesAndPromotionPage = this._bellWebService.MoveToNextStepAfterSelfInstallType(ModemPageReturnByPostingSelfInstall, sessionId);

            if (!this._pageParser.DarktvProductPageParser.IsAdditionalServicePage(AdditionalServicesAndPromotionPage))
            {
                return new EngineResult()
                {
                    ErrorCode = ErrorCodes.UnExpectedPage,
                    ErrorDetail = "Return Page is Invalid, after selecting SelfInstall Type",
                    ErrorPage = AdditionalServicesAndPromotionPage

                };
            }

            // Press Next
            var ConfirmationDetailsPage = this._bellWebService.MoveToNextFromAddtionalServicePage(AdditionalServicesAndPromotionPage, sessionId);

            if (!this._pageParser.DarktvProductPageParser.IsSubmittionPageAfterAddtionalService(ConfirmationDetailsPage))
            {
                return new EngineResult()
                {
                    ErrorCode = ErrorCodes.UnExpectedPage,
                    ErrorDetail = "Return Page is Invalid, after next button press from addtiobal service page",
                    ErrorPage = ConfirmationDetailsPage

                };
            }

            //************************************* TEMP DISABLED [start] *******************************//
            /*
        var SuccessPage = this._bellWebService.SubmitFinalOrderofScenerio1(ConfirmationDetailsPage, sessionId);

        if (!this._pageParser.DarktvProductPageParser.IsOrderSuccessfullySubmitted(SuccessPage))
        {
            return new EngineResult()
            {
                ErrorCode = ErrorCodes.UnExpectedPage,
                ErrorDetail = "Return Page is Invalid, after final submit of product change",
                ErrorPage = SuccessPage

            };
        }
        //************************************* TEMP DISABLED [end] *******************************/
               
        return new EngineResult()
        {
            ErrorCode = ErrorCodes.NoError,
            ErrorDetail = "Scenerio-1 ran Successfully  -WithoutSubmittion "
        };


    }

    private IEngineResult ProcessScenerio2(Customer customer, string sessionId, string url)
    {

        // step1: Send Get Request on given URL
           var FibeTVpricestructurePage = _bellWebService.GetPricingStructurePage(url, sessionId);

        // Step2: Check edit is enable or disable 
        // if edit is disbaled then send error 
            if (!_pageParser.DarktvProductPageParser.IsPriceStructurePageEditEnable(FibeTVpricestructurePage))
            {
                return new EngineResult()
                {
                    ErrorCode = ErrorCodes.EditisDisabled,
                    ErrorDetail = "Edit button disbaled on pricing Structure page - Scenerio 2"
                };
            }


        //step3: Click on Edit Button
            var EditPage = _bellWebService.GetEditPriceStructurePage(url, sessionId);

        // Step4: Parse the list and look for value ="PS114" 

            if (!_pageParser.DarktvProductPageParser.isExisting0FibeTVExist(EditPage))
            {
                return new EngineResult()
                {
                    ErrorCode = ErrorCodes.InvalidPriceStructure,
                    ErrorDetail = "Existing - $0/m Fibe TV network is not available for this customer"
                };

            }

            //************************************* TEMP DISABLED [start] *******************************//
                /*
                    // Step 5: select PS041 and post the request to Server
                    var FinalSubmitReturnPage = _bellWebService.SubmitFinalOrderofScenerio2(url, sessionId);

                    if (!this._pageParser.DarktvProductPageParser.IsOrderSuccessfullySubmittedForScenerio2(FinalSubmitReturnPage))
                    {
                        return new EngineResult()
                        {
                            ErrorCode = ErrorCodes.UnExpectedPage,
                            ErrorDetail = "Return Page is Invalid, after final submit of product change scenerio2",
                            ErrorPage = FinalSubmitReturnPage

                        };
                    }
                    */
            //************************************* TEMP DISABLED [end] *******************************//
            return new EngineResult()
            {
                ErrorCode = ErrorCodes.NoError,
                ErrorDetail = "Scenerio-2 ran Successfully - WithoutSubmittion "
            };



    


            return null;

        }


    }

}
