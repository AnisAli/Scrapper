﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleWebScrapper.Common;
using SimpleWebScrapper.Engine.Contracts;
using SimpleWebScrapper.Engine.Models;

namespace SimpleWebScrapper.Engine.Engine
{
   public class PromotionStatusEngine : IProcessingEngine
   {
           private readonly ISimpleBellHttpCaller _bellWebService;
           private readonly ISimpleBellPageParser _pageParser;
        public PromotionStatusEngine(ISimpleBellHttpCaller bellWebService, ISimpleBellPageParser pageParser)
        {
            _bellWebService = bellWebService;
            _pageParser = pageParser;
        }


        public IEngineResult ProcessCustomer(Customer customer, string sessionID)
        {
            var productList =  this._bellWebService.GetCustomerProductServices(customer.CustomerB1Number, sessionID);
            string unlimitedUsagePlanurl="";

            var bellProductService = productList.FirstOrDefault(c => c.ProductName.Contains("Unlimited Usage Plan"));
            if (bellProductService != null)
            {
                unlimitedUsagePlanurl = bellProductService.ProductUrl;
                unlimitedUsagePlanurl = unlimitedUsagePlanurl.Replace("&amp;", "&");
            }
            else
            {
                return new EngineResult()
                {
                    ErrorCode = ErrorCodes.ProductNotFound,
                    ErrorDetail = "Unlimited Usage Plan Product Not Found"
                };
            }

            var result = this._bellWebService.GetRequest("https://simple.int.bell.ca"+unlimitedUsagePlanurl,sessionID, "https://simple.int.bell.ca/accounts_ViewProductInfo.html");


           var unlimitedUsageDetail =   this._pageParser.ProductDetailParser.ParseUnlimitedProductDetails(result);
            if (unlimitedUsageDetail.PromotionCode == "PBW28")
                return new EngineResult()
                {
                    ErrorCode = ErrorCodes.NoError
                };
            else
            {
                return new EngineResult()
                {
                    ErrorCode = ErrorCodes.EngineExpectedError,
                    ErrorDetail = unlimitedUsageDetail.PromotionCode
                };
            }
        }

       
   }
}
