﻿using System.Collections.Generic;
using SimpleWebScrapper.Engine.Models;

namespace SimpleWebScrapper.Engine
{
    public interface ISimpleBellHttpCaller
    {
        string GetSessionId();
        string GetRequest(string url, string loginSessionId, string referer = "");

        IList<BellProductService> GetCustomerProductServices(string customerId, string sessionId);
        string GetValueEditPage(string inSession);
        string GetProductViewInfoPage(string inSession);
        bool IsAdditionalServiceEditAllow(string inSessionId);
        string SearchCustomerPage(string customerId, string sessionId);
        bool Submit_UnlimitedPlan_Uncheck(string inSession);

        //Require in DarkTV Project
        string GetPricingStructurePage(string url, string inSession);
        string GetProductSpeedChangePage(string url, string inSession);
        string GetEditPriceStructurePage(string url, string inSession);
        string PressQualifyButton(string productSpeedPageHtml, string inSession);
        string SelectDarkTVProduct(string pageAfterQualifyClick, string inSession, string province);
        string SelectPricingWithVOIPIndicator(string pageAfterQualifyClick, string inSession, string province);
        string PressContinueButtonPressAfterPriceandProductChange(string pageAfterQualifyClick, string inSession, string province);
        string SelectSelfInstallOptionWithNoBattery(string modemPage, string inSession);
        string MoveToNextStepAfterSelfInstallType(string modemPage, string inSession);
        string MoveToNextFromAddtionalServicePage(string addtionalServicePage, string inSession);
        string SubmitFinalOrderofScenerio1(string ConfirmationDetailPage, string inSession);
        string SubmitFinalOrderofScenerio2(string ConfirmationDetailPage, string inSession);
    }
}