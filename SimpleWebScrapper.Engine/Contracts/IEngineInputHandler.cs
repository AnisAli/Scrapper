﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleWebScrapper.Common;

namespace SimpleWebScrapper.Engine
{
    public interface IEngineInputHandler
    {
        List<Customer> GetCustomerList();

        bool ReadyToRead(Customer cust);

         bool IsAllowToProcess(Customer cust);
       
    }
}