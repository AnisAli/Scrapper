﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleWebScrapper.Engine.Models;
using SimpleWebScrapper.Engine.PageParsers;

namespace SimpleWebScrapper.Engine.Contracts
{
    public interface ISimpleBellPageParser
    {

        string ParseSessionIdFronLoginResponse(string htmlPage);
        BellCustomerInfo ParseLoginPageResponse(string htmlPage);
        BellCustomerInfo ParseCustomerInfoPage(string htmlPage);

        SimpleBellProductDetailParser ProductDetailParser { get; }
        ValueAddedServicePageParser ValueAddedServicePageParser { get; }
        ValueEditPageParser ValueEditPageParser { get; }
        DarkTV_ProductPageParser DarktvProductPageParser { get; }
        bool IsSessionExpired(string htmlPage);
        IList<BellProductService> ParseProductPage(string htmlPage);

        bool IsCustomerFound_SearhPageParse(string htmlPage);
       
    }


}
