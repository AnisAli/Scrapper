﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleWebScrapper.Engine
{
    public interface IEngineConfiguration
    {
        string UserName { get; }

        string Password { get; }

        string SimpleWebURL { get; }


        bool ReloadRepoEachDay { get; }

        bool SucessFilterOnly { get; }  // this property is used to check only successlist for filtering input file otherwise it will use both Succcess and Failure list for filtering input data
    }
}
