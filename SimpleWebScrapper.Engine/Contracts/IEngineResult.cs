﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleWebScrapper.Engine.Contracts
{
    public interface IEngineResult
    {
        string CustomerId { get; set; }

        int ErrorCode { get; set; }

        string ErrorDetail { get; set; }

        string ErrorPage { get; set; }
    }
}
