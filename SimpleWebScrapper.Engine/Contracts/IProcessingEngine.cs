﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleWebScrapper.Common;

namespace SimpleWebScrapper.Engine.Contracts
{
    public interface IProcessingEngine
    {
        IEngineResult ProcessCustomer(Customer customer, string SessionId);
    }
}
