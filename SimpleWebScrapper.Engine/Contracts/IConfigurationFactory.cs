﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleWebScrapper.Common;
namespace SimpleWebScrapper.Engine
{
    public interface IConfigurationFactory
    {
        ICustomerRepository GetCustomerRepository();
        IEngineOutputHandler GetOutputHandler();
        IEngineConfiguration GetEngineConfiguration();
    }
}
