﻿namespace SimpleWebScrapper.Engine
{
    public interface ISimpleWebScrapperEngine
    {
        void  GetCustomerPromotionStatus();

        void UncheckUnlimitedVas();

        void ProcessDarkTVMigration();

    }
}