﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleWebScrapper.Engine.Configuration
{
    class EngineConfiguration :IEngineConfiguration
    {

        public EngineConfiguration(IConfigurationFactory configFactory)
        {
            UserName = configFactory.GetEngineConfiguration().UserName;  // read from App Config

            Password = configFactory.GetEngineConfiguration().Password;  // read from App Config

            SimpleWebURL = configFactory.GetEngineConfiguration().SimpleWebURL;  // read from App Config

            ReloadRepoEachDay = configFactory.GetEngineConfiguration().ReloadRepoEachDay;  // read from App Config

            SucessFilterOnly = configFactory.GetEngineConfiguration().SucessFilterOnly;  // read from App Config

        }

        public string UserName { get; }
        public string Password { get; }
        public string SimpleWebURL { get; }
        public bool ReloadRepoEachDay { get; }
        public bool SucessFilterOnly { get; }
    }
}
