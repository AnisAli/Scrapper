﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;

namespace SimpleWebScrapper.Engine.Configuration
{
    public class SimpleWSEngineConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("customerRepository", IsRequired = true)]
        public ProviderTypeElement CustomerRepository
        {
            get { return (ProviderTypeElement)base["customerRepository"]; }
            set { base["customerRepository"] = value; }
        }

        [ConfigurationProperty("engineOutputHandler", IsRequired = true)]
        public ProviderTypeElement EngineOutputHandler
        {
            get { return (ProviderTypeElement)base["engineOutputHandler"]; }
            set { base["engineOutputHandler"] = value; }
        }

        [ConfigurationProperty("engineConfiguration", IsRequired = true)]
        public EngineConfigurationElement EngineConfiguration
        {
            get { return (EngineConfigurationElement)base["engineConfiguration"]; }
            set { base["engineConfiguration"] = value; }
        }

    }

    public class ProviderTypeElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true, IsKey = true)]
        public string Name
        {
            get { return (string) base["name"]; }
            set { base["name"] = value; }
        }

        [ConfigurationProperty("type", IsRequired = true, IsKey = true)]
        public string Type
        {
            get { return (string) base["type"]; }
            set { base["type"] = value; }
        }
    }

    public class EngineConfigurationElement : ConfigurationElement, IEngineConfiguration
    {
        [ConfigurationProperty("username", IsRequired = true, IsKey = true)]
        public string UserName
        {
            get { return (string)base["username"]; }
            set { base["username"] = value; }
        }

        [ConfigurationProperty("password", IsRequired = true, IsKey = true)]
        public string Password
        {
            get { return (string)base["password"]; }
            set { base["password"] = value; }
        }

        [ConfigurationProperty("reloadRepoEachDay", IsRequired = true, IsKey = true)]
        public bool ReloadRepoEachDay
        {
            get { return (bool)base["reloadRepoEachDay"]; }
            set { base["reloadRepoEachDay"] = value; }
        }


        [ConfigurationProperty("sucessFilterOnly", IsRequired = false,DefaultValue = true, IsKey = true)]  
        // this filter is used to reload the repo after removing successlist only otherewise reload repo by removing both success and failure customer id
        public bool SucessFilterOnly
        {
            get { return (bool)base["sucessFilterOnly"]; }
            set { base["sucessFilterOnly"] = value; }
        }

      
        [ConfigurationProperty("simpleWebUrl", IsRequired = true, IsKey = true)]
        public string SimpleWebURL
        {
            get { return (string)base["simpleWebUrl"]; }
            set { base["simpleWebUrl"] = value; }
        }
    }
}
