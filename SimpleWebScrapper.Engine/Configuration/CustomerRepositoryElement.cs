using System.Configuration;

namespace SimpleWebScrapper.Engine.Configuration
{
    public class CustomerRepositoryElement : ProviderTypeElement
    {
        [ConfigurationProperty("inputFile", IsRequired = false, IsKey = true)]
        public string InputFile
        {
            get { return (string) base["inputFile"]; }
            set { base["inputFile"] = value; }
        }


        //TODO: it might contain connection string for reading input from Database......

        [ConfigurationProperty("connectionString", IsRequired = false, IsKey = true)]
        public string ConnectionString
        {
            get { return (string)base["connectionString"]; }
            set { base["connectionString"] = value; }
        }

        //[ConfigurationProperty("filterSuccessOnly", DefaultValue = "false")]
        //public string FilterSuccessOnly
        //{
        //    get { return (string) base["filterSuccessOnly"]; }
        //    set { base["inpufilterSuccessOnlytFile"] = value; }
        //}
    }
}