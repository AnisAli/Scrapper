﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleWebScrapper.Engine.PageParsers
{
    public class ValueEditPageParser
    {

        public bool IsUnlimitedUsagePlanExist(string htmlPage)
        {
            var doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(htmlPage);
            var UnlimitedPlanTable = doc.DocumentNode.SelectNodes("//table[@id='table_BWUNL']");  // check for unlimted plan table
            if (UnlimitedPlanTable == null) 
                return false;    
            else  // check is unlimited plan check box checked or not
            {
                try
                {
                    var st = UnlimitedPlanTable[0].InnerHtml;
                    bool unlimitedPlanChecked = true;
                    doc.LoadHtml(st);
                    var nodet = doc.DocumentNode.SelectNodes("//input[@id='BWUNL']");
                    if (nodet != null  )
                    {
                        if(nodet[0].Attributes["checked"] == null)
                            unlimitedPlanChecked = false;
                        else if (!nodet[0].Attributes["checked"].Value.Contains("checked"))
                            unlimitedPlanChecked = false;
                    }
                    else
                        unlimitedPlanChecked = false;

                    return unlimitedPlanChecked;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }


        }

        public bool Is20GbExtraUsagePlanExist(string htmlPage)
        {

            // Unlimited Plan Checkbox exist means not Null (BBUL) 
            // Unlimited Plan Checkbox is checked or Not 

            var doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(htmlPage);
            var Extra20GBPlanTable = doc.DocumentNode.SelectNodes("//table[@id='table_BWGBB']");  // check for unlimted plan table
            if (Extra20GBPlanTable == null)
                return false;  // NOT EXIST
            else  // check is unlimited plan check box checked or not
            {
                try
                {
                    var st = Extra20GBPlanTable[0].InnerHtml;
                    bool extra20GBPlanChecked = true;
                    doc.LoadHtml(st);
                    var nodet = doc.DocumentNode.SelectNodes("//input[@id='BWGBB']");
                    if (nodet != null)
                    {
                        if (nodet[0].Attributes["checked"] == null) //element exist
                            extra20GBPlanChecked = false;
                        else if (!nodet[0].Attributes["checked"].Value.Contains("checked")) //element value is checked or not
                            extra20GBPlanChecked = false;
                    }
                    else
                        extra20GBPlanChecked = false;

                    return extra20GBPlanChecked;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }


        public string ExtractPostMessage(string htmlPage)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(htmlPage);
            //formid
            var formidCheck = doc.DocumentNode.SelectNodes("//input[@name='formids']");
            if (formidCheck == null) return "";
            HtmlNode node = null;
            if (formidCheck.Count > 1)
                node = doc.DocumentNode.SelectNodes("//input[@name='formids']")[1];
            else
                return "";

            String tempFormID = node.Attributes["value"].Value;
            tempFormID = "formids=" + tempFormID.Replace(",", "%2C");

            //seedid
            var seedsidCheck = doc.DocumentNode.SelectNodes("//input[@name='seedids']");
            if (seedsidCheck == null) return "";
            node = null;
            if (formidCheck.Count > 1)
                node = doc.DocumentNode.SelectNodes("//input[@name='seedids']")[1];
            else
                return "";

            String tempSeedID = node.Attributes["value"].Value;
            tempSeedID = tempSeedID.Replace("/", "%2F");
            tempSeedID = tempSeedID.Replace("=", "%3D");
            tempSeedID = tempSeedID.Replace("+", "%2B");
            tempSeedID = tempSeedID.Replace("!", "%21");
            tempSeedID = tempSeedID.Replace("#", "%23");
            tempSeedID = tempSeedID.Replace("$", "%24");
            tempSeedID = tempSeedID.Replace("*", "%2A");
            tempSeedID = tempSeedID.Replace(",", "%2C");
            tempSeedID = tempSeedID.Replace("-", "%2D");
            tempSeedID = tempSeedID.Replace(".", "%2E");
            tempSeedID = tempSeedID.Replace(":", "%3A");
            tempSeedID = tempSeedID.Replace(";", "%3B");
            tempSeedID = tempSeedID.Replace("<", "%3C");
            tempSeedID = tempSeedID.Replace(">", "%3E");
            tempSeedID = tempSeedID.Replace("?", "%3F");
            tempSeedID = tempSeedID.Replace("@", "%40");
            tempSeedID = tempSeedID.Replace("^", "%5E");
            tempSeedID = tempSeedID.Replace("_", "%5F");
            tempSeedID = "&seedids=" + tempSeedID;

            node = doc.DocumentNode.SelectNodes("//div[@id='vasSubmitFormhidden']")[0];
            doc.LoadHtml(node.InnerHtml);
            node = doc.DocumentNode;

            var CheckedBoxes = node.SelectNodes("//input[@type='hidden']");
            if (CheckedBoxes == null) return "";

            String T = "";
            String elename, elevalue = "";

            for (int i = 2; i < CheckedBoxes.Count; i++)
            {
                elename = node.SelectNodes("//input[@type='hidden']")[i].Attributes["name"].Value;
                elevalue = node.SelectNodes("//input[@type='hidden']")[i].Attributes["value"].Value;
                T += "&" + elename + "=" + elevalue;
            }

            String checkedIDs = GenerateExistingService_PostMessage(htmlPage);

            String finalstring = tempFormID + tempSeedID + T + checkedIDs;

            return finalstring;
        }

        private String GenerateExistingService_PostMessage(String insHtml)
        {

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(insHtml);
            var DivBordered = doc.DocumentNode.SelectNodes("//div[@class='borderedContent']");

            if (DivBordered == null) return "";

            HtmlNode node = doc.DocumentNode.SelectNodes("//div[@class='borderedContent']")[0];

            doc.LoadHtml(node.InnerHtml);

            node = doc.DocumentNode;

            var CheckedBoxes = node.SelectNodes("//input[@checked='checked']");
            if (CheckedBoxes == null) return "";

            String CheckedID;
            String FinalString = "";

            for (int i = 0; i < CheckedBoxes.Count; i++)
            {
                CheckedID = node.SelectNodes("//input[@checked='checked']")[i].Attributes["id"].Value;
                if (CheckedID == "BWUNL" || CheckedID == "BWGBB") continue; // remove Unlimited checkbox and 20GB Extra usage Plan checkbox from final post message
                FinalString += "&" + CheckedID + "=on";
            }
            return FinalString;
        }

        public bool IsSubmitPageSuccessful(string htmlPage)
        {
            var doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(htmlPage);

            var itemTablePWEBEnabled = doc.DocumentNode.SelectNodes("//table[@class='listing']");
            if (itemTablePWEBEnabled == null) // not added successfully
                return false;

            HtmlNode node = doc.DocumentNode.SelectNodes("//table[@class='listing']")[0];
            string s = node.InnerText;

            //if (s.ToUpper().Contains("UNLIMITED USAGE PLAN"))  // means Successfully Add VAS - now add in Database 
            //{
            //    //update table left

            //    TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.AddVAS_UpdateStatus(inUSER_ID, Constants.ADD_VAS.Status.__COMPLETE);
            //    Logging.Add(inUSER_ID, Constants.LOG.SUCCESS, Constants.LOG.SUCCESS);
            //    return true;
            //}
            //else  // means not add vas successfully - now add in logs and update status ERROR  
            //{

            //    TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.AddVAS_UpdateStatus(inUSER_ID, Constants.ADD_VAS.Status.__ERROR);
            //    Logging.Add(inUSER_ID, Constants.LOG.ERROR, "[code 0007] " + Constants.LOG.ErrorDescription.APPLY_VAS_ERROR);
            //    return false;
            //}

            return true;
        }
    }
}
