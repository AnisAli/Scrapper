﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleWebScrapper.Engine.PageParsers
{
    public class DarkTV_ProductPageParser
    {

        public string GetProvince(string productPage)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(productPage);
            var productLoopDiv = doc.DocumentNode.SelectNodes("//div[@id='productLoop']");

            if (productLoopDiv == null) return string.Empty;

            doc.LoadHtml(productLoopDiv[0].InnerHtml);

            var productTitleanchor = doc.DocumentNode.SelectNodes("//a[@id='productExpand']");
            if (productTitleanchor != null)
                if (productTitleanchor[0].InnerText.ToUpper().Contains("QUEBEC"))
                    return "QC";
                else if (productTitleanchor[0].InnerText.ToUpper().Contains("ONTARIO"))
                    return "ON";
                else
                    return string.Empty;

            return string.Empty;
        }

        public string GetFirstElementUrl(string productPage)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(productPage);
            var productLoopDiv = doc.DocumentNode.SelectNodes("//div[@id='productLoop']");

            if (productLoopDiv == null) return string.Empty;

            doc.LoadHtml(productLoopDiv[0].InnerHtml);
            var productLinkanchor = doc.DocumentNode.SelectNodes("//a[@id='productLink']");

            if (productLinkanchor != null)
                return productLinkanchor[0].Attributes["href"].Value.Replace("&amp;", "&");

            return string.Empty;

        }

        public string GetSpeedProductChangeURL(string productPage)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(productPage);
            var speedPrdLink = doc.DocumentNode.SelectNodes("//a[@id='productChangeLink']");

            if (speedPrdLink != null)
                return speedPrdLink[0].Attributes["href"].Value.Replace("&amp;", "&");

            return string.Empty;

        }

        public bool IsPriceStructurePageEditEnable(string fibeTVpricestructurePage)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(fibeTVpricestructurePage);
            var editButtonLink = doc.DocumentNode.SelectNodes("//a[@id='editButton']");

            if (editButtonLink == null)
                return false;

            if (editButtonLink[0].InnerHtml.ToUpper().Contains("DISABLED"))
                return false;

            return true;

        }


      



        public string GetExistingFibeTVNetworkAccessCode(string editPage)
        {
            //Anis
            var doc = new HtmlDocument();
            doc.LoadHtml(editPage);
            var selectDropBox = doc.DocumentNode.SelectNodes("//select[@id='pricingStructureDropDown']");

            if (selectDropBox == null)
                return "";


            HtmlAgilityPack.HtmlDocument doc1 = new HtmlAgilityPack.HtmlDocument();
            doc1.LoadHtml(selectDropBox[0].InnerHtml);

            var optionCollection = doc1.DocumentNode.SelectNodes("//option");

            foreach (var option in optionCollection)
            {
                if (option.NextSibling.InnerText.ToLower().Replace("\r\n", "") == "existing - $0/m fibe tv network access")
                {
                    return option.Attributes["value"].Value;
                }
           }
            return "";
        }

        public bool isExisting0FibeTVExist(string editPage)
        {

            var doc = new HtmlDocument();
            doc.LoadHtml(editPage);
            var editButtonLink = doc.DocumentNode.SelectNodes("//select[@id='pricingStructureDropDown']");

            if (editButtonLink == null)
                return false;

            if (editButtonLink[0].InnerHtml.ToLower().Contains("existing - $0/m fibe tv network access")) //PS114 : code for "Existing - $0/m Fibe TV network access"
                return true;

            return false;

        }

        public string ExtractGeneralPostMessage(string speedChangepage, string submitname = "", string formhiddenid = "formhidden")
        {

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(speedChangepage);
            //formid
            var formHiddenDiv = doc.DocumentNode.SelectNodes("//div[@id='" + formhiddenid + "']");
            if (formHiddenDiv == null) return "";

            //var allHiddenInputnodes = null;
            doc.LoadHtml(formHiddenDiv[0].InnerHtml);
            var allHiddenInputnodes = doc.DocumentNode.SelectNodes("//input[@type='hidden']");

            string Body = "";
            foreach (var inputbox in allHiddenInputnodes)
            {
                if (inputbox.Attributes["name"].Value == "formids")
                    Body += inputbox.Attributes["name"].Value + "=" + inputbox.Attributes["value"].Value.Replace(",", "%2C") + "&";
                else if (inputbox.Attributes["name"].Value == "seedids")
                    Body += inputbox.Attributes["name"].Value + "=" + EncodeHtmlCharacter(inputbox.Attributes["value"].Value) + "&";
                else if (inputbox.Attributes["name"].Value == "submitname")
                    Body += inputbox.Attributes["name"].Value + "=" + submitname + "&";
                else
                    Body += inputbox.Attributes["name"].Value + "=" + inputbox.Attributes["value"].Value + "&";
            }

            return Body;

        }


        private string ExtractCheckBoxesFromAddtionalServicePage(string additionalServicePage)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(additionalServicePage);
            var DivBordered = doc.DocumentNode.SelectNodes("//div[@class='borderedContent']");

            if (DivBordered == null) return "";

            HtmlNode node = doc.DocumentNode.SelectNodes("//div[@class='borderedContent']")[0];

            doc.LoadHtml(node.InnerHtml);

            node = doc.DocumentNode;

            var CheckedBoxes = node.SelectNodes("//input[@checked='checked']");
            if (CheckedBoxes == null) return "";

            String CheckedID;
            String FinalString = "";

            for (int i = 0; i < CheckedBoxes.Count; i++)
            {
                CheckedID = node.SelectNodes("//input[@checked='checked']")[i].Attributes["id"].Value;
                //if (CheckedID == "BWUNL" || CheckedID == "BWGBB") continue; // remove Unlimited checkbox and 20GB Extra usage Plan checkbox from final post message
                FinalString += "&" + CheckedID + "=on";
            }
            return FinalString;
        }

        private string ExtractDropBoxesFromAddtionalServicePage(string additonalServicePage)
        {
            // HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            //  doc.LoadHtml(additionalServicePage);
            string body = "";



            string value = SelectBoxValue(additonalServicePage, "promotions_INETC");
            if (value != "")
            {
                body += "&promotions_INETC=" + value;
            }

            value = SelectBoxValue(additonalServicePage, "promotions_BWGBB");
            if (value != "")
            {
                body += "&promotions_BWGBB=" + value;
            }

            value = SelectBoxValue(additonalServicePage, "BWGBB_BWQTY");
            if (value != "")
            {
                body += "&BWGBB_BWQTY_1=" + value;
            }

            value = SelectBoxValue(additonalServicePage, "promotions_BWUNL");
            if (value != "")
            {
                body += "&promotions_BWUNL=" + value;
            }

            return body;
        }

        public string ExtractMessageBodyFromAddditonalServicePage(string additionalServicePage)
        {
            string body = "";
            body += ExtractGeneralPostMessage(additionalServicePage, "nextButton");

            //POD01_PQTY
            body += "POD01_PQTY_1=";
            body += SelectBoxValue(additionalServicePage, "POD01_PQTY") + "&";

            body += ExtractCheckBoxesFromAddtionalServicePage(additionalServicePage);
            body += ExtractDropBoxesFromAddtionalServicePage(additionalServicePage);
            body = body.Replace("&&", "&");
            return body;
        }

        public string ExtractFinalSubmitScenerio1Body(string ConfirmationDetailsPage)
        {
            string body = "";
            body += ExtractGeneralPostMessage(ConfirmationDetailsPage, "submitButton");

            //agentId
            body += "agentId=";
            body += TextBoxValue(ConfirmationDetailsPage, "agentId") + "&";

            //dealerId
            body += "dealerId=";
            body += SelectBoxValue(ConfirmationDetailsPage, "dealerId");

            return body;
        }

        public string ExtractFinalSubmitScenerio2Body(string ConfirmationDetailsPage)
        {
            string body = "";
            body += ExtractGeneralPostMessage(ConfirmationDetailsPage, "submitButton");
            var code = GetExistingFibeTVNetworkAccessCode(ConfirmationDetailsPage);
            body += "pricingStructureDropDown="+ code + "&promotionDropDown=";
            return body;
        }
        public string ExtractExistingValueFromProductModemChangePage(string modemPage)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(modemPage);
            String body = "";

            //appartmentNumberField
            body += "appartmentNumberField=";
            body += TextBoxValue(modemPage, "appartmentNumberField") + "&";

            //floorNumberField
            body += "floorNumberField=";
            body += TextBoxValue(modemPage, "floorNumberField") + "&";

            //streetNumberField
            body += "streetNumberField=";
            body += TextBoxValue(modemPage, "streetNumberField") + "&";

            //streetNameField
            body += "streetNameField=";
            body += TextBoxValue(modemPage, "streetNameField") + "&";

            //streetTypeField
            body += "streetTypeField=";
            body += TextBoxValue(modemPage, "streetTypeField") + "&";

            //streetDirectionField
            body += "streetDirectionField=";
            body += TextBoxValue(modemPage, "streetDirectionField") + "&";

            //cityField
            body += "cityField=";
            body += TextBoxValue(modemPage, "cityField") + "&";

            //provinceSelect
            body += "provinceSelect=";
            body += SelectBoxValue(modemPage, "provinceSelect") + "&";

            //postalCodeField
            body += "postalCodeField=";
            body += TextBoxValue(modemPage, "postalCodeField") + "&";

            //instructions
            body += "instructions=";
            body += TextAreaValue(modemPage, "instructions") + "&";


            //servicesRadioGroup
            body += "servicesRadioGroup=";
            body += RadioGroupValue(modemPage, "servicesRadioGroup") + "&";


            // Check Home Hub 3000  then batteryRadioGroup
            if (IsRadioButtonChecked(modemPage, "H3MOD"))
            {
                body += "batteryRadioGroup=";
                body += RadioButtonValue(modemPage, "NoBattery") + "&"; //get nobattery value
            }
            else
            {

                body += "batteryRadioGroup=";
                body += RadioGroupValue(modemPage, "batteryRadioGroup") + "&"; //get whatever is selected in existing page
            }


            //H3MOD_HUIND_1
            body += "H3MOD_HUIND_1=";
            body += CheckBoxValue(modemPage, "H3MOD_HUIND_1") + "&";

            //installType
            body += "installType=";
            //body += RadioGroupValue(modemPage, "installType") + "&";
            body += "0&"; //hardcoded self install


            //timePreference
            body += "timePreference=";
            body += SelectBoxValue(modemPage, "timePreference") + "&";


            //installationDatePicker
            body += "installationDatePicker=";
            body += CalendarPicker(modemPage, "installationDatePicker") + "&";

            //fwfmRemarksField
            body += "fwfmRemarksField=";
            body += TextAreaValue(modemPage, "fwfmRemarksField");

            return body.Replace(" ", "+");
        }

        public string ExtractBodyMessageForSelfInstallTypeSelectPosting(string speedChangePage, string submitType = "")
        {

            String body = ExtractGeneralPostMessage(speedChangePage, submitType, "shippingFormhidden");
            body += ExtractExistingValueFromProductModemChangePage(speedChangePage);
            return body.Replace(" ", "+");
        }

        public bool IsQualifyButtonOperationSucess(string QualifyPostReturnPage)
        {

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(QualifyPostReturnPage);


            //Get qualificationRadioGroup
            var productList = doc.DocumentNode.SelectNodes("//select[@id='productSelect']");
            if (productList == null)
                return false;

            HtmlAgilityPack.HtmlDocument doc1 = new HtmlAgilityPack.HtmlDocument();
            doc1.LoadHtml(productList[0].InnerHtml);
            var optionCollection = doc1.DocumentNode.SelectNodes("//option");

            if (optionCollection == null) return false;
            if (optionCollection.Count > 2)
                return true;
            else
                return false;


        }

        public bool IsSelectProductOperationSuccess(string SelectProductSubmitReturnPage)
        {

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(SelectProductSubmitReturnPage);


            //Get qualificationRadioGroup
            var loopTypetList = doc.DocumentNode.SelectNodes("//select[@id='null_LTYPE']");
            if (loopTypetList == null)
                return false;

            HtmlAgilityPack.HtmlDocument doc1 = new HtmlAgilityPack.HtmlDocument();
            doc1.LoadHtml(loopTypetList[0].InnerHtml);
            var optionCollection = doc1.DocumentNode.SelectNodes("//option");

            if (optionCollection == null) return false;
            if (optionCollection.Count > 1)
                return true;
            else
                return false;
        }

        public bool IsPricingandVoipSubmitSuccess(string SelectPricingSubmitReturnPage)
        {

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(SelectPricingSubmitReturnPage);


            //Get qualificationRadioGroup
            var readBCRISCheckbox = doc.DocumentNode.SelectNodes("//input[@id='readBCRISandVOIP']");
            if (readBCRISCheckbox == null)
                return false;

            if (readBCRISCheckbox[0].Attributes["checked"] != null)
                return true;

            return false;
        }

        public bool IsContinueRetunPageSuccess(string ContinuePressedReturnPage)
        {
            return ContinuePressedReturnPage.Contains("<h5>Modem</h5>");
        }

        public bool IsSelfInstallReturnPageSuccess(string SelfInstallTypePostReturnPage) {


            if (RadioGroupValue(SelfInstallTypePostReturnPage, "installType") == "0")
                return true;
            else
                return false;

        }

        public bool IsAdditionalServicePage(string AdditonalServicePage)
        {
            return AdditonalServicePage.Contains("Additional Services");
        }

        public bool IsSubmittionPageAfterAddtionalService(string SubmitPage)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(SubmitPage);


            //Get qualificationRadioGroup
            var agentTextBox = doc.DocumentNode.SelectNodes("//input[@id='agentId']");
            if (agentTextBox == null)
                return false;

            return true;
        }

        public bool IsOrderSuccessfullySubmitted(string SubmitPage)
        {

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(SubmitPage);
            var allH4Headings = doc.DocumentNode.SelectNodes("//h4");

            if (allH4Headings == null) return false;

           

            for (int i = 0; i < allH4Headings.Count; i++)
            {
                if (allH4Headings[i].InnerText.ToLower().Contains("account update status") &&
                      allH4Headings[i].InnerText.ToLower().Contains("successful"))
                    return true;
            }
            return false;

        }

        public bool IsOrderSuccessfullySubmittedForScenerio2(string SubmitPage)
        {

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(SubmitPage);
            var tabDiv = doc.DocumentNode.SelectNodes("//div[@id='tab1Content']");

            if (tabDiv == null) return false;

          
                if (tabDiv[0].InnerText.ToLower().Contains("existing - $0/m fibe tv network access"))
                    return true;
           
            return false;

        }
        


        public string ExtractExistingFieldValues(string speedChangepage)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(speedChangepage);
            String body ="";
            //Get qualificationRadioGroup
            var qulificationRadioGrp = doc.DocumentNode.SelectNodes("//input[@name='qualificationRadioGroup']");
            if (qulificationRadioGrp == null) return "";

            body += "qualificationRadioGroup=";
            foreach (var qualifyRadio in qulificationRadioGrp)
            {
                if (qualifyRadio.Attributes["checked"] != null)
                {
                    body += qualifyRadio.Attributes["value"].Value + "&";
                    break;
                }
            }

            // Get TN
            var qualifyTN = doc.DocumentNode.SelectNodes("//input[@id='stnField']");
            if (qualifyTN == null) return "";

            body += "stnField=";
            body += qualifyTN[0].Attributes["value"].Value + "&";

            // Address
            var streetNumber = doc.DocumentNode.SelectNodes("//input[@id='streetNumber']");
            if (streetNumber == null) return "";
            body += "streetNumber=";
            body += streetNumber[0].Attributes["value"].Value + "&";


            var streetName = doc.DocumentNode.SelectNodes("//input[@id='streetName']");
            if (streetName == null) return "";
            body += "streetName=";
            body += streetName[0].Attributes["value"].Value.Replace("'", "%27") + "&";

            var streetType = doc.DocumentNode.SelectNodes("//input[@id='streetType']");
            if (streetType == null) return "";
            body += "streetType=";
            body += streetType[0].Attributes["value"].Value + "&";


            var streetDirection = doc.DocumentNode.SelectNodes("//input[@id='streetDirection']");
            if (streetDirection == null) return "";
            body += "streetDirection=";
            body += streetDirection[0].Attributes["value"].Value + "&";

            var appartmentNumber = doc.DocumentNode.SelectNodes("//input[@id='appartmentNumber']");
            if (appartmentNumber == null) return "";
            body += "appartmentNumber=";
            body += appartmentNumber[0].Attributes["value"].Value + "&";

            var floorNumber = doc.DocumentNode.SelectNodes("//input[@id='floorNumber']");
            if (floorNumber == null) return "";
            body += "floorNumber=";
            body += floorNumber[0].Attributes["value"].Value + "&";

            var city = doc.DocumentNode.SelectNodes("//input[@id='city']");
            if (city == null) return "";
            body += "city=";
            body += city[0].Attributes["value"].Value + "&";

            // province
                var provinceDropBox = doc.DocumentNode.SelectNodes("//select[@id='province']");
                if (provinceDropBox == null) return "";

                HtmlAgilityPack.HtmlDocument doc1 = new HtmlAgilityPack.HtmlDocument();
                doc1.LoadHtml(provinceDropBox[0].InnerHtml);

                var optionCollection = doc1.DocumentNode.SelectNodes("//option");

                body += "province=";
                foreach (var provincedb in optionCollection)
                {
                    if (provincedb.Attributes["selected"] != null)
                    {
                        body += provincedb.Attributes["value"].Value + "&";
                        break;
                    }
                }


            var postalCode = doc.DocumentNode.SelectNodes("//input[@id='postalCode']");
            if (postalCode == null) return "";
            body += "postalCode=";
            body += postalCode[0].Attributes["value"].Value + "&";



            // body += "telcoList=0&sn=&productSelect=0";



            return body.Replace(" ", "+");
        }

        private string ExtractDialQualificationInfo(string speedChangePageAfterQualifyClick)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(speedChangePageAfterQualifyClick);
            String body = "";

            // Teleco
            var telcoDropBox = doc.DocumentNode.SelectNodes("//select[@id='telcoList']");
            if (telcoDropBox == null) return "";

            HtmlAgilityPack.HtmlDocument doc2 = new HtmlAgilityPack.HtmlDocument();
            doc2.LoadHtml(telcoDropBox[0].InnerHtml);

            var telcoOptionCollection = doc2.DocumentNode.SelectNodes("//option");

            body += "telcoList=";
            foreach (var telcovalue in telcoOptionCollection)
            {
                if (telcovalue.Attributes["selected"] != null)
                {
                    body += telcovalue.Attributes["value"].Value + "&";
                    break;
                }
            }

            body += "&";

            //regionList
            var regionListDropBox = doc.DocumentNode.SelectNodes("//select[@id='regionList']");
            if (regionListDropBox == null) return "";

            HtmlAgilityPack.HtmlDocument doc3 = new HtmlAgilityPack.HtmlDocument();
            doc3.LoadHtml(telcoDropBox[0].InnerHtml);

            var regionListOptionCollection = doc3.DocumentNode.SelectNodes("//option");

            body += "regionList=";
            foreach (var regionValue in regionListOptionCollection)
            {
                if (regionValue.Attributes["selected"] != null)
                {
                    body += regionValue.Attributes["value"].Value + "&";
                    break;
                }
            }
            body += "&";

            //SN
            var sn = doc.DocumentNode.SelectNodes("//input[@id='sn']");
            if (sn == null) return "";
            body += "sn=";
            body += sn[0].Attributes["value"].Value + "&";

            body = body.Replace("&&", "&");
            return body.Replace(" ", "+");
        }

        public string ExtractPostMessageForQualifyButton(string speedChangepage)
        {

            String body = ExtractGeneralPostMessage(speedChangepage, "dslQualifyButton");
            return body += ExtractExistingFieldValues(speedChangepage) + "telcoList=0&sn=&productSelect=0";

        }


        public bool IsMigrationProductExist(string speedChangePage)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(speedChangePage);
            string value = "";
            var selectDropBox = doc.DocumentNode.SelectNodes("//select[@id='productSelect']");
            if (selectDropBox == null) return false;

            HtmlAgilityPack.HtmlDocument doc1 = new HtmlAgilityPack.HtmlDocument();
            doc1.LoadHtml(selectDropBox[0].InnerHtml);

            var optionCollection = doc1.DocumentNode.SelectNodes("//option");

            //ANIS
            foreach (var option in optionCollection)
            {
                //if (option.Attributes["value"] != null)
                //{
                //    value = option.Attributes["value"].Value;
                //    if (value.ToUpper() == "OIBTH" || value.ToUpper() == "QIBTH")
                //        return true;
                //}
                if ((option.NextSibling.InnerText.ToLower().Replace("\n", "") == "ontario fibe tv 15 (fttn)")
                    || (option.NextSibling.InnerText.ToLower().Replace("\n", "") == "quebec fibe tv 15 (fttn)"))
                {
                    return true;
                }


            }
            return false;
        }


        public string GetProductCodeForFibeTV15(string qualifypage)
        {
            //Anis
            var doc = new HtmlDocument();
            doc.LoadHtml(qualifypage);
            var selectDropBox = doc.DocumentNode.SelectNodes("//select[@id='productSelect']");

            if (selectDropBox == null)
                return "";


            HtmlAgilityPack.HtmlDocument doc1 = new HtmlAgilityPack.HtmlDocument();
            doc1.LoadHtml(selectDropBox[0].InnerHtml);

            var optionCollection = doc1.DocumentNode.SelectNodes("//option");

            foreach (var option in optionCollection)
            {
                if ((option.NextSibling.InnerText.ToLower().Replace("\n", "") == "ontario fibe tv 15 (fttn)")
                        || (option.NextSibling.InnerText.ToLower().Replace("\n", "") == "quebec fibe tv 15 (fttn)"))
                {
                    return option.Attributes["value"].Value;
                }
            }
            return "";
        }




        public string ExtractPostMessageForSubmmitProduct(string speedChangePage,string productValue)
        {

            String body = ExtractGeneralPostMessage(speedChangePage, ""); //no submitname
            body += ExtractExistingFieldValues(speedChangePage);
            body += ExtractDialQualificationInfo(speedChangePage);
            body += "productSelect="+productValue.Trim();
            return body.Replace(" ", "+");
        }


        public string GetPricingStructureCode(string editPage)
        {
            //Anis
            var doc = new HtmlDocument();
            doc.LoadHtml(editPage);
            var selectDropBox = doc.DocumentNode.SelectNodes("//select[@id='pricingStructureSelect']");

            if (selectDropBox == null)
                return "";


            HtmlAgilityPack.HtmlDocument doc1 = new HtmlAgilityPack.HtmlDocument();
            doc1.LoadHtml(selectDropBox[0].InnerHtml);

            var optionCollection = doc1.DocumentNode.SelectNodes("//option");

            foreach (var option in optionCollection)
            {
                if (option.NextSibling.InnerText.ToLower().Replace("\r\n", "").Replace("\n","") == "existing - $0/m fibe tv network access")
                {
                    return option.Attributes["value"].Value;
                }
            }
            return "";
        }



        public string ExtractPostMessageForSubmitPricingwithVOIP(string speedChangePage, string productValue)
        {

            String body = ExtractGeneralPostMessage(speedChangePage, ""); //no submitname
            body += ExtractExistingFieldValues(speedChangePage);
            body += ExtractDialQualificationInfo(speedChangePage);
            body += "productSelect=" + productValue.Trim();
            var pricingCode = GetPricingStructureCode(speedChangePage);
            body += "&null_LTYPE_1=1&null_BORDN_1=&null_BDUED_1=&null_BOFCD_1=&null_BPTN_1=&null_VOIPN_1=on&readBCRISandVOIP=on&HOIField=&pricingStructureSelect=" + pricingCode ;
            return body.Replace(" ", "+");
        }

        public string ExtractBodyMessageForContinueButtonPosting(string speedChangePage, string productValue)
        {

            String body = ExtractGeneralPostMessage(speedChangePage, "finalButtonCopy1"); 
            body += ExtractExistingFieldValues(speedChangePage);
            body += ExtractDialQualificationInfo(speedChangePage);
            body += "productSelect=" + productValue.Trim();
            var pricingCode = GetPricingStructureCode(speedChangePage);
            body += "&null_LTYPE_1=1&null_BORDN_1=&null_BDUED_1=&null_BOFCD_1=&null_BPTN_1=&null_VOIPN_1=on&readBCRISandVOIP=on&HOIField=&pricingStructureSelect="+ pricingCode + "&promotionSelect=0";
            return body.Replace(" ", "+");
        }





      
        public string EncodeHtmlCharacter(string Body)
        {
            Body = Body.Replace(",", "%2C");

            //String tempSeedID = node.Attributes["value"].Value;
            Body = Body.Replace("/", "%2F");
            Body = Body.Replace("=", "%3D");
            Body = Body.Replace("+", "%2B");
            Body = Body.Replace("!", "%21");
            Body = Body.Replace("#", "%23");
            Body = Body.Replace("$", "%24");
            Body = Body.Replace("*", "%2A");
            Body = Body.Replace(",", "%2C");
            Body = Body.Replace("-", "%2D");
            Body = Body.Replace(".", "%2E");
            Body = Body.Replace(":", "%3A");
            Body = Body.Replace(";", "%3B");
            Body = Body.Replace("<", "%3C");
            Body = Body.Replace(">", "%3E");
            Body = Body.Replace("?", "%3F");
            Body = Body.Replace("@", "%40");
            Body = Body.Replace("^", "%5E");
            Body = Body.Replace("_", "%5F");

            return Body;
        }

        public string TextBoxValue(string page, string textboxid)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(page);
            var appartmentNumber = doc.DocumentNode.SelectNodes("//input[@id='"+ textboxid + "']");
            if (appartmentNumber == null) return "";            
            return appartmentNumber[0].Attributes["value"].Value;
        }

        public string TextAreaValue(string page, string textboxid)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(page);
            var appartmentNumber = doc.DocumentNode.SelectNodes("//textarea[@id='" + textboxid + "']");
            if (appartmentNumber == null) return "";
            return appartmentNumber[0].InnerText;
        }

        public string SelectBoxValue(string page, string selectBoxId)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(page);
            string value = "";
            var selectDropBox = doc.DocumentNode.SelectNodes("//select[@id='"+ selectBoxId + "']");
            if (selectDropBox == null) return "";

            HtmlAgilityPack.HtmlDocument doc1 = new HtmlAgilityPack.HtmlDocument();
            doc1.LoadHtml(selectDropBox[0].InnerHtml);

            var optionCollection = doc1.DocumentNode.SelectNodes("//option");

            
            foreach (var option in optionCollection)
            {
                if (option.Attributes["selected"] != null)
                {
                    value = option.Attributes["value"].Value;
                    break;
                }
            }
            return value;

        }

        public string RadioGroupValue(string page, string id)
        {

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(page);
            String body = "";
            //Get qualificationRadioGroup
            var qulificationRadioGrp = doc.DocumentNode.SelectNodes("//input[@name='"+id+"']");
            if (qulificationRadioGrp == null) return "";

            string value = "";
            foreach (var qualifyRadio in qulificationRadioGrp)
            {
                if (qualifyRadio.Attributes["checked"] != null)
                {
                    value = qualifyRadio.Attributes["value"].Value;
                    break;
                }
            }
            return value;
        }

        public string CheckBoxValue(string page, string id)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(page);
            var checkbox = doc.DocumentNode.SelectNodes("//input[@id='" + id + "']");
            if (checkbox == null) return "";
            if (checkbox[0].Attributes["checked"] != null)
                return "on";
            return "";
        }

        public string CalendarPicker(string page, string id)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(page);
            string value = "";
            var appartmentNumber = doc.DocumentNode.SelectNodes("//input[@id='" + id + "']");
            if (appartmentNumber == null) return "";
            value= appartmentNumber[0].Attributes["value"].Value;
            return value.Replace("/", "%2F");
        }

        public bool IsRadioButtonChecked(string page, string id)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(page);     
            var radiobutton = doc.DocumentNode.SelectNodes("//input[@id='" + id + "']");
            if (radiobutton == null) return false;
            if(radiobutton[0].Attributes["checked"] != null)
                return true;
            return false;
        }

        public string RadioButtonValue(string page, string id)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(page);
            var radiobutton = doc.DocumentNode.SelectNodes("//input[@id='" + id + "']");
            if (radiobutton == null) return "";
            if (radiobutton[0].Attributes["value"] != null)
                return radiobutton[0].Attributes["value"].Value;
            return "";
        }
    }
}
