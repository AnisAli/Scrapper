﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Serilog;
using SimpleWebScrapper.Engine.Contracts;
using SimpleWebScrapper.Engine.Models;
using SimpleWebScrapper.Engine.PageParsers;

namespace SimpleWebScrapper.Engine
{
    public class SimpleBellPageParser:ISimpleBellPageParser
    {
        private readonly ILogger _logger;

        public SimpleBellProductDetailParser _productDetailParser;
        public ValueAddedServicePageParser _valueAddedServiceParser;
        public ValueEditPageParser _valueEditPageParser;
        public DarkTV_ProductPageParser _darktvProductPageParser;
        // public SimpleBellProductDetailParser ProductDetailParser = new SimpleBellProductDetailParser();
        public SimpleBellPageParser(ILogger logger)
        {
            _logger = logger;
            _productDetailParser = new SimpleBellProductDetailParser();
            _valueAddedServiceParser = new ValueAddedServicePageParser();
            _valueEditPageParser = new ValueEditPageParser();
            _darktvProductPageParser = new DarkTV_ProductPageParser();
        }

        public string ParseSessionIdFronLoginResponse(string htmlPage)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlPage);
            var itemCheckSessionExist = doc.DocumentNode.SelectNodes("//a[@id='restart']");
            if (itemCheckSessionExist == null)
            {
                _logger.Error("ParseSessionIdFronLoginResponse: expected id not exist during parsing Login Session: Expected[id=restart]");
                return string.Empty;
            }
            var node = doc.DocumentNode.SelectNodes("//a[@id='restart']")[0];
            var att = node.Attributes["href"];
            var end = att.Value.IndexOf("service=restart");
            const int start = 19;
            var sessionId = att.Value.Substring(start, end - start - 1);
            return sessionId;
        }

        public bool IsSessionExpired(string htmlPage)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlPage);
            var passwordBoxColl = doc.DocumentNode.SelectNodes("//input[@id='inputPassword']");
            return passwordBoxColl != null;
        }

        public BellCustomerInfo ParseLoginPageResponse(string htmlPage)
        {
            throw new NotImplementedException();
        }

        public BellCustomerInfo ParseCustomerInfoPage(string htmlPage)
        {
            BellCustomerInfo customer = new BellCustomerInfo();
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlPage);
            var accountIdentifierSpanCollection = doc.DocumentNode.SelectNodes("//span[@class='accountIdentifier']");
            foreach (var span in accountIdentifierSpanCollection)
            {
                customer.CustomerIdentifier = span.InnerText;
                break;
            }
            return customer;
        }

        public IList<BellProductService> ParseProductPage(string htmlPage)
        {
            IList<BellProductService> productServiceLst = new List<BellProductService>();
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlPage);
            var productLoop = doc.DocumentNode.SelectNodes("//div[@id='productLoop']");

            if (productLoop == null)
                return null;

            foreach (var divProductLoop in productLoop) // read all div under productLoop
            {
                doc.LoadHtml(divProductLoop.InnerHtml);
                // get all div collection under div under productLoop
                var divCollections = doc.DocumentNode.SelectNodes("//div");

                foreach (var productDiv in divCollections)
                {
                    var productDivDoc = new HtmlAgilityPack.HtmlDocument();
                    productDivDoc.LoadHtml(productDiv.InnerHtml);

                    var table = productDivDoc.DocumentNode.SelectNodes("//table");

                    if (table?.Count > 1) // we are interested in 1 table each div
                            continue;

                    if (table == null) continue;

                    var productColumnCollection =  table[0].SelectNodes("//td[@class='serviceTitle']") ??
                                                   table[0].SelectNodes("//td[@class='serviceSubTitle']");

                    var link = productColumnCollection?[0].SelectNodes("//a");

                    if (link == null) continue;

                    var productService = new BellProductService
                    {
                        ProductName = link[0].InnerText,
                        ProductUrl = link[0].Attributes["href"].Value
                    };
                    productServiceLst.Add(productService);
                }   
            }
            return productServiceLst;
        }

        public bool IsCustomerFound_SearhPageParse(string htmlPage)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlPage);
            var isCustomerNotFoundSpans = doc.DocumentNode.SelectNodes("//span[@id='noRecordFound']");
            var customerNotFoundSpans = isCustomerNotFoundSpans;
            return customerNotFoundSpans == null || isCustomerNotFoundSpans.Select(span => span.InnerText.ToUpper()).All(result => !result.Contains("NO RECORDS FOUND"));
        }

        public SimpleBellProductDetailParser ProductDetailParser => _productDetailParser;

        public ValueAddedServicePageParser ValueAddedServicePageParser => _valueAddedServiceParser;

        public ValueEditPageParser ValueEditPageParser => _valueEditPageParser;

        DarkTV_ProductPageParser ISimpleBellPageParser.DarktvProductPageParser => _darktvProductPageParser;
    }
}
