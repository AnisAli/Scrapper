﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleWebScrapper.Engine.Models;

namespace SimpleWebScrapper.Engine.PageParsers
{
    public class SimpleBellProductDetailParser
    {

        public UnlimitedUsageProductDetails ParseUnlimitedProductDetails(string htmlPage)
        {
            UnlimitedUsageProductDetails productDetail = new UnlimitedUsageProductDetails();
            var doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(htmlPage);
            var showPricingDivCollection = doc.DocumentNode.SelectNodes("//div[@id='showPricing1If_0']");

            if (showPricingDivCollection == null)
                return null;

            foreach (var promotionDiv in showPricingDivCollection)
            {
                var productDivDoc = new HtmlAgilityPack.HtmlDocument();
                productDivDoc.LoadHtml(promotionDiv.InnerHtml);

                var tableCollection = productDivDoc.DocumentNode.SelectNodes("//table");
                if (tableCollection == null) return null;

                foreach (var table in tableCollection)
                {
                    if (table.InnerText.Contains("Code:"))
                    {

                        var rowDoc = new HtmlAgilityPack.HtmlDocument();
                        rowDoc.LoadHtml(table.InnerHtml);

                        var rows = rowDoc.DocumentNode.SelectNodes("//tr");
                        foreach (var row in rows)
                        {
                            var columnsDoc = new HtmlAgilityPack.HtmlDocument();
                            columnsDoc.LoadHtml(row.InnerHtml);

                            var columns=  columnsDoc.DocumentNode.SelectNodes("//td");
                            if (columns.Count <= 1) continue;
                            if (columns[0].InnerText.Contains("Code:"))
                            {
                                productDetail.PromotionCode = columns[1].InnerText;
                            }
                            else
                            {
                                productDetail.PromotionName = columns[1].InnerText;
                            }
                        }
                    }
                }
            }
            return productDetail;
        } //
    }
}
