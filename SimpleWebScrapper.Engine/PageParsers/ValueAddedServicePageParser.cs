﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleWebScrapper.Engine.PageParsers
{
    public class ValueAddedServicePageParser
    {
        public bool EditButtonEnable(string htmlPage)
        {   
            var doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(htmlPage);
            var itemEditButtonEnabled = doc.DocumentNode.SelectNodes("//a[@id='editVASLink']");

            if (itemEditButtonEnabled == null)
                return false;
            else
                return true;
        } 
    }
}
