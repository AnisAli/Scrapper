﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Serilog;
using SimpleWebScrapper.Engine.Contracts;
using SimpleWebScrapper.Engine.Models;

namespace SimpleWebScrapper.Engine
{
    public class SimpleBellHttpCaller : ISimpleBellHttpCaller
    {

        private readonly ILogger _logger;
        private readonly IEngineConfiguration _engineConfig;
        private readonly ISimpleBellPageParser _pageParser;
        public SimpleBellHttpCaller(ILogger logger, IEngineConfiguration engineConfiguration, ISimpleBellPageParser simpleBellPageParser )
        {
            _logger = logger;
            _engineConfig = engineConfiguration;
            _pageParser = simpleBellPageParser;
        }


        public string GetSessionId()
        {
            var checkLoginSuccess = false;
            var resultSessionId = string.Empty;

            for (var i = 0; i < 8; i++)
            {
                 resultSessionId = GetSessionFromSimpleBell();
                checkLoginSuccess = !string.IsNullOrEmpty(resultSessionId);

                if (checkLoginSuccess == true)
                    break;
            }

            if (checkLoginSuccess != false) return resultSessionId;

            _logger.Error("Get SessionID 8 time Failed");
            throw new Exception("Login Fail!");
        }

        #region Authentication

        private string GetSessionFromSimpleBell()
        {
            try
            {
                var loginSessionId = "";
                string firstSession = GetSessionIdFromLogInPostRequest();
                System.Threading.Thread.Sleep(4000);
                string secondSession = GetSessionIdFromLogInGetRequest(firstSession);
                System.Threading.Thread.Sleep(4000);
                if (secondSession.Trim() == "")
                    secondSession = GetSessionIdFromLogInGetRequest(firstSession);
                loginSessionId = secondSession;
                System.Threading.Thread.Sleep(3000);
                GetSessionIdFromLogInPostRequest(false, loginSessionId);
                Console.WriteLine(loginSessionId);
                return loginSessionId.Trim();
            }
            catch (Exception ex)
            {
                _logger.Error("GetSessionFromSimpleBell:", ex);
                return string.Empty;
            }
        }

        private string GetSessionIdFromLogInGetRequest(string inSessionId)
        {
            // Send Another Get Request for Login
            CookieContainer cookies = new CookieContainer();
            HttpWebRequest objRequest1 = (HttpWebRequest)WebRequest.Create(_engineConfig.SimpleWebURL);
            cookies.Add(new Cookie("JSESSIONID", inSessionId.Trim()) { Domain = "simple.int.bell.ca" });
            objRequest1.CookieContainer = cookies;
            objRequest1.Method = "GET";
            objRequest1.Host = "simple.int.bell.ca";
            HttpWebResponse objResponse1 = (HttpWebResponse)objRequest1.GetResponse();
            CookieCollection cook = objResponse1.Cookies;

            using (var sr = new StreamReader(objResponse1.GetResponseStream()))
            {
                sr.ReadToEnd();
                sr.Close();
            }

            if (cook.Count != 0)
            {
                var newSessionId = cook[0].Value;
                return newSessionId;
            }
            else
                return "";
        }

        private string GetSessionIdFromLogInPostRequest(bool isFirst = true, string inSession = "")
        {
            const string funcName = "SimpleBellHttp-LogIn";

            // LOGIN First Request
            var resultFromServer = "";
            var strPost = "formids=inputUserName%2CinputPassword%2CinputLanguage%2ChasMessages_0%2CdisplayRedirectMsg_0&seedids=ZH4sIAAAAAAAAAFvzloG1vJ6hVqc4tagsMzlVxUCnIDEdRCXn5xbk56XmlYDZeSWJmXmpRUB2cWpxcWZ%2BHohVACR88tMz83wSK%2FNLQQqLM1JzcsCKispSi%2FwSc1N9U0sSQxLTgWJO%2BSmVQCooNS8ltQjKScsvylUxAAD7bj%2FThQAAAA%3D%3D&component=form&page=Login&service=direct&session=T&submitmode=&submitname=&hasMessages_0=F&displayRedirectMsg_0=F&inputUserName=" + _engineConfig.UserName + "&inputPassword=" + _engineConfig.Password + "&inputLanguage=0&submit_button=Login";
            StreamWriter myWriter = null;
            var objRequest = (HttpWebRequest)WebRequest.Create(_engineConfig.SimpleWebURL);
            objRequest.Method = "POST";
            objRequest.ContentLength = strPost.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";
            objRequest.Host = "simple.int.bell.ca";

            if (isFirst == false)
            {
                var cookies = new CookieContainer();
                cookies.Add(new Cookie("JSESSIONID", inSession.Trim()) { Domain = "simple.int.bell.ca" });
                objRequest.CookieContainer = cookies;
            }

            objRequest.AllowAutoRedirect = true;
            try
            {
                myWriter = new StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(strPost);
            }
            catch (Exception ex)
            {
                _logger.Error(funcName, ex);
                throw new Exception(ex.Message);
            }
            finally
            {
                myWriter.Close();
            }

            var objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (var sr = new StreamReader(objResponse.GetResponseStream()))
            {
                resultFromServer = sr.ReadToEnd();
                sr.Close();
            }

            return isFirst != true ? "" : _pageParser.ParseSessionIdFronLoginResponse(resultFromServer);
        }
        #endregion

        public bool IsAdditionalServiceEditAllow(string inSessionId)
        {
            var valueEditPage = this.GetRequest("https://simple.int.bell.ca/accounts_ViewAdditionalServices.html", inSessionId);
            if (this._pageParser.IsSessionExpired(valueEditPage))
                throw new Exception("Session Expired");
            return this._pageParser.ValueAddedServicePageParser.EditButtonEnable(valueEditPage);
        }

        public string GetProductViewInfoPage(string inSession)
        {
            String result = GetRequest("https://simple.int.bell.ca/accounts_ViewProductInfo.html", inSession);
            if (this._pageParser.IsSessionExpired(result))
                throw new Exception("Session Expired");
            return result;
        }

        public string GetValueEditPage(string inSession)
        {
            String result = GetRequest("https://simple.int.bell.ca/pegasus?component=editVASLink&page=accounts_ViewAdditionalServices&service=direct&session=T", inSession);
            if (this._pageParser.IsSessionExpired(result))
                throw new Exception("Session Expired");
            return result;
        }


        public string GetPricingStructurePage(string url, string inSession)
        {
         
            String result = GetRequest("https://simple.int.bell.ca/"+url, inSession, "https://simple.int.bell.ca/accounts_ViewProductInfo.html");
            if (this._pageParser.IsSessionExpired(result))
                throw new Exception("Session Expired");
            return result;
        }

        public string GetProductSpeedChangePage(string url, string inSession)
        {

            String result = GetRequest("https://simple.int.bell.ca/" + url, inSession, "https://simple.int.bell.ca/accounts_ViewProductInfo.html");
            if (this._pageParser.IsSessionExpired(result))
                throw new Exception("Session Expired");
            return result;
        }

        public string GetEditPriceStructurePage(string url, string inSession)
        {
            String result = GetRequest("https://simple.int.bell.ca/pegasus?component=productDetails.editButton&page=accounts_ViewProductInfo&service=direct&session=T", inSession, "https://simple.int.bell.ca/" + url);
            if (this._pageParser.IsSessionExpired(result))
                throw new Exception("Session Expired");
            return result;
        }

        public string SearchCustomerPage(string customerId,string sessionId)
        {
            
            string result = "";
            string strPost = "formids=pcidtsResultCodeAccntSrch%2CpcidtsTokenAccntSrch%2CpcidtsLogNoteAccntSrch%2ChasMessages_0%2ChasMessages_2%2CcriteriaSelect%2CsearchField%2CsearchLink%2CshowBackButton_0&seedids=ZH4sIAAAAAAAAAF1RW27CMBBESL0Jn3xQ9QSUFpUKWtpwgZUzSSz8iNYOVThTr9Y7dJ1AePx4d2d3Z8b279%2Fo4TgajZ%2FGj9MAPmiFyWxaU5mC8rb2Di52uYukHVjygBC0dymr5XhBQY2Ja2p9k0ZDBWPSSghr7fY7KvtiKRSZPuIGfP9qwO1rUUDFE6fYAH%2BQxQaR%2BsE9UJPRB6xclDaZa2ynLXrlZ5%2B3Egy5spErJKFUJj3%2F5i2GYssowhnxZb9NxvifLLYGi0oYUi%2BHYlBAsi4lIyCecu1uWrqjy4ZbWHLiYK6Ub1wMg%2FDmHhZK37DC%2Fcgn5%2BAEJpV43hiGFndoL7cllt%2B6YtdhnlvtLrbEps5z3CDdY1yAbziRPj0l9VwZiFXVfU5Klp7tZPYP3zgpwDwCAAA%3D&component=accountSearch.searchForm&page=accounts_SearchResults&service=direct&session=T&submitmode=&submitname=searchLink&pcidtsResultCodeAccntSrch=empty&pcidtsTokenAccntSrch=&pcidtsLogNoteAccntSrch=&hasMessages_0=F&hasMessages_2=F&showBackButton_0=F&criteriaSelect=0&searchField=" + customerId;
            StreamWriter myWriter = null;
            CookieContainer cookies = new CookieContainer();
            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(_engineConfig.SimpleWebURL);
            cookies.Add(new Cookie("JSESSIONID", sessionId.Trim()) { Domain = "simple.int.bell.ca" });
            cookies.Add(new Cookie("userName", _engineConfig.UserName) { Domain = "simple.int.bell.ca" });
            objRequest.CookieContainer = cookies;
            objRequest.Method = "POST";
            objRequest.Referer = "https://simple.int.bell.ca/accounts_LandingPage.html";
            objRequest.ContentLength = strPost.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";
            objRequest.Host = "simple.int.bell.ca";
            try
            {
                myWriter = new StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(strPost);
            }
            catch (Exception e)
            {
                _logger.Error(e.Message,e);
                throw new Exception(e.Message);
            }
            finally
            {
                myWriter.Close();
            }

                HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
                using (StreamReader sr =
                   new StreamReader(objResponse.GetResponseStream()))
                {
                    result = sr.ReadToEnd();
                    sr.Close();
                }

                return GetRequest("https://simple.int.bell.ca/accounts_ViewCustomerInfo.html",sessionId); ;
           }

        public IList<BellProductService> GetCustomerProductServices(string customerId, string sessionId)
        {
            var result = SearchCustomerPage(customerId, sessionId);

            if (this._pageParser.IsSessionExpired(result))
                throw new Exception("Session Expired");
            
            if(!_pageParser.IsCustomerFound_SearhPageParse(result)) // if customer not found
                throw new Exception("Customer not Found");

            var CustomerViewPage = this.GetRequest("https://simple.int.bell.ca/accounts_ViewCustomerInfo.html" , sessionId);
            if (this._pageParser.IsSessionExpired(CustomerViewPage))
                throw new Exception("Session Expired");

            var ProductViewPage = this.GetRequest("https://simple.int.bell.ca/accounts_ViewProductInfo.html", sessionId);
            if (this._pageParser.IsSessionExpired(ProductViewPage))
                throw new Exception("Session Expired");

            return this._pageParser.ParseProductPage(ProductViewPage);

        }

        #region get/post Request
        public string GetRequest(string url, string loginSessionId, string referer ="")
        {
            try
            {
                string result = "";
                CookieContainer cookies = new CookieContainer();
                HttpWebRequest objRequest = (HttpWebRequest) WebRequest.Create(url);
                cookies.Add(new Cookie("JSESSIONID", loginSessionId.Trim()) {Domain = "simple.int.bell.ca"});
                cookies.Add(new Cookie("userName", _engineConfig.UserName) {Domain = "simple.int.bell.ca"});
                objRequest.CookieContainer = cookies;
                objRequest.Method = "GET";
                objRequest.Host = "simple.int.bell.ca";
                if (string.IsNullOrEmpty(referer))
                    objRequest.Referer = referer;
                    
                HttpWebResponse objResponse = (HttpWebResponse) objRequest.GetResponse();
                using (StreamReader sr =
                    new StreamReader(objResponse.GetResponseStream()))
                {
                    result = sr.ReadToEnd();
                    sr.Close();
                }
                return result;
            }
            catch (Exception ex)
            {
                _logger.Error("Get Request Error:", ex);
            }
            return "";
        }

        private string PostRequest(string loginSessionId, string PostMessage)
        {
            string result = "";

            PostMessage = PostMessage.Replace("È", "%C3%88");
            PostMessage = PostMessage.Replace("'", "%27");
            PostMessage = PostMessage.Replace("É", "%C3%89");
            PostMessage = PostMessage.Replace("Ô", "%C3%94");
            PostMessage = PostMessage.Replace("Ê", "%C3%8A");
            PostMessage = PostMessage.Replace("Â", "%C3%71");
            
                PostMessage = PostMessage.Replace("Ç", "%C3%87");
            PostMessage = PostMessage.Replace("Î", "%C3%8E");

            StreamWriter myWriter = null;
            CookieContainer cookies = new CookieContainer();
            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(_engineConfig.SimpleWebURL);
            cookies.Add(new Cookie("JSESSIONID", loginSessionId.Trim()) { Domain = "simple.int.bell.ca" });
            cookies.Add(new Cookie("userName", _engineConfig.UserName) { Domain = "simple.int.bell.ca" });
            objRequest.CookieContainer = cookies;
            objRequest.Method = "POST";
            objRequest.Referer = "https://simple.int.bell.ca/accounts_LandingPage.html";
            objRequest.ContentLength = PostMessage.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";
            objRequest.Host = "simple.int.bell.ca";
            try
            {
                myWriter = new StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(PostMessage);
            }
            catch (Exception e)
            {
                _logger.Error("PostRequest: "+e.Message);
                throw new Exception(e.Message);
            }
            finally
            {
                myWriter.Close();
            }
            try
            {
                HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
                using (StreamReader sr =
                   new StreamReader(objResponse.GetResponseStream()))
                {
                    result = sr.ReadToEnd();
                    sr.Close();
                }
                return result;
            }//try
            catch (Exception ex)
            {
                _logger.Error("PostRequest: " + ex.Message);
                throw new Exception(ex.Message);
            }
        }
        #endregion
        private string GeneratePostMessage(string inSession)
        {
            var editPageHtml = GetValueEditPage(inSession);
            var postMesssage = _pageParser.ValueEditPageParser.ExtractPostMessage(editPageHtml);

            if (postMesssage.Length < 10) // measn POST Message generation Error
            {

                _logger.Error("Invalid PostMessage Length");
                // Logging.LogHtmlFile(inUSER_ID, result);  // store screen shot of page
                throw new Exception("Invalid PostMessage Lenght [" + postMesssage.Length + "] expected Lenght Should be > [10]");
            }
            postMesssage = postMesssage.Replace("submitmode=&submitname=&editModeIf_0", "submitmode=&submitname=submitVASLink&editModeIf_0");
            return postMesssage;
        }

        public bool Submit_UnlimitedPlan_Uncheck(string inSession)
        {
            try
            {
                var postMessage = GeneratePostMessage(inSession);
                //  postMessage = postMessage + "&BWUNL=on&promotions_BWUNL=PBW28";
                // postMessage = postMessage;// + "&BWUNL=";  //remove unlimited plan from post message
              //  postMessage = postMessage.Replace("&BWGBB=on", "");
                var resultPage = this.PostRequest(inSession, postMessage);

                if (_pageParser.ValueEditPageParser.IsSubmitPageSuccessful(resultPage))
                    return true;
                else
                    return false;
            }
            catch (Exception ex) {
                _logger.Error("Submit_UnlimitedPlan_Uncheck" + ex.Message);
                throw ex;
            }
        }

        public string PressQualifyButton(string productSpeedPageHtml, string inSession)
        {
            try
            {
                var PostBody = this._pageParser.DarktvProductPageParser.ExtractPostMessageForQualifyButton(productSpeedPageHtml);
                var resultPage = this.PostRequest(inSession, PostBody);
                return resultPage;       
            }
            catch (Exception ex)
            {
                _logger.Error("PressQualifyButton" + ex.Message);
                throw ex;
            }
        }

        public string SelectDarkTVProduct(string pageAfterQualifyClick, string inSession, string province)
        {
            try
            {
                var product = "";
                //if (province == "ON")
                //    product = "OIBTH";
                //else
                //    product = "QIBTH";

                product = this._pageParser.DarktvProductPageParser.GetProductCodeForFibeTV15(pageAfterQualifyClick);

                var PostBody = this._pageParser.DarktvProductPageParser.ExtractPostMessageForSubmmitProduct(pageAfterQualifyClick, product);
                var resultPage = this.PostRequest(inSession, PostBody);
                return resultPage;
            }
            catch (Exception ex)
            {
                _logger.Error("Select Product Error" + ex.Message);
                throw ex;
            }
        }


        public string SelectPricingWithVOIPIndicator(string pageAfterSubmittingProduct, string inSession,string province)
        {
            try
            {
                var product = "";
                //if (province == "ON")
                //    product = "OIBTH";
                //else
                //    product = "QIBTH";

                product = this._pageParser.DarktvProductPageParser.GetProductCodeForFibeTV15(pageAfterSubmittingProduct);
                var PostBody = this._pageParser.DarktvProductPageParser.ExtractPostMessageForSubmitPricingwithVOIP(pageAfterSubmittingProduct, product);
                var resultPage = this.PostRequest(inSession, PostBody);
                return resultPage;
            }
            catch (Exception ex)
            {
                _logger.Error("Select Pricing Error" + ex.Message);
                throw ex;
            }
        }

        public string PressContinueButtonPressAfterPriceandProductChange(string pageAfterPriceChange, string inSession, string province)
        {
            try
            {
                var product = "";
                //if (province == "ON")
                //    product = "OIBTH";
                //else
                //    product = "QIBTH";

                product = this._pageParser.DarktvProductPageParser.GetProductCodeForFibeTV15(pageAfterPriceChange);

                var PostBody = this._pageParser.DarktvProductPageParser.ExtractBodyMessageForContinueButtonPosting(pageAfterPriceChange, product);
                var resultPage = this.PostRequest(inSession, PostBody);
                return resultPage;
            }
            catch (Exception ex)
            {
                _logger.Error("Continue button click after product, price and voip indicator" + ex.Message);
                throw ex;
            }
        }

        public string SelectSelfInstallOptionWithNoBattery(string modemPage, string inSession)
        {
            try
            {
               
                var PostBody = this._pageParser.DarktvProductPageParser.ExtractBodyMessageForSelfInstallTypeSelectPosting(modemPage);
                var resultPage = this.PostRequest(inSession, PostBody);
                return resultPage;
            }
            catch (Exception ex)
            {
                _logger.Error("selecting SelfInstall Type , Post error" + ex.Message);
                throw ex;
            }
        }

        public string MoveToNextStepAfterSelfInstallType(string modemPage, string inSession)
        {
            try
            {

                var PostBody = this._pageParser.DarktvProductPageParser.ExtractBodyMessageForSelfInstallTypeSelectPosting(modemPage, "nextButton");
                var resultPage = this.PostRequest(inSession, PostBody);
                return resultPage;
            }
            catch (Exception ex)
            {
                _logger.Error("Move To Next Step After SelfInstallType  , Post error" + ex.Message);
                throw ex;
            }
        }

        public string MoveToNextFromAddtionalServicePage(string addtionalServicePage, string inSession)
        {
            try
            {

                var PostBody = this._pageParser.DarktvProductPageParser.ExtractMessageBodyFromAddditonalServicePage(addtionalServicePage);
                var resultPage = this.PostRequest(inSession, PostBody);
                return resultPage;
            }
            catch (Exception ex)
            {
                _logger.Error("Move To Next from Addtional Service Page  , Post error" + ex.Message);
                throw ex;
            }
        }

        public string SubmitFinalOrderofScenerio1(string ConfirmationDetailPage, string inSession)
        {
            try
            {

                var PostBody = this._pageParser.DarktvProductPageParser.ExtractFinalSubmitScenerio1Body(ConfirmationDetailPage);
                var resultPage = this.PostRequest(inSession, PostBody);
                return resultPage;
            }
            catch (Exception ex)
            {
                _logger.Error("Final Submittion of Product Change using Scenerio 1  , Post error" + ex.Message);
                throw ex;
            }
        }

        public string SubmitFinalOrderofScenerio2(string ConfirmationDetailPage, string inSession)
        {
            try
            {

                var PostBody = this._pageParser.DarktvProductPageParser.ExtractFinalSubmitScenerio2Body(ConfirmationDetailPage);
                var resultPage = this.PostRequest(inSession, PostBody);
                return resultPage;
            }
            catch (Exception ex)
            {
                _logger.Error("Final Submittion of Product Change using Scenerio 2  , Post error" + ex.Message);
                throw ex;
            }
        }
    }
}
