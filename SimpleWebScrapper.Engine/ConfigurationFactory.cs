﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using Serilog;
using SimpleWebScrapper.Common;
using SimpleWebScrapper.Engine.Configuration;

namespace SimpleWebScrapper.Engine
{
    public class ConfigurationFactory : IConfigurationFactory
    {
        private IEngineOutputHandler _engineOutputHandler;
        private ICustomerRepository _customerRepository;
        private IEngineConfiguration _engineConfiguration;

        public ConfigurationFactory(ILogger logger)
        {
            var config =
                ConfigurationManager.GetSection("simpleWebScrapperEngine") as SimpleWSEngineConfigurationSection;
            if (config?.CustomerRepository != null)
            {
                ICustomerRepository customerRepository =
                    Activator.CreateInstance(Type.GetType(typeName: config.CustomerRepository.Type),logger) as ICustomerRepository;
                _customerRepository = customerRepository;
            //    _customerRepository.Logger = logger;
            }
            else
            {
                throw new Exception("No repository provider found on AppConfig");
            }

            if (config.EngineOutputHandler != null)
            {
                IEngineOutputHandler engineOutputHandler =
                    Activator.CreateInstance(Type.GetType(config.EngineOutputHandler.Type), logger) as IEngineOutputHandler;
                _engineOutputHandler = engineOutputHandler;
            }
            else
            {
                throw new Exception("No Outputhandler provider found on AppConfig");
            }

            _engineConfiguration = config.EngineConfiguration as IEngineConfiguration;
            
        }

        public ICustomerRepository GetCustomerRepository()
        {
            return _customerRepository;
        }

        public IEngineOutputHandler GetOutputHandler()
        {
            return _engineOutputHandler;
        }

        public IEngineConfiguration GetEngineConfiguration()
        {
            return _engineConfiguration;

        }
    }
}
