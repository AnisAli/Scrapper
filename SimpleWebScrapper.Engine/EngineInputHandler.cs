﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleWebScrapper.Common;
using Serilog;

namespace SimpleWebScrapper.Engine
{

    //This class is using facade pattern to make abraction of repo and output for filtering data using configuration
   public class EngineInputHandler:IEngineInputHandler
   {
       private readonly ICustomerRepository _customerRepository;
       private readonly IEngineConfiguration _engineConfiguration;
       private readonly IEngineOutputHandler _engineOutputHandler;
       private readonly ILogger _logger;
  
        public EngineInputHandler(IConfigurationFactory configurationFactory, IEngineConfiguration engineConfiguration, ILogger logger)
        {
            _customerRepository = configurationFactory.GetCustomerRepository();// customerRepository;
            _engineConfiguration = engineConfiguration;
            _engineOutputHandler = configurationFactory.GetOutputHandler();//englineOutputHandler;
            _logger = logger;
        }
        public List<Customer> GetCustomerList()
        {
            var tempCustomerList = _customerRepository.Customers;
            var customerList = new List<Customer>();
            _logger.Information("Total Customer From Repo [{0}]", tempCustomerList.Count);
            customerList = _engineConfiguration.SucessFilterOnly ? 
                            FilterOnlySucessList(tempCustomerList, this._engineOutputHandler.GetSuccessCustomerList()) 
                                : FilterBothSuccessFailList(tempCustomerList, this._engineOutputHandler.GetSuccessCustomerList(), _engineOutputHandler.GetFailureCustomerList());
            _logger.Information("Total Customer After Filter [{0}]", customerList.Count);
            return customerList;
        }

        public bool ReadyToRead(Customer cust)
        {
            return _customerRepository.ReadyToRead(cust);
        }

        public bool IsAllowToProcess(Customer cust)
        {
            return _customerRepository.IsAllowToProcess(cust);
        }


        private List<Customer> FilterOnlySucessList(IEnumerable<Customer> customerList, List<Customer> successList)
        {
            if (successList != null)
                _logger.Information("Filter List [{0}]", successList.Count);

            return (List<Customer>)(successList == null ? customerList : (from orgUser in customerList let found = successList.Any(successUser => string.Equals(successUser.CustomerB1Number, orgUser.CustomerB1Number)) where found == false select orgUser).ToList());
        }

        private List<Customer> FilterBothSuccessFailList(IEnumerable<Customer> customerList, List<Customer> successList, List<Customer> failList)
        {
            var newList = FilterOnlySucessList(customerList, successList);
            return FilterOnlySucessList(newList, failList);
        }
    }
}
