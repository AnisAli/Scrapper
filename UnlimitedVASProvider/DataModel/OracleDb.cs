﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleWebScrapper.Common;
using System.Data;

namespace UnlimitedVASProvider.DataModel
{
    public class OracleDb
    {
        private String DatabaseConnectionString;
        private static OracleDb oracleDBInstance = null;

        private OracleDb(string connectionstring)
        {
            DatabaseConnectionString = connectionstring; //"Data Source=142.117.248.15:1915/PRADMSP1;User ID=daryld;Password=daryld4x4;";
        }

        public static OracleDb GetInstance(string connectionString)
        {

            if (oracleDBInstance == null)
            {
                oracleDBInstance = new OracleDb(connectionString);
                return oracleDBInstance;
            }
            else
                return oracleDBInstance;
        }


        public List<Customer> GetAllCustomer()
        {
            String strConnectionString = DatabaseConnectionString;
            List<Customer> lst = new List<Customer>();
            OracleCommand oraCmd = new OracleCommand();
            System.Data.DataTable outDT = null;
            oraCmd.CommandText = "select SCREEN_SCRAPER_ID, PRMRY_USER_NO from INT_UNLTD_SCREEN_SCRAPER_QUEUE where Status = \'PENDING\' ";
            oraCmd.CommandType = System.Data.CommandType.Text;
            OracleConnection conn = new OracleConnection(strConnectionString);
            conn.Open();
            oraCmd.Connection = conn;
           // oraCmd.Parameters.Add("outTable", OracleDbType.RefCursor, ParameterDirection.Output);
            outDT = new System.Data.DataTable();
            oraCmd.ExecuteNonQuery();
            OracleDataAdapter da = new OracleDataAdapter(oraCmd);
            da.Fill(outDT);
            conn.Close();
            foreach (DataRow row in outDT.Rows)
            {
                lst.Add(new Customer
                {
                    CustomerB1Number = (row["PRMRY_USER_NO"] != DBNull.Value) ? row["PRMRY_USER_NO"].ToString() : string.Empty,
                    uniqueId = (row["SCREEN_SCRAPER_ID"] != DBNull.Value) ? Int32.Parse(row["SCREEN_SCRAPER_ID"].ToString())  : 0

                });
            }
            return lst;
        }


        public bool UpdateInformation(Customer customer, string Status, string ProcessNote)
        {
            String strConnectionString = DatabaseConnectionString;
            OracleCommand oraCmd = new OracleCommand();
            System.Data.DataTable outDT = null;
            oraCmd.CommandText = "UPDATE INT_UNLTD_SCREEN_SCRAPER_QUEUE SET RECORD_UPDATED=sysdate, STATUS = \'"+ Status + "\' , PROCESS_NOTES=\'" + ProcessNote + "\' WHERE SCREEN_SCRAPER_ID = \'"+ customer.uniqueId + "\' ";
            oraCmd.CommandType = System.Data.CommandType.Text;
            OracleConnection conn = new OracleConnection(strConnectionString);
            conn.Open();
            oraCmd.Connection = conn;
            // oraCmd.Parameters.Add("outTable", OracleDbType.RefCursor, ParameterDirection.Output);
            outDT = new System.Data.DataTable();
            oraCmd.ExecuteNonQuery();
            conn.Close();
            return true;
        }

        //public bool RemoveKey(string s)
        //{
        //    return true;
        //}

        //public List<String> LoadCanadaPostKeyFromDB()
        //{
        //    String strConnectionString = DatabaseConnectionString;
        //    List<String> lst = new List<String>();
        //    OracleCommand oraCmd = new OracleCommand();
        //    System.Data.DataTable outDT = null;
        //    oraCmd.CommandText = "canadapost_addressautomation.GetCP_ACTIVEKEYS";
        //    oraCmd.CommandType = CommandType.StoredProcedure;
        //    //OracleDB_PRADMS oraclePRAMS = new OracleDB_PRADMS();
        //    // oraclePRADMSDB.OpenConnnection();

        //    OracleConnection conn = new OracleConnection(strConnectionString);
        //    conn.Open();
        //    oraCmd.Connection = conn;
        //    oraCmd.Parameters.Add("outTable", OracleDbType.RefCursor, ParameterDirection.Output);//.Direction = ParameterDirection.Output;
        //    //  oraclePRADMSDB.Execute_Procedure_WithResult(ref oraCmd, out outDT);
        //    outDT = new System.Data.DataTable();
        //    // oraCmd.CommandType = System.Data.CommandType.StoredProcedure;
        //    oraCmd.ExecuteNonQuery();
        //    OracleDataAdapter da = new OracleDataAdapter(oraCmd);
        //    da.Fill(outDT);

        //    // oraclePRADMSDB.CloseConnnection();
        //    conn.Close();

        //    foreach (DataRow row in outDT.Rows)
        //    {
        //        lst.Add(row[0].ToString());
        //    }

        //    return lst;
        //}

        //public bool SaveAddress_InDB(IList<CanadaPostAddress> AddressList)
        //{

        //    IList<CanadaPostAddress> BulkLst = AddressList;
        //    //   oraclePRADMSDB.OpenConnnection();
        //    OracleConnection conn = new OracleConnection(DatabaseConnectionString);
        //    conn.Open();

        //    try
        //    {
        //        using (var command = conn.CreateCommand())
        //        {
        //            command.CommandText = "canadapost_addressautomation.Store_CanadapostAddress";
        //            command.CommandType = CommandType.StoredProcedure;
        //            command.BindByName = true;
        //            command.ArrayBindCount = BulkLst.Count;
        //            command.Parameters.Add("inId", OracleDbType.Varchar2, BulkLst.Select(c => c.Id).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inDomesticId", OracleDbType.Varchar2, BulkLst.Select(c => c.DomesticId).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inDepartment", OracleDbType.Varchar2, BulkLst.Select(c => c.Department).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inCompany", OracleDbType.Varchar2, BulkLst.Select(c => c.Company).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inUnit", OracleDbType.Varchar2, BulkLst.Select(c => c.SubBuilding).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inBuildingNumber", OracleDbType.Varchar2, BulkLst.Select(c => c.BuildingNumber).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inBuildingName", OracleDbType.Varchar2, BulkLst.Select(c => c.BuildingName).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inSecondaryStreet", OracleDbType.Varchar2, BulkLst.Select(c => c.SecondaryStreet).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inStreet", OracleDbType.Varchar2, BulkLst.Select(c => c.Street).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inBlock", OracleDbType.Varchar2, BulkLst.Select(c => c.Block).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inNeighbourhood", OracleDbType.Varchar2, BulkLst.Select(c => c.Neighbourhood).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inDistrict", OracleDbType.Varchar2, BulkLst.Select(c => c.District).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inCity", OracleDbType.Varchar2, BulkLst.Select(c => c.City).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inLine1", OracleDbType.Varchar2, BulkLst.Select(c => c.Line1).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inLine2", OracleDbType.Varchar2, BulkLst.Select(c => c.Line2).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inLine3", OracleDbType.Varchar2, BulkLst.Select(c => c.Line3).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inLine4", OracleDbType.Varchar2, BulkLst.Select(c => c.Line4).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inLine5", OracleDbType.Varchar2, BulkLst.Select(c => c.Line5).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inAdminAreaName", OracleDbType.Varchar2, BulkLst.Select(c => c.AdminAreaCode).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inAdminAreaCode", OracleDbType.Varchar2, BulkLst.Select(c => c.AdminAreaName).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inProvince", OracleDbType.Varchar2, BulkLst.Select(c => c.Province).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inProvinceName", OracleDbType.Varchar2, BulkLst.Select(c => c.ProvinceName).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inProvinceCode", OracleDbType.Varchar2, BulkLst.Select(c => c.ProvinceCode).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inPostalCode", OracleDbType.Varchar2, BulkLst.Select(c => c.PostalCode).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inCountryName", OracleDbType.Varchar2, BulkLst.Select(c => c.CountryName).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inCountryIso2", OracleDbType.Varchar2, BulkLst.Select(c => c.CountryIso2).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inCountryIso3", OracleDbType.Varchar2, BulkLst.Select(c => c.CountryIso3).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inCountryIsoNumber", OracleDbType.Varchar2, BulkLst.Select(c => c.CountryIsoNumber).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inSortingNumber1", OracleDbType.Varchar2, BulkLst.Select(c => c.SortingNumber1).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inSortingNumber2", OracleDbType.Varchar2, BulkLst.Select(c => c.SortingNumber2).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inBarcode", OracleDbType.Varchar2, BulkLst.Select(c => c.Barcode).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inPOBoxNumber", OracleDbType.Varchar2, BulkLst.Select(c => c.POBoxNumber).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inLabel", OracleDbType.Varchar2, BulkLst.Select(c => c.Label).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inType", OracleDbType.Varchar2, BulkLst.Select(c => c.Type).ToArray(), ParameterDirection.Input);
        //            command.Parameters.Add("inDataLevel", OracleDbType.Varchar2, BulkLst.Select(c => c.DataLevel).ToArray(), ParameterDirection.Input);

        //            int result;
        //            try
        //            {
        //                result = command.ExecuteNonQuery();
        //            }
        //            catch (Exception ex)
        //            {
        //                throw ex;
        //            }

        //            // int result = oraclePRADMSDB.Execute_Procedure_BulkList(command);

        //            if (result == BulkLst.Count)
        //                return true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //    finally
        //    {
        //        conn.Close();//.CloseConnnection();
        //    }

        //    conn.Close();

        //    // /oraclePRADMSDB.CloseConnnection();
        //    return true;
        //}

        //public bool UpdateAddressCompleteStatusInDB(WatchAddress inAddress)
        //{
        //    String strConnectionString = DatabaseConnectionString;
        //    OracleCommand oraCmd = new OracleCommand();
        //    OracleConnection conn = new OracleConnection(strConnectionString);
        //    conn.Open();
        //    oraCmd.Connection = conn;
        //    oraCmd.CommandText = "canadapost_addressautomation.Update_AddressListStatus";
        //    oraCmd.CommandType = CommandType.StoredProcedure;
        //    oraCmd.Parameters.Add("inBuildingnum", inAddress.BuildingNumber);
        //    oraCmd.Parameters.Add("inStreetname", inAddress.StreetName);
        //    oraCmd.Parameters.Add("inCity", inAddress.City);
        //    oraCmd.Parameters.Add("inProvabrv", inAddress.Province);
        //    oraCmd.Parameters.Add("inPostalcode", inAddress.PostalCode);

        //    try
        //    {
        //        oraCmd.ExecuteNonQuery();

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    conn.Close();
        //    return true;
        //}



    }
}
