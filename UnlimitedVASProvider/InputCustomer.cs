﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleWebScrapper.Common;
using System.Configuration;
using UnlimitedVASProvider.Configuration;
using UnlimitedVASProvider.DataModel;
using Serilog;

namespace UnlimitedVASProvider
{
    class InputCustomer : CustomerRepository
    {

        private  string _oraConnection ;
        private string _username;
        private string _password;
    

        public InputCustomer(ILogger logger) : base(logger)
        {
            var config = ConfigurationManager.GetSection("unlimitedVasProviderSetting") as ProviderConfigurationSection;
            if (config != null)
            {
                _oraConnection = config.RepositorySetting.OracleConnection;
                _username = config.RepositorySetting.UserName;
                _password = config.RepositorySetting.Password;
            }
            else
            {
                throw new Exception("Provider Setting is missing , Please add setting in AppSetting for section 'pbw28ProviderSetting'");
            }
            //  _logger = logger;
            Initialize();


        }

        public List<Customer> CustomerList;

        public override List<Customer> Customers => CustomerList;

        public override void Initialize()
        {
            // Create Oracle connection
            //_oracleconnection

            // read oracle Table
            //_username
            // read all the customer columns and save in Customers = 

          //   this.CustomerList = new List<Customer>();

         //   this.CustomerList.Add(new Customer() { CustomerB1Number= "b1aqhn13", uniqueId=22 });

         //   this.CustomerList.Add(new Customer() { CustomerB1Number = "b1rpvw49", uniqueId = 22 });
     

            var oracle = OracleDb.GetInstance(_oraConnection);
            this.CustomerList = oracle.GetAllCustomer();



        }




    }
}
