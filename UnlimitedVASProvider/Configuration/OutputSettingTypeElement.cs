using System.Configuration;

namespace UnlimitedVASProvider.Configuration
{
    public class OutputSettingTypeElement : ConfigurationElement
    {
        [ConfigurationProperty("oracleConnnectionString", IsRequired = true, IsKey = true)]
        public string ConnectionSetting
        {
            get { return (string)base["oracleConnnectionString"]; }
            set { base["oracleConnnectionString"] = value; }
        }

        [ConfigurationProperty("username", IsRequired = true, IsKey = true)]
        public string Username
        {
            get { return (string)base["username"]; }
            set { base["username"] = value; }
        }

        [ConfigurationProperty("password", IsRequired = true, IsKey = true)]
        public string Password
        {
            get { return (string)base["password"]; }
            set { base["password"] = value; }
        }
    }
}