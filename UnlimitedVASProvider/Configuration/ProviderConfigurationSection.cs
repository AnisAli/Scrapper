﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnlimitedVASProvider.Configuration
{
    public class ProviderConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("repositorySetting", IsRequired = true)]
        public RepoSettingTypeElement RepositorySetting
        {
            get { return (RepoSettingTypeElement)base["repositorySetting"]; }
            set { base["repositorySetting"] = value; }
        }

        [ConfigurationProperty("outputSetting", IsRequired = true)]
        public OutputSettingTypeElement OutputSetting
        {
            get { return (OutputSettingTypeElement)base["outputSetting"]; }
            set { base["outputSetting"] = value; }
        }
    }
}
