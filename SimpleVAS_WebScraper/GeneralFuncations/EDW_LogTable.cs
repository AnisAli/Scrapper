﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Teradata.Client.Provider;

namespace SimpleVAS_WebScraper.GeneralFuncations
{

    public class EDW_LogTable
    {
        public bool AddLog(ref TeradataDB EDW, String insAcctNo, String insStatus, String insComment, String VAS_TYPE)
        {

            String Query = "INSERT INTO GRP_ADMS_HHPM.PERM_TEMP_VAS_WEB_LOG  ('" + insAcctNo + "', '" + insStatus + "', current_timestamp, '" + insComment + "','" + VAS_TYPE + "')";
            try
            {
                EDW.ExecuteInsertQuery(Query);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("[AddLog] Func " + ex.Message);

            }

        }


        public bool AddException(ref TeradataDB EDW, String insFunctionName, String insExeceptionMsg)
        {

            String Query =
                "INSERT INTO GRP_ADMS_HHPM.PERM_TEMP_VAS_EXCEPTIONS SELECT (SELECT  NVL(MAX(EXP_ID) + 1,1) ID FROM   GRP_ADMS_HHPM.PERM_TEMP_VAS_EXCEPTIONS ) EXP_ID, CURRENT_TIMESTAMP, ' " + insFunctionName + "', '" + insExeceptionMsg + "' ";
            try
            {
                EDW.ExecuteInsertQuery(Query);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("[AddException] Func " + ex.Message);

            }

        }

    }
}
