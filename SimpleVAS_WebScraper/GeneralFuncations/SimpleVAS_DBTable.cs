﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using Teradata.Client.Provider;
using SimpleVAS_WebScraper.GeneralFuncations;
using System.IO;

namespace SimpleVAS_WebScraper.GeneralFuncations
{
    public class SimpleVAS_DBTable
    {
        TeradataDB EDW = null;
        static private String query_ADDVAS_B1List = "";
        static private String query_REMOVEVAS_B1List = "";

        public string FolderName { get; private set; }
        public string AddCustomerFile { get; private set; }

        public string KickoutCustomerFile { get; private set; }

        static public bool LoadQueries_FromFile(String QueryFolder)
        {
            if (QueryFolder == "") return false;
            query_ADDVAS_B1List = GeneralFuncations.GeneralFunctions.ReadQuery_FromFile(QueryFolder, Constants.SQLQueries.SIMPLE_VAS_DAILY_Queries.ADDVAS_B1LIST_QueryFile);
            query_REMOVEVAS_B1List = GeneralFuncations.GeneralFunctions.ReadQuery_FromFile(QueryFolder, Constants.SQLQueries.SIMPLE_VAS_DAILY_Queries.RemoveVAS_B1LIST_QueryFile);

            return false;
        }


        public  SimpleVAS_DBTable(String insDataSource, String insUserName, String insPassword)
        {
            EDW = new TeradataDB(insDataSource,insUserName,insPassword);
        }

        public SimpleVAS_DBTable(String folderName, string tick)
        {
           // EDW = new TeradataDB(insDataSource, insUserName, insPassword);
            this.FolderName = folderName;
            AddCustomerFile = "Add_" + tick + ".txt";
            KickoutCustomerFile = "Fail_" + tick + ".txt";
        }

        public ArrayList Get_B1List()
        {
            ArrayList returnList = new ArrayList();

            return LoadCSVFileforToday();
        }

        public ArrayList LoadAlreadyDoneList()
        {
            string fileName = FolderName + "\\" + "Done_" + DateTime.Now.Day + "-" + DateTime.Now.Month + "-" + DateTime.Now.Year + ".txt"; ;
            ArrayList listA = new ArrayList();
            using (var fs = File.OpenRead(fileName))
            using (var reader = new StreamReader(fs))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    listA.Add(line);

                }
            }
            return listA;
        }
        public ArrayList LoadCSVFileforToday()
        {
            string fileName = FolderName + "\\" + "InputCustomer.csv";
            ArrayList listA = new ArrayList();
            using (var fs = File.OpenRead(fileName))
            using (var reader = new StreamReader(fs))
            {  
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');
                    var date = values[3];
                    var todayDate = DateTime.Now.Day.ToString();
                    if (date == todayDate)
                        listA.Add(values[0]);
                }
            }
            return listA;
        }

        public ArrayList Get_B1List_for_AddVas(){
            ArrayList returnList = new ArrayList();
            String Query = query_ADDVAS_B1List; //" select DISTINCT PRMRY_USER_NO from  GRP_ADMS_HHPM.PERM_TEMP_USG_VAS_DAILY where VAS_ADD_STATUS in ( 'PENDING' , 'KICKOUT' ) OR VAS_ADD_STATUS IS NULL";
            try
            {
                EDW.OpenConnnection();
                TdDataReader reader = EDW.ExecuteDataQuery(Query);
                while (reader.Read())
                {
                   returnList.Add(reader.GetValue(0));
                }
                return returnList;
            }
            catch(Exception ex){
                throw new Exception("Get_B1List() Func " + ex.Message);
            }

        }

       public ArrayList Get_B1List_for_RemoveVas()
       {
           ArrayList returnList = new ArrayList();
          //Final query with BllingCycle <= current_date -1
           //String Query = "select DISTINCT PRMRY_USER_NO  from  GRP_ADMS_HHPM.PERM_TEMP_USG_VAS_DAILY where VAS_ADD_STATUS in ( 'COMPLETE' ) AND  VAS_ADD_DT IS NOT NULL AND BILL_CYCLE_DT <= CURRENT_DATE -1 and VAS_REMOVE_DT is NULL AND COALESCE(VAS_REMOVE_STATUS, ' ') NOT IN ('" + Constants.REMOVE_VAS.Status.__REMOVED + "' , '" + Constants.REMOVE_VAS.Status.__NO_VAS_EXIST + "') ";

           //temp query
           String Query = query_REMOVEVAS_B1List;// "select DISTINCT PRMRY_USER_NO  from  GRP_ADMS_HHPM.PERM_TEMP_USG_VAS_DAILY where VAS_ADD_STATUS in ( 'COMPLETE' ) AND  VAS_ADD_DT IS NOT NULL and VAS_REMOVE_DT is NULL AND COALESCE(VAS_REMOVE_STATUS, ' ') NOT IN ('" + Constants.REMOVE_VAS.Status.__REMOVED + "' , '" + Constants.REMOVE_VAS.Status.__NO_VAS_EXIST + "') ";
           try
           {
               EDW.OpenConnnection();
               TdDataReader reader = EDW.ExecuteDataQuery(Query);
               while (reader.Read())
               {
                   returnList.Add(reader.GetValue(0));
               }
               return returnList;
           }
           catch (Exception ex)
           {
               throw new Exception("Get_B1List_for_RemoveVas() Func " + ex.Message);
           }

       }


        public void AddInTextFile(string text, string fileName)
        {
            string LogLine = text;
            try
            {
                using (StreamWriter outputFile = new StreamWriter(FolderName + @"\" + fileName, append: true))
                {
                    outputFile.WriteLine(LogLine);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
       public void AddVAS_UpdateStatus(String insUserID, String inStatus){


            string logText = "";
            if (inStatus == "COMPLETE")
            {
                logText = insUserID;
                AddInTextFile(logText, AddCustomerFile);
            }
            else if(inStatus == "ALREADY")
            {
                logText = insUserID + "           [Already PBW28]";
                AddInTextFile(logText, AddCustomerFile);
            }
            else
            {
                logText = insUserID + "          ["+ inStatus + "]";
                AddInTextFile(logText, KickoutCustomerFile);
            }

           //String FunctionName = "SimpleVAS_DBTable.AddVAS_UpdatePendingStatus";
           //String query ="";
           //if(inStatus == Constants.ADD_VAS.Status.__COMPLETE)
           //    query = "UPDATE  GRP_ADMS_HHPM.PERM_TEMP_USG_VAS_DAILY SET VAS_ADD_STATUS = '" + inStatus + "' , VAS_ADD_RETRY = NVL(VAS_ADD_RETRY + 1, 1) , VAS_ADD_DT = current_date where prmry_user_no = '" + insUserID + "' ";
           //else
           //    query = "UPDATE  GRP_ADMS_HHPM.PERM_TEMP_USG_VAS_DAILY SET VAS_ADD_STATUS = '" + inStatus + "' , VAS_ADD_RETRY = NVL(VAS_ADD_RETRY + 1, 1) where prmry_user_no = '" + insUserID + "' ";
           //try
           //{
           //    EDW.OpenConnnection();
           //    EDW.ExecuteUpdateQuery(query);
           //    EDW.CloseConnnection();
           //}
           //catch (Exception ex)
           //{
           //    Logging.Log_Exception(FunctionName, ex.Message);
           //    throw new Exception(FunctionName + ex.Message);
           //}



       }

       public void RemoveVAS_UpdateStatus(String insUserID, String inStatus)
       {
           String FunctionName = "SimpleVAS_DBTable.AddVAS_UpdatePendingStatus";
           String query = "";
           if (inStatus == Constants.REMOVE_VAS.Status.__REMOVED)
               query = "UPDATE  GRP_ADMS_HHPM.PERM_TEMP_USG_VAS_DAILY SET VAS_REMOVE_STATUS = '" + inStatus + "' , VAS_REMOVE_RETRY = NVL(VAS_REMOVE_RETRY + 1, 1) , VAS_REMOVE_DT = current_date where prmry_user_no = '" + insUserID + "' ";
           else
               query = "UPDATE  GRP_ADMS_HHPM.PERM_TEMP_USG_VAS_DAILY SET VAS_REMOVE_STATUS = '" + inStatus + "' , VAS_REMOVE_RETRY = NVL(VAS_REMOVE_RETRY + 1, 1) where prmry_user_no = '" + insUserID + "' ";
           try
           {
               EDW.OpenConnnection();
               EDW.ExecuteUpdateQuery(query);
               EDW.CloseConnnection();
           }
           catch (Exception ex)
           {
               Logging.Log_Exception(FunctionName, ex.Message);
               throw new Exception(FunctionName + ex.Message);
           }



       }


    }
}
