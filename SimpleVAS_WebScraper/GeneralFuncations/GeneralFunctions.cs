﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SimpleVAS_WebScraper.GeneralFuncations
{
    static public class GeneralFunctions
    {

        static public String ReadQuery_FromFile(String Path, String FileName)
        {
            try
            {
                VerifyQuery(Path, FileName);  // exit program if query has any error
                StreamReader reader = new StreamReader(Path + FileName);
                String query = reader.ReadLine();
                reader.Close();
                return query;
            }
            catch (Exception ex)
            {
                Logging.Log_Exception("ReadQuery_FromFile - [" + FileName + "]", ex.Message);
                Email.Generate_QueryFileErrorEmail(Path+FileName, ex.Message);
                System.Environment.Exit(-1);
            }


            return "";
        }

        static public bool VerifyQuery(String Path, String FileName)
        {
            try
            {

                StreamReader reader = new StreamReader(Path + FileName);
                String query = reader.ReadToEnd();
                if (query.Substring(0,query.Length-2).Contains("\n"))
                    throw new Exception("Query file contains multiple lines");

                if (query.Contains("--"))
                    throw new Exception("Query file contains comments");

                if (query.Contains("/*"))
                    throw new Exception("Query file contains comments");

                reader.Close();
                return true;
            }
            catch (Exception ex)
            {
                Logging.Log_Exception("ReadQuery_FromFile - [" + FileName + "]", ex.Message);
                Email.Generate_QueryFileErrorEmail(Path + FileName, ex.Message);
                System.Environment.Exit(-1);
            }


            return true;
        }

        static public bool IsFolderExist(String Path)
        {
            return Directory.Exists(Path);
        }

        static public bool CreateFolder(String Path, String FolderName)
        {
           try 
            {
            // Try to create the directory.
            DirectoryInfo di = Directory.CreateDirectory(Path + FolderName );
             return true;
             } 
             catch (Exception e) 
             {
                throw new Exception("The process failed: "+ e.ToString() + " "+ e.Message);
            } 
           
        }

        static public void CreateFolderIfNotExist(String Path, String FolderName)
        {
            if (IsFolderExist(Path + FolderName))
                return;
            else
                CreateFolder(Path, FolderName);
        }


    }
}
