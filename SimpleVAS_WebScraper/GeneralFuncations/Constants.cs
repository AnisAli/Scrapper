﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleVAS_WebScraper.GeneralFuncations
{
    public static class Constants
    {

        static public class CONFIG_KEYS
        {

            static public String EMAIL_HOST = "EMAIL_HOST";

            static public String EMAIL_LIST = "EMAIL_LIST";
            static public String EMAIL_EXCEPTION_LIST = "EMAIL_EXCEPTION_LIST";
            static public String EMAIL_PORT = "EMAIL_PORT";
            static public String FROM_EMAIL = "FROM_EMAIL";
            static public String SIMPLE_PASS = "SIMPLE_PASS";
            static public String SIMPLE_URL = "SIMPLE_URL";
            static public String SIMPLE_USERID = "SIMPLE_USERID";
            static public String ADDITIONAL_VAS_SERVICES = "VAS_SERVICES";
        }

        static public class LOG
        {
            static public String ERROR = "ERROR";
            static public String SUCCESS = "OK";
            static public String UNKNOWN = "UNKNOWN";

           

            static public class ErrorDescription
            {
                static public String EDIT_UNLIMITED_DISABLED = "Edit Option is disabled and Customer already ordered Unlimited Usage";
                static public String EDIT_NO_VAS_DISABLED = "Edit Option is disabled and Customer do not have unlimited usage VAS";
                static public String EDIT_UNKNOWNN = "Edit Option is disabled , VAS Info Unknow, Please check logs";
                static public String UNKNOWN = "UNKNOWN";
                static public String EDIT_PAGE_RETURN_ERROR = "Error after click on EDIT Button";
                static public String UNEXPECTED_PAGE_RETURN = "Wrong page return- please check html logs";
                static public String APPLY_VAS_ERROR = "Error generated when automation apply temp vas- check html logs";
                static public String POST_MESSAGE_GENERATION_ERROR = "POST Message generated was invalid, please check logs formid, seedid and compnent tags in html";
                static public String NEW____UNLIMITED_NOT_FOUND = "edit is enabled but Unlimited is not in the list";
                static public String NEW____NO_UNLIMITED_CHECKBOX = "edit page doesnot have unlimited plan dropdowm list";
                static public String NEW____NotChecked_UNLIMITED_PLAN = "Unlimited Plan is not checked on edit page";
            }

            static public class RemoveVAS_ErrorDescription
            {
                static public String EDIT_DISABLE_NO_TEMP_VAS = "Edit option is disabled and Customer do not have temp VAS";
                static public String EDIT_ENABLE_NO_TEMP_VAS = "Edit option is enabled and Customer do not have temp VAS";
                static public String EDIT_DISABLE_TEMP_VAS_EXIST = "Edit option is disabled and Customer have temp VAS";
                static public String EDIT_NO_VAS_DISABLED = "Edit Option is disabled and Customer do not have unlimited usage VAS";
                static public String EDIT_UNKNOWNN = "Edit Option is disabled , VAS Info Unknow, Please check logs";
                static public String UNKNOWN = "UNKNOWN";
                static public String EDIT_PAGE_RETURN_ERROR = "Error after click on EDIT Button";
                static public String UNEXPECTED_PAGE_RETURN = "Wrong page return- please check html logs";
                static public String REMOVE_VAS_ERROR = "Error generated when automation remove temp vas - check html logs";
                static public String POST_MESSAGE_GENERATION_ERROR = "POST Message generated was invalid, please check logs formid, seedid and compnent tags in html";
            }

        }

        static public class ADD_VAS
        {
            static public String VAS_TYPE = "ADD_VAS";
            static public class Status
            {
                static public String __PENDING = "PENDING";
                static public String __COMPLETE = "COMPLETE";
                static public String __KICKOUT = "KICKOUT";
                static public String __ERROR = "ERROR";
                static public String __EXISTING_UVAS = "EXISTING_UVAS";
            }
        }

        static public class REMOVE_VAS
        {
            static public String VAS_TYPE = "REMOVE_VAS";
            static public class Status
            {
               
                static public String __REMOVED = "REMOVED";  
                static public String __NO_VAS_EXIST = "TUVAS_NOT_EXIST";  // not select in b1list remove query
                static public String __PENDING_REMOVE = "PENDING_REMOVE"; // selett again in remove query
               // static public String __KICKOUT = "KICKOUT";
                static public String __ERROR = "ERROR";  // select again in remove query

            }
        }


        static public class SQLQueries
        {
            static public class SIMPLE_VAS_DAILY_Queries
            {
                static public String ADDVAS_B1LIST_QueryFile = "AddVAS_B1List.sql";
                static public String RemoveVAS_B1LIST_QueryFile = "RemoveVAS_B1List.sql";
            //  static public String MoveKickout_Query = "MoveKickout.sql";
            // static public String DeleteKickout_Query = "DeleteKickout.sql";
            }
        }

    }
}
