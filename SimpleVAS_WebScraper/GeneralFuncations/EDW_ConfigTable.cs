﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using Teradata.Client.Provider;

namespace SimpleVAS_WebScraper.GeneralFuncations
{
    public class EDW_ConfigTable
    {

        public Dictionary<string, string> LoadConfiguration(String EDW_HOST,String EDW_USER, String EDW_PASS)
        {
           // ArrayList returnList = new ArrayList();
            Dictionary<string, string> configTable =
            new Dictionary<string, string>();

            TeradataDB EDW = new TeradataDB(EDW_HOST, EDW_USER, EDW_PASS);
            String Query = "select ID, Val from GRP_ADMS_HHPM.PERM_TEMP_VAS_CONFIG "; //" select DISTINCT PRMRY_USER_NO from  GRP_ADMS_HHPM.PERM_TEMP_USG_VAS_DAILY where VAS_ADD_STATUS in ( 'PENDING' , 'KICKOUT' ) OR VAS_ADD_STATUS IS NULL";
            try
            {
                EDW.OpenConnnection();
                TdDataReader reader = EDW.ExecuteDataQuery(Query);
                while (reader.Read())
                {
                  //  returnList.Add(reader.GetValue(0));
                    configTable.Add(reader.GetValue(0).ToString(), reader.GetValue(1).ToString());
                }
                return configTable;
            }
            catch (Exception ex)
            {
                throw new Exception("LoadConfiguration Func " + ex.Message);
            }

        }

    }
}
