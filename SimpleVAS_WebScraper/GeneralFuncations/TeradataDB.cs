﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Teradata.Client.Provider;

namespace SimpleVAS_WebScraper.GeneralFuncations
{
    public class TeradataDB
    {
        String strConnectionString ="";
        TdConnection conn = null;
        string FolderName = "";
     	public TeradataDB(String inDataSource, String inUserName, String inPassword)
	    {
       
            strConnectionString = "Data Source=" +inDataSource  +";User Id="+inUserName +";Password="+inPassword +";" ;
	    }

        public TeradataDB(string folderName)
        {
            this.FolderName = folderName;
        }

        public bool OpenConnnection(){
                 try
                 {
                     conn = new TdConnection(strConnectionString);
                     conn.Open();

                 }
                 catch (Exception ex)
                 {
                     throw new Exception("Teradata Open Connection Problem" + ex.Message);
                 }
                 return true;
         }

        public bool CloseConnnection()
         {         

                 try
                 {
                      if (null != conn)
                          conn.Close(); 
                     return true;
                 }
                 catch (Exception ex)
                 {
                    throw new Exception("Teradata Close Connection Problem" + ex.Message);
                 }
                
         }

        public bool ExecuteQuery(String Query){

              TdCommand cmd = null;
            try{
                cmd = new TdCommand(Query, conn);
                String customers = (String) cmd.ExecuteScalar();
                cmd.Dispose();
                return true;
              }
            catch(Exception ex){
                 throw new Exception("Teradata Execute Scalar Query Problem" + ex.Message);
            }
        }

        public TdDataReader ExecuteDataQuery(String Query)
        {

            TdCommand cmd = null;
            try
            {
                cmd = new TdCommand(Query, conn);
                return cmd.ExecuteReader();
            }
            catch (Exception ex)
            {
                throw new Exception("Teradata Execute Data Reader Query Problem" + ex.Message);
            }
        }

        public bool ExecuteInsertQuery(String Query)
        {

            TdCommand cmd = null;
            try
            {
                cmd = new TdCommand(Query, conn);
                int rowaffected = cmd.ExecuteNonQuery();
                return (rowaffected > 0) ? true : false;
            }
            catch (Exception ex)
            {
                throw new Exception("Teradata Execute Insert Query Problem" + ex.Message);
            }
        }

        public bool ExecuteUpdateQuery(String Query)
        {
            return ExecuteInsertQuery(Query);
        }
    }
}
