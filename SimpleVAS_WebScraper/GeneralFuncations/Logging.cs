﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace SimpleVAS_WebScraper.GeneralFuncations
{
    public static class Logging
    {
       static private TeradataDB EDW = null;
       static private String  LogFolder = null;
       static private EDW_LogTable LogTable = null;
        static private string LogFileName = null;
        static private string ExceptionFileName = null;
        static private string AlreadyProcessFileName = null;
        public static void LogSetting(String insDataSource,String insUser,String insPassword, String insLogFolder, String RUN_FOLDER){


            //GeneralFuncations.GeneralFunctions.CreateFolderIfNotExist(RUN_FOLDER, "logs");
            // EDW = new TeradataDB(insDataSource, insUser, insPassword);
            // LogFolder = insLogFolder;
            // LogTable = new EDW_LogTable();

            
            LogFolder = RUN_FOLDER;
            LogFileName = "Log_" + DateTime.Now.Ticks + ".txt";
            ExceptionFileName = "Exception_" + DateTime.Now.Ticks + ".txt";
            AlreadyProcessFileName = "Done" + "_" + DateTime.Now.Day + "-" + DateTime.Now.Month + "-" + DateTime.Now.Year+".txt";
        }

       public static void Add(String insAccountNo, String insStatus, String insComment, String VAS_TYPE="ADD_VAS") {
            //try { 
            //EDW.OpenConnnection();
            //DateTime CurrentTimeStamp = DateTime.Now;
            //LogTable.AddLog(ref EDW, insAccountNo, insStatus, insComment, VAS_TYPE);
            //    }
            //catch(Exception ex)
            //{
            //    throw;
            //}
            string LogLine = "Account=["+ insAccountNo + "] Status=["+ insStatus + "] insComment=["+ insComment + "]";
            try
            {
                using (StreamWriter outputFile = new StreamWriter(LogFolder + @"\"+ LogFileName,append:true))
                {
                        outputFile.WriteLine(LogLine);
                }
                using (StreamWriter outputFile = new StreamWriter(LogFolder + @"\" + AlreadyProcessFileName, append: true))
                {
                    outputFile.WriteLine(insAccountNo);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

       public static void Log_Exception(String FunctionName, String ExceptionMsg){
            //try { 
            //  EDW.OpenConnnection();
            //  DateTime CurrentTimeStamp = DateTime.Now;

            //  ExceptionMsg = ExceptionMsg.Replace("'", " ");

            //  LogTable.AddException(ref EDW, FunctionName, ExceptionMsg);
            //    }
            //  catch(Exception ex)
            //  {
            //      throw new Exception(ex.Message);
            //  }
            string LogLine = "FunctionName=[" + FunctionName + "] ExceptionMsg=[" + ExceptionMsg + "]";
            try
            {
                using (StreamWriter outputFile = new StreamWriter(LogFolder + @"\" + ExceptionFileName, append: true))
                {
                    outputFile.WriteLine(LogLine);
                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }

       public static void Log_Exception(String FunctionName, String ExceptionMsg, String inUserID)
        {
            //try
            //{
            //    EDW.OpenConnnection();
            //    DateTime CurrentTimeStamp = DateTime.Now;
            //    LogTable.AddException(ref EDW, FunctionName,"["+ inUserID + "]  " + ExceptionMsg);
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception(ex.Message);
            //}

            string LogLine = "FunctionName=[" + FunctionName + "] UserID=["+ inUserID + "] ExceptionMsg=[" + ExceptionMsg + "]";
            try
            {
                using (StreamWriter outputFile = new StreamWriter(LogFolder + @"\" + ExceptionFileName, append: true))
                {
                    outputFile.WriteLine(LogLine);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

       public static void LogHtmlFile(String insAcctNo, String LogFileData)
        {

            String FunctionName = "Logging.LogHtmlFile";
            StreamWriter writer = null;
            String LogFileName = insAcctNo + "_" + DateTime.Now.Day + DateTime.Now.Month + DateTime.Now.Year + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Second + ".html";
            try
            {
                writer = new StreamWriter(LogFolder + LogFileName);
                writer.WriteLine(LogFileData);
                writer.Close();
            }
            catch (Exception ex)
            {
                Log_Exception(FunctionName, ex.Message);
                throw new Exception(ex.Message);
            }


        }

    }
}
