﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using SimpleVAS_WebScraper.GeneralFuncations;
using System.Collections;

namespace SimpleVAS_WebScraper
{
    public static class Email
    {
        static public String DistributionListFolder ;
        static public String ToList = "";
        static public String ToExceptionList = "";
        static public String ToList_FileName;
        static private String SMTP_HOST = "";
        static private int SMTP_PORT = 0;
        static private String FROM_EMAIL = "";

        public static void EmailSettings(Dictionary<String, String> inConfigTable )
        {
            try
            {
                if (inConfigTable == null) return;
                SMTP_HOST = inConfigTable[Constants.CONFIG_KEYS.EMAIL_HOST];
                SMTP_PORT = Int32.Parse(inConfigTable[Constants.CONFIG_KEYS.EMAIL_PORT]);
                FROM_EMAIL = inConfigTable[Constants.CONFIG_KEYS.FROM_EMAIL];
                ToList = inConfigTable[Constants.CONFIG_KEYS.EMAIL_LIST];
                ToExceptionList = inConfigTable[Constants.CONFIG_KEYS.EMAIL_EXCEPTION_LIST];

            }
            catch (Exception ex)
            {
                throw new Exception("EmailSetting " +ex.Message);
            }
        }

        public static void SendMail(String inSubject, String inBody,String inEmailType, Boolean IsExpetion= false)
        {
            String FunctionName = "Email.SendMail [" + inEmailType + "]";
            try
            {
                String ToEmailList;
                
                if(IsExpetion)
                    ToEmailList = ToExceptionList;
                else
                    ToEmailList = ToList;

                MailMessage mail = new MailMessage(FROM_EMAIL, ToEmailList);
                SmtpClient client = new SmtpClient();
                client.Port = SMTP_PORT;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Host = SMTP_HOST;
                mail.IsBodyHtml = true;
                mail.Subject = inSubject;
                mail.Body = Header + inBody + Footer;
                client.Send(mail);

            }
            catch (Exception Ex)
            {
                GeneralFuncations.Logging.Log_Exception(FunctionName, Ex.Message);
                return;
            }
        }


        public static void SendSummaryMail(String inSubject, String inBody, String inEmailType)
        {
            String FunctionName = "Email.SendMail [" + inEmailType + "]";
            try
            {
                MailMessage mail = new MailMessage(FROM_EMAIL, ToList);
                
                SmtpClient client = new SmtpClient();
                client.Port = SMTP_PORT;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Host = SMTP_HOST;
                mail.IsBodyHtml = true;
                mail.Subject = inSubject;
                mail.Body = HeaderSummary + inBody + FooterSummary;
                client.Send(mail);

            }
            catch (Exception Ex)
            {
                GeneralFuncations.Logging.Log_Exception(FunctionName, Ex.Message);
                return;
            }
        }

        public static void Generate_QueryFileErrorEmail(String inFileName, String Exception)
        {
           String FunctionName = "Generate_QueryFileErrorEmail";
           String Subject = "Simple TEMP VAS - Exception";
           String Body = @"<div> ERROR </div> <h5 class='' style='margin-top:20px'>  <div class='column'> <div  class='cross'></div> ERROR</div></h5><span class='clear'></span><br><h5 style='color:red;'> " + Exception + "!</h5><p style='color:red;'> " + inFileName  + " !</p>";        
            SendMail(Subject, Body, FunctionName,true);        }

        public static void Generate_ConfigurationLoadError( String Exception)
        {
            String FunctionName = "Generate_QueryFileErrorEmail";
            String Subject = "Simple TEMP VAS - Config Exception";
            String Body = @" <h5 class='' style='margin-top:20px'>  <div class='column'> <div  class='cross'></div> ERROR</div></h5><span class='clear'></span><br><h5 style='color:red;'> " + Exception + "</h5>";
            SendMail(Subject, Body, FunctionName,true);
        }

        public static void Generate_GeneralError(String Exception)
        {
            String FunctionName = "Generate_QueryFileErrorEmail";
            String Subject = "Simple TEMP VAS - Application Error";
            String Body = @" <h5 class='' style='margin-top:20px'>  <div class='column'> <div  class='cross'></div> ERROR</div></h5><span class='clear'></span><br><h5 style='color:red;'> " + Exception + "</h5>";
            SendMail(Subject, Body, FunctionName,true);
        }


        public static void Generate_SummaryEmail(int inTotalAddVAS, int inTotalRemoveVAS, int AddKickOut, int AddError, int AddPending, 
                               int RemovePending, int RemoveError)
        {
            String FunctionName = "Generate_QueryFileErrorEmail";
            String Subject = "SIMPLE : TEMP VAS Summary";
            String Body = @"
            <table align='left' class='column'>
                    <tr>
                     <td>
                        <h5 class=''>ADD Temp VAS</h5>
                                 Total Add = " + inTotalAddVAS + @"<br>
                                 Pending = " + AddPending + @"<br>
                                Kickouts = " + AddKickOut + @" <br>
                                Errors = " + AddError + @"
                           
                        </td>
                     </tr>
                    </table>
                   <table align='left' class='column'>
                    <tr>
                        <td>
                        <h5 class=''>Remove Temp VAS</h5>
                               Total Removed = " + inTotalRemoveVAS + @"<br>
                                 Pending = " + RemovePending + @"<br>
                                Errors = " + RemoveError + @" <br>
                        </td>
                        </tr>
                    </table>

";
            SendMail(Subject, Body, FunctionName);

        }

        public static void Generate_ServiceChangeEmail(String newServiceIDs){
             // Email sending code
            String FunctionName = "Generate_ServiceChangeEmail";
            String Exception = "[Simple TempVas ADD/Remove Service Closed] New Additonal VAS services found on Edit Page, Please verify manually new services --> New IDs = " + newServiceIDs;
            String Subject = "Simple TEMP VAS - New VAS Exception";
            String Body = @" <h5 class='' style='margin-top:20px'>  <div class='column'> <div  class='cross'></div> ERROR</div></h5><span class='clear'></span><br><h5 style='color:red;'> " + Exception + "</h5>";
            SendMail(Subject, Body, FunctionName,true);
        }

        static private String CSS = @" { 
	margin:0;
	padding:0;
}
* { font-family: Calibri; }

img { 
	max-width: 100%; 
}
.collapse {
	margin:0;
	padding:0;
}
body {
	-webkit-font-smoothing:antialiased; 
	-webkit-text-size-adjust:none; 
	width: 100%!important; 
	height: 100%;
}


/* ------------------------------------- 
		ELEMENTS 
------------------------------------- */
a { color: #2BA6CB;}

.btn {
	text-decoration:none;
	color: #FFF;
	background-color: #666;
	padding:10px 16px;
	font-weight:bold;
	margin-right:10px;
	text-align:center;
	cursor:pointer;
	display: inline-block;
}

p.callout {
	padding:15px;
	background-color:#ECF8FF;
	margin-bottom: 15px;
}
.callout a {
	font-weight:bold;
	color: #2BA6CB;
}

table.social {
/* 	padding:15px; */
	background-color: #ebebeb;
	
}
.social .soc-btn {
	padding: 3px 7px;
	font-size:12px;
	margin-bottom:10px;
	text-decoration:none;
	color: #FFF;font-weight:bold;
	display:block;
	text-align:center;
}
a.fb { background-color: #3B5998!important; }
a.tw { background-color: #1daced!important; }
a.gp { background-color: #DB4A39!important; }
a.ms { background-color: #000!important; }

.sidebar .soc-btn { 
	display:block;
	width:100%;
}

/* ------------------------------------- 
		HEADER 
------------------------------------- */
table.head-wrap { width: 100%;}

.header.container table td.logo { padding: 15px; }
.header.container table td.label { padding: 15px; padding-left:0px;}


/* ------------------------------------- 
		BODY 
------------------------------------- */
table.body-wrap { width: 100%;}


/* ------------------------------------- 
		FOOTER 
------------------------------------- */
table.footer-wrap { width: 100%;	clear:both!important;
}
.footer-wrap .container td.content  p { border-top: 1px solid rgb(215,215,215); padding-top:15px;}
.footer-wrap .container td.content p {
	font-size:10px;
	font-weight: bold;
	
}


/* ------------------------------------- 
		TYPOGRAPHY 
------------------------------------- */
h1,h2,h3,h4,h5,h6 {
font-family: Calibri; line-height: 1.1; margin-bottom:15px; color:#000;
}
h1 small, h2 small, h3 small, h4 small, h5 small, h6 small { font-size: 60%; color: #6f6f6f; line-height: 0; text-transform: none; }

h1 { font-weight:200; font-size: 44px;}
h2 { font-weight:200; font-size: 37px;}
h3 { font-weight:500; font-size: 27px;}
h4 { font-weight:500; font-size: 23px;}
h5 { font-weight:900; font-size: 17px;}
h6 { font-weight:900; font-size: 14px; text-transform: uppercase; color:#444;}

.collapse { margin:0!important;}

p, ul { 
	margin-bottom: 10px; 
	font-weight: normal; 
	font-size:14px; 
	line-height:1.6;
}
p.lead { font-size:17px; }
p.last { margin-bottom:0px;}

ul li {
	margin-left:5px;
	list-style-position: inside;
}

/* ------------------------------------- 
		SIDEBAR 
------------------------------------- */
ul.sidebar {
	background:#ebebeb;
	display:block;
	list-style-type: none;
}
ul.sidebar li { display: block; margin:0;}
ul.sidebar li a {
	text-decoration:none;
	color: #666;
	padding:10px 16px;
/* 	font-weight:bold; */
	margin-right:10px;
/* 	text-align:center; */
	cursor:pointer;
	border-bottom: 1px solid #777777;
	border-top: 1px solid #FFFFFF;
	display:block;
	margin:0;
}
ul.sidebar li a.last { border-bottom-width:0px;}
ul.sidebar li a h1,ul.sidebar li a h2,ul.sidebar li a h3,ul.sidebar li a h4,ul.sidebar li a h5,ul.sidebar li a h6,ul.sidebar li a p { margin-bottom:0!important;}



/* --------------------------------------------------- 
		RESPONSIVENESS
		Nuke it from orbit. It's the only way to be sure. 
------------------------------------------------------ */

/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
.container {
	display:block!important;
	max-width:600px!important;
	margin:0 auto!important; /* makes it centered */
	clear:both!important;
}

/* This should also be a block element, so that it will fill 100% of the .container */
.content {
	padding:15px;
	max-width:600px;
	margin:0 auto;
	display:block; 
}

/* Let's make sure tables in the content area are 100% wide */
.content table { width: 100%; }


/* Odds and ends */
.column {
	width: 300px;
	float:left;
}
.column tr td { padding: 15px; }
.column-wrap { 
	padding:0!important; 
	margin:0 auto; 
	max-width:600px!important;
}
.column table { width:100%;}
.social .column {
	width: 280px;
	min-width: 279px;
	float:left;
}

/* Be sure to place a .clear element after each set of columns, just to be safe */
.clear { display: block; clear: both; }


/* ------------------------------------------- 
		PHONE
		For clients that support media queries.
		Nothing fancy. 
-------------------------------------------- */
@media only screen and (max-width: 600px) {
	
	a[class='btn'] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}

	div[class='column'] { width: auto!important; float:none!important;}
	
	table.social div[class='column'] {
		width:auto!important;
	}

}";
        static private String HeaderSummary = @"<html >
                    <head>
                    <meta name='viewport' content='width=device-width'/>
                    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
                    <style>
                    " + CSS + @"
                    </style>
                    </head>
                    <body bgcolor='#f7f7f7'>
                     <font face='Calibri'>
                    <table class='head-wrap' bgcolor='#2b7ecb'>
                    <tr>
                    <td></td>
                    <td class='header container'>
                    <div class='content'>
                    <table bgcolor='#2b7ecb'>
                    <tr>
                    <td><h3 class='collapse'>SIMPLE TEMP UVAS</h6></td>
                    <td align='right' class='collapse' style='color:#ffffff;font-family:Calibri'></td>
                    </tr>
                    </table>
                    </div>
                    </td>
                    <td></td>
                    </tr>
                    </table> 
                    <div style='color:black;background-color:#d3d3d3;height:20px;width:20px;font-size:15px '> Add/Remove VAS Status for 21-Dec </div>
                     <table class='social' width='100%'>
                    <tr>
                    <td>";
 static private String FooterSummary = @"
                                             <span class='clear'></span>
                                            </td>
                                            </tr>
                                            </table> 
                                        <table class='footer-wrap'>
                                        <tr>
                                        <td></td>
                                        <td class='container'>
 
                                        <div class='content'>
                                        <table>
                                        <tr>
                                        <td align='center'>
                                        <p>
                                        <p>For general inquiries or comments, please contact
                                         <strong><a href='emailto:support@adms.ca><span class='__cf_email__' >support@adms.ca</span> </a></p>
                                        <p>For incident or outage reporting, please contact 
                                         <strong><a href='emailto:tickets@adms.ca><span class='__cf_email__' >tickets@adms.ca. </span><script data-cfhash='f9e31' type='text/javascript'> </p>
                                        <a href='#'>Terms</a> |
                                        <a href='#'>Privacy</a> |
                                        <a href='#'><unsubscribe>Unsubscribe</unsubscribe></a>
                                        </p>
                                        </td>
                                        </tr>
                                        </table>
                                        </div> 
                                        </td>
                                        <td></td>
                                        </tr>
                                        </table> 

                                        </font>
                                        </body>
                                        </html>";



        static private String Footer = @"</td>
</tr>
</table> 
<span class='clear'></span>
</td>
</tr>
</table> 
</td>
</tr>
</table>
</div> 
</td>
<td></td>
</tr>
</table> 
 
<table class='footer-wrap'>
<tr>
<td></td>
<td class='container'>
 
<div class='content'>
<table>
<tr>
<td align='center'>
<p>
<p>For general inquiries or comments, please contact
 <strong><a href='emailto:support@adms.ca><span class='__cf_email__' >support@adms.ca</span> </a></p>
<p>For incident or outage reporting, please contact 
 <strong><a href='emailto:tickets@adms.ca><span class='__cf_email__' >tickets@adms.ca. </span><script data-cfhash='f9e31' type='text/javascript'> </p>
<a href='#'>Terms</a> |
<a href='#'>Privacy</a> |
<a href='#'><unsubscribe>Unsubscribe</unsubscribe></a>
</p>
</td>
</tr>
</table>
</div> 
</td>
<td></td>
</tr>
</table> 

</font>
</body>
</html>
";

        static private String Header = @"<html>
<head>
 
<meta name='viewport' content='width=device-width'/>
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
<title>ZURBemails</title>
<link rel='stylesheet' type='text/css' href='stylesheets/email.css'/>
<style>
/* ------------------------------------- 
		GLOBAL 
------------------------------------- */
* { 
	margin:0;
	padding:0;
}
* { font-family: Calibri; }

img { 
	max-width: 100%; 
}
.collapse {
	margin:0;
	padding:0;
}
body {
	-webkit-font-smoothing:antialiased; 
	-webkit-text-size-adjust:none; 
	width: 100%!important; 
	height: 100%;
}


/* ------------------------------------- 
		ELEMENTS 
------------------------------------- */
a { color: #2b7ecb;}

p.callout {
	padding:15px;
	background-color:#a0dcff;
	margin-bottom: 15px;
}
.callout a {
	font-weight:bold;
	color: #2BA6CB;
}

table.social {
/* 	padding:15px; */
	background-color: #FFFFFF;
	
}
.social .soc-btn {
	padding: 3px 7px;
	font-size:12px;
	margin-bottom:10px;
	text-decoration:none;
	color: #FFF;font-weight:bold;
	display:block;
	text-align:center;
}
a.fb { background-color: #3B5998!important; }
a.tw { background-color: #1daced!important; }
a.gp { background-color: #DB4A39!important; }
a.ms { background-color: #000!important; }

.sidebar .soc-btn { 
	display:block;
	width:100%;
}

/* ------------------------------------- 
		HEADER 
------------------------------------- */
table.head-wrap { width: 100%;}

.header.container table td.logo { padding: 15px; }
.header.container table td.label { padding: 15px; padding-left:0px;}

/* ------------------------------------- 
		BODY 
------------------------------------- */
table.body-wrap { width: 100%;}

/* ------------------------------------- 
		FOOTER 
------------------------------------- */
table.footer-wrap { width: 100%;	clear:both!important;
}
.footer-wrap .container td.content  p { border-top: 1px solid rgb(215,215,215); padding-top:15px;}
.footer-wrap .container td.content p {
	font-size:10px;
	font-weight: bold;
	
}


/* ------------------------------------- 
		TYPOGRAPHY 
------------------------------------- */
h1,h2,h3,h4,h5,h6 {
font-family: Calibri; line-height: 1.1; margin-bottom:15px; color:#000;
}
h1 small, h2 small, h3 small, h4 small, h5 small, h6 small { font-size: 60%; color: #6f6f6f; line-height: 0; text-transform: none; }

h1 { font-weight:200; font-size: 44px;}
h2 { font-weight:200; font-size: 37px;}
h3 { font-weight:500; font-size: 27px; color:#FFFFFF;}
h4 { font-weight:500; font-size: 23px;}
h5 { font-weight:900; font-size: 17px;}
h6 { font-weight:900; font-size: 14px; text-transform: uppercase; color:#444;}

.collapse { margin:0!important;}

p, ul { 
	margin-bottom: 10px; 
	font-weight: normal; 
	font-size:14px; 
	line-height:1.6;
}
p.lead { font-size:17px; }
p.last { margin-bottom:0px;}

ul li {
	margin-left:5px;
	list-style-position: inside;
}


/* --------------------------------------------------- 
		RESPONSIVENESS
		Nuke it from orbit. It's the only way to be sure. 
------------------------------------------------------ */

/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
.container {
	display:block!important;
	max-width:600px!important;
	margin:0 auto!important; /* makes it centered */
	clear:both!important;
}

/* This should also be a block element, so that it will fill 100% of the .container */
.content {
	padding:15px;
	max-width:600px;
	margin:0 auto;
	display:block; 
}

/* Let's make sure tables in the content area are 100% wide */
.content table { width: 100%; }


/* Odds and ends */
.column {
	width: 300px;
	float:left;
}
.column tr td { padding: 15px; }
.column-wrap { 
	padding:0!important; 
	margin:0 auto; 
	max-width:600px!important;
}
.column table { width:100%;}
.social .column {
	width: 280px;
	min-width: 279px;
	float:left;
}

/* Be sure to place a .clear element after each set of columns, just to be safe */
.clear { display: block; clear: both; }


/* ------------------------------------------- 
		PHONE
		For clients that support media queries.
		Nothing fancy. 
-------------------------------------------- */
@media only screen and (max-width: 600px) {
	
	a[class='btn'] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}

	div[class='column'] { width: auto!important; float:none!important;}
	
	table.social div[class='column'] {
		width:auto!important;
	}

	
	
}


div{
    height: 50px;
    width: 50px;
    background-color: #ff4856;
    border-radius: 5px;
    position: relative;
   position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    content: '\274c';
    font-size: 30px; 
    color: #FFF;
    line-height: 50px;
    text-align: center;
}

div:after {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    content: '\274c';
    font-size: 30px; 
    color: #FFF;
    line-height: 50px;
    text-align: center;
}
div {
color: White;
 font-size: 30px; 
}
table{
    mso-table-lspace:0pt;
    mso-table-rspace:0pt;
}

body{
    -webkit-text-size-adjust:100%;
}

    img{
        -ms-interpolation-mode:bicubic;
    }
	
	#outlook a{
    padding:0;
}

h2{
    color:#0066CC !important;
}

.ExternalClass{
    width:100%;
}

.ExternalClass,
.ExternalClass p,
.ExternalClass span,
.ExternalClass font,
.ExternalClass td,
.ExternalClass div{
    line-height: 100%;
}
</style>

</head>
<body bgcolor='#f7f7f7'>
  <font face='Calibri'>
<table class='head-wrap' bgcolor='#2b7ecb'>
<tr>
<td></td>
<td class='header container'>
<div class='content'>
<table bgcolor='#2b7ecb'>
<tr>
<td><h3 class='collapse'>SIMPLE TEMP UVAS</h6></td>
<td align='right' class='collapse' style='color:#ffffff;font-family:Calibri(Body)'></td>
</tr>
</table>
</div>
</td>
<td></td>
</tr>
</table> 

<table class='body-wrap'>
<tr>
<td></td>
<td class='container' bgcolor='#f7f7f7'>
<div class='content'>
<table>
<tr>
<td>

 
<table class='social' width='100%'>
<tr>
<td>
 
<table>
<tr>
<td></td>
<td>
";

    }
}
