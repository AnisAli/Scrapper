﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleVAS_WebScraper.GeneralFuncations;
using System.IO;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using System.Net;
using System.Collections;

namespace SimpleVAS_WebScraper
{
    public class SimpleWebScraper
    {
        static private string SIMPLE_URL = "https://simple.int.bell.ca/pegasus";
        static private string SIMPLE_USER = "adad6697";
        static private string SIMPLE_PASS = "Qwerty6!";
        string LoginSessionID = "";


        public static int ADD_VAS_COUNT = 0;
        public static int REMOVE_VAS_COUNT = 0;
        public static int ADD_KICKOUT_COUNT = 0;
        public static int ADD_ERROR_COUNT = 0;
        public static int ADD_PENDING_COUNT = 0;
        public static List<String> Additonal_Services_IDList = null;
        
        public static int REMOVE_ERROR_COUNT = 0;
        public static int REMOVE_PENDING_COUNT = 0;


        public static void ScraperSettings(Dictionary<String, String> inConfigTable)
        {
            if (inConfigTable == null) return;
            try
            {
                SIMPLE_URL = inConfigTable[Constants.CONFIG_KEYS.SIMPLE_URL];
                SIMPLE_USER = inConfigTable[Constants.CONFIG_KEYS.SIMPLE_USERID];
                SIMPLE_PASS = inConfigTable[Constants.CONFIG_KEYS.SIMPLE_PASS];
                String ServicesID_String = inConfigTable[Constants.CONFIG_KEYS.ADDITIONAL_VAS_SERVICES];
                Additonal_Services_IDList = new List<string>();
                String[] ServiceID = ServicesID_String.Split(',');
                foreach (String ID in ServiceID)
                {
                    Additonal_Services_IDList.Add(ID.ToUpper().Trim());
                }
                
            }
            catch (Exception ex)
            {
                throw new Exception("ScraperSettings " + ex.Message);
            }
        }

        public void Add_TempVAS(ArrayList insUserList,String inEDW_HOST,String inEDW_USER,String inEDW_PASS){

           bool checkLoginSuccess = false;
            for (int i = 0; i < 8; i++) {
                if (!(GetLoginSessionID()))
                    checkLoginSuccess = false;
                else
                    checkLoginSuccess = true;

                if (checkLoginSuccess == true) 
                    break;
            }

            if (checkLoginSuccess == false)
                throw new Exception("Login Fail!");




            SimpleVAS_DBTable TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY = new SimpleVAS_DBTable(inEDW_HOST, inEDW_USER, inEDW_PASS);
            foreach (String UserID in insUserList)
            {
               // String aUserID = "b1sgzi34";
                Console.WriteLine(UserID);
                String HTMLEditPage = GetEdit_UserVASPage(UserID);
                 HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                           
                /// Check Edit Button Enabled or Disabled
                   doc.LoadHtml(HTMLEditPage);
         
            
                /// check edit page return successfully or not


                  var itemTitleCheckEnabled = doc.DocumentNode.SelectNodes("//span[@class='secondaryTitle']");
                  if (itemTitleCheckEnabled == null)  // if null mean wrong page
                  {  
                      TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.AddVAS_UpdateStatus(UserID, Constants.ADD_VAS.Status.__ERROR);
                      Logging.Add(UserID, Constants.LOG.ERROR, "[code 0001] " + Constants.LOG.ErrorDescription.UNEXPECTED_PAGE_RETURN  );
                      Logging.LogHtmlFile(UserID, HTMLEditPage);
                      ADD_ERROR_COUNT++;
                      continue;
                  }
              
            
                   var itemEditButtonEnabled = doc.DocumentNode.SelectNodes("//a[@id='editVASLink']");
                   if (itemEditButtonEnabled == null)
                   {  //if Edit Disabled

                       var itemTableARPUDetails = doc.DocumentNode.SelectNodes("//table[@class='listing']");
                       if (itemTableARPUDetails == null)   // incase of Additional Services empty and edit disbaled
                       {
                           TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.AddVAS_UpdateStatus(UserID, Constants.ADD_VAS.Status.__PENDING);
                           Logging.Add(UserID, Constants.LOG.UNKNOWN,"[code 0002] "  +  Constants.LOG.ErrorDescription.EDIT_UNKNOWNN);
                           Logging.LogHtmlFile(UserID, HTMLEditPage);
                           ADD_PENDING_COUNT++;
                              
            
                           continue;
                       }
                       else   // edit disabled and Additional Services exist check unlimited exist or not
                       {
                           HtmlNode node = doc.DocumentNode.SelectNodes("//table[@class='listing']")[0];
                           String s = node.InnerText;
                           if (s.ToUpper().Contains("UNLIMITED USAGE PLAN") || s.ToUpper().Contains("25 GB EXTRA USAGE PLAN"))
                           {
                               //update table left
                               TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.AddVAS_UpdateStatus(UserID, Constants.ADD_VAS.Status.__KICKOUT);
                               Logging.Add(UserID, Constants.LOG.UNKNOWN, "[code 0003] " + Constants.LOG.ErrorDescription.EDIT_UNLIMITED_DISABLED);
                               Logging.LogHtmlFile(UserID, HTMLEditPage);
                               ADD_KICKOUT_COUNT++;
                               continue;
                           }
                           else
                           {                       
                               TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.AddVAS_UpdateStatus(UserID, Constants.ADD_VAS.Status.__PENDING);
                               Logging.Add(UserID, Constants.LOG.UNKNOWN, "[code 0004] " +  Constants.LOG.ErrorDescription.EDIT_NO_VAS_DISABLED);
                               Logging.LogHtmlFile(UserID, HTMLEditPage);
                               ADD_PENDING_COUNT++;
                               continue;
                           }
                       }
                   } //if in case of edit button disabled
                   else  // If Edit Button Enabled
                   {
                       
                       // Edit Enable
                       //Verify if VAS is already Unlimited then kickout and log 
                       var itemTableARPUDetails = doc.DocumentNode.SelectNodes("//table[@class='listing']");
                       if (itemTableARPUDetails == null)   // incase of Additional Services empty and edit enabled
                       {

                           if (ApplyTempVas_UsingEditPage(UserID, inEDW_HOST, inEDW_USER, inEDW_PASS))
                               ADD_VAS_COUNT++;
                           else
                               ADD_KICKOUT_COUNT++;
                      
                           /// ****Code to Add VAS ***********
                          // TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.AddVAS_UpdateStatus(UserID, Constants.ADD_VAS.Status.__KICKOUT);
                         //  Logging.Add(UserID, Constants.LOG.UNKNOWN, Constants.LOG.ErrorDescription.EDIT_UNKNOWNN);
                          // Logging.LogHtmlFile(UserID, HTMLEditPage);
                       
                           continue;
                       }
                       else   // edit Enabled and Additional Services exist check unlimited exist or not
                       {
                        
                           HtmlNode node = doc.DocumentNode.SelectNodes("//table[@class='listing']")[0];
                           String s = node.InnerText;
                          
                           if (s.ToUpper().Contains("UNLIMITED USAGE PLAN") || s.ToUpper().Contains("25 GB EXTRA USAGE PLAN"))
                           {
                               //update table left
                               TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.AddVAS_UpdateStatus(UserID, Constants.ADD_VAS.Status.__EXISTING_UVAS);
                               Logging.Add(UserID, Constants.LOG.UNKNOWN, "[code 0005] " + Constants.LOG.ErrorDescription.EDIT_UNLIMITED_DISABLED);
                               Logging.LogHtmlFile(UserID, HTMLEditPage);
                               
                               continue;
                           }
                           else
                           {

                       
                               if(ApplyTempVas_UsingEditPage(UserID,inEDW_HOST,inEDW_USER,inEDW_PASS))
                                   ADD_VAS_COUNT++;
                               else
                                   ADD_KICKOUT_COUNT++;
                            
                               //*********** ADD VAS CODE
                           //    TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.AddVAS_UpdateStatus(UserID, Constants.ADD_VAS.Status.__PENDING);
                           //    Logging.Add(UserID, Constants.LOG.UNKNOWN, Constants.LOG.ErrorDescription.EDIT_NO_VAS_DISABLED);
                           //    Logging.LogHtmlFile(UserID, HTMLEditPage);
                         //      continue;
                           }
                       } 
                  
                   } //else, in case of edit button enabled

            } //For Each for UserID from UserID List

        } //Add_TempVAS


        public void UpdatePackage(ArrayList insUserList, String folderName,String tick)
        {

            bool checkLoginSuccess = false;
            for (int i = 0; i < 8; i++)
            {
                if (!(GetLoginSessionID()))
                    checkLoginSuccess = false;
                else
                    checkLoginSuccess = true;

                if (checkLoginSuccess == true)
                    break;
            }

            if (checkLoginSuccess == false)
                throw new Exception("Login Fail!");




            SimpleVAS_DBTable TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY = new SimpleVAS_DBTable(folderName, tick);
            foreach (String UserID in insUserList)
            {
                // String aUserID = "b1sgzi34";
                Console.WriteLine(UserID);
                String HTMLEditPage = GetEdit_UserVASPage(UserID);
                HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();

               
                if (HTMLEditPage == "NOT (PBW27 or PBW28)")
                {

                    TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.AddVAS_UpdateStatus(UserID, "NOT (PBW27 or PBW28)");
                    continue;
                }
                else if (HTMLEditPage == "SP NOT FOUND")
                {
                    TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.AddVAS_UpdateStatus(UserID, "NOT (PBW27 or PBW28)");
                    continue;
                }
                else if (HTMLEditPage == "already PBW28")
                {
                    TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.AddVAS_UpdateStatus(UserID, "ALREADY");
                    continue;
                }
               
                    /// Check Edit Button Enabled or Disabled
                    doc.LoadHtml(HTMLEditPage);


                /// check edit page return successfully or not


                var itemTitleCheckEnabled = doc.DocumentNode.SelectNodes("//span[@class='secondaryTitle']");
                if (itemTitleCheckEnabled == null)  // if null mean wrong page
                {
                    TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.AddVAS_UpdateStatus(UserID, Constants.ADD_VAS.Status.__ERROR);
                    Logging.Add(UserID, Constants.LOG.ERROR, "[code 0001] " + Constants.LOG.ErrorDescription.UNEXPECTED_PAGE_RETURN);
                    Logging.LogHtmlFile(UserID, HTMLEditPage);
                    ADD_ERROR_COUNT++;
                    continue;
                }


                var itemEditButtonEnabled = doc.DocumentNode.SelectNodes("//a[@id='editVASLink']");
                if (itemEditButtonEnabled == null)
                {  //if Edit Disabled

                    var itemTableARPUDetails = doc.DocumentNode.SelectNodes("//table[@class='listing']");
                    if (itemTableARPUDetails == null)   // incase of Additional Services empty and edit disbaled
                    {
                        TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.AddVAS_UpdateStatus(UserID, Constants.ADD_VAS.Status.__PENDING);
                        Logging.Add(UserID, Constants.LOG.UNKNOWN, "[code 0002] " + Constants.LOG.ErrorDescription.EDIT_UNKNOWNN);
                        Logging.LogHtmlFile(UserID, HTMLEditPage);
                        ADD_PENDING_COUNT++;
                        continue;
                    }
                    else   // edit disabled and Additional Services exist check unlimited exist or not
                    {
                        HtmlNode node = doc.DocumentNode.SelectNodes("//table[@class='listing']")[0];
                        String s = node.InnerText;
                        if (s.ToUpper().Contains("UNLIMITED USAGE PLAN"))
                        {
                            //update table left
                            TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.AddVAS_UpdateStatus(UserID, Constants.ADD_VAS.Status.__KICKOUT);
                            Logging.Add(UserID, Constants.LOG.UNKNOWN, "[code 0003] " + Constants.LOG.ErrorDescription.EDIT_UNLIMITED_DISABLED);
                            Logging.LogHtmlFile(UserID, HTMLEditPage);
                            ADD_KICKOUT_COUNT++;
                            continue;
                        }
                        else
                        {
                            TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.AddVAS_UpdateStatus(UserID, Constants.ADD_VAS.Status.__PENDING);
                            Logging.Add(UserID, Constants.LOG.UNKNOWN, "[code 0004] " + Constants.LOG.ErrorDescription.EDIT_NO_VAS_DISABLED);
                            Logging.LogHtmlFile(UserID, HTMLEditPage);
                            ADD_PENDING_COUNT++;
                            continue;
                        }
                    }
                } //if in case of edit button disabled
                else  // If Edit Button Enabled
                {

                    // Edit Enable
                    //Verify if VAS is already Unlimited then kickout and log 
                    var itemTableARPUDetails = doc.DocumentNode.SelectNodes("//table[@class='listing']");
                    if (itemTableARPUDetails == null)   // incase of Additional Services empty and edit enabled
                    {
                        Logging.Add(UserID, Constants.LOG.UNKNOWN, "[code 1006] " + Constants.LOG.ErrorDescription.NEW____UNLIMITED_NOT_FOUND);
                        Logging.LogHtmlFile(UserID, HTMLEditPage);
                        continue;
                    }
                    else   // edit Enabled and Additional Services exist check unlimited exist or not
                    {
                            if (ApplyTempVas_UsingEditPage(UserID, folderName,tick))
                                ADD_VAS_COUNT++;
                            else
                                ADD_KICKOUT_COUNT++;
                    }

                } //else, in case of edit button enabled

            } //For Each for UserID from UserID List

        } //Add_TempVAS
        
        
        public void Remove_TempVAS(ArrayList insUserList, String inEDW_HOST, String inEDW_USER, String inEDW_PASS)
        {
            
           bool checkLoginSuccess = false;
            for (int i = 0; i < 8; i++) {
                if (!(GetLoginSessionID()))
                    checkLoginSuccess = false;
                else
                    checkLoginSuccess = true;

                if (checkLoginSuccess == true) 
                    break;
            }

            if (checkLoginSuccess == false)
                throw new Exception("Login Fail!");

            SimpleVAS_DBTable TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY = new SimpleVAS_DBTable(inEDW_HOST, inEDW_USER, inEDW_PASS);
            foreach (String UserID in insUserList)
            {
                // String aUserID = "b1sgzi34";
                Console.WriteLine(UserID);
                String HTMLEditPage = GetEdit_UserVASPage(UserID);
                HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();

                /// Check Edit Button Enabled or Disabled Plus also verify expected page return
                doc.LoadHtml(HTMLEditPage);

                /// check edit page return successfully or not
                var itemTitleCheckEnabled = doc.DocumentNode.SelectNodes("//span[@class='secondaryTitle']");
                if (itemTitleCheckEnabled == null)  // if null mean wrong page
                {  //if Edit Disabled
                    Logging.Add(UserID, Constants.LOG.ERROR, "[code 0001] " + Constants.LOG.ErrorDescription.UNEXPECTED_PAGE_RETURN, Constants.REMOVE_VAS.VAS_TYPE);
                    Logging.LogHtmlFile(UserID, HTMLEditPage);
                    REMOVE_ERROR_COUNT++;
                    continue;
                }


                var itemEditButtonEnabled = doc.DocumentNode.SelectNodes("//a[@id='editVASLink']");
                if (itemEditButtonEnabled == null)
                {  //if Edit Disabled
                    var itemTableARPUDetails = doc.DocumentNode.SelectNodes("//table[@class='listing']");
                    if (itemTableARPUDetails == null)   // incase of Additional Services empty and edit disbaled -- do nothing incase of remove vas
                    {
                        TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.RemoveVAS_UpdateStatus(UserID, Constants.REMOVE_VAS.Status.__NO_VAS_EXIST);  
                        Logging.Add(UserID, Constants.LOG.UNKNOWN, "[code 0002] " + Constants.LOG.RemoveVAS_ErrorDescription.EDIT_DISABLE_NO_TEMP_VAS,Constants.REMOVE_VAS.VAS_TYPE);
                        Logging.LogHtmlFile(UserID, HTMLEditPage);
                        continue;
                    }
                    else   // edit disabled and Additional Services exist check unlimited TEMP VAS exist or not
                    {
                        HtmlNode node = doc.DocumentNode.SelectNodes("//table[@class='listing']")[0];
                        String s = node.InnerText;
                        if (s.ToUpper().Contains("UNLIMITED USAGE PLAN") && s.ToUpper().Contains("TEMP $0 WITH PLAN CHANGE"))  //temp vas exist
                        {
                            //update table by status pending remove, automation should remove it later on
                            TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.RemoveVAS_UpdateStatus(UserID, Constants.REMOVE_VAS.Status.__PENDING_REMOVE);
                            Logging.Add(UserID, Constants.LOG.UNKNOWN, "[code 0003] " + Constants.LOG.RemoveVAS_ErrorDescription.EDIT_DISABLE_TEMP_VAS_EXIST,Constants.REMOVE_VAS.VAS_TYPE);
                            Logging.LogHtmlFile(UserID, HTMLEditPage);
                            REMOVE_PENDING_COUNT++;
                            continue;
                        }
                        else
                        { //temp vas not exist

                            TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.RemoveVAS_UpdateStatus(UserID, Constants.REMOVE_VAS.Status.__NO_VAS_EXIST);
                            Logging.Add(UserID, Constants.LOG.UNKNOWN, "[code 0004] " + Constants.LOG.RemoveVAS_ErrorDescription.EDIT_DISABLE_NO_TEMP_VAS, Constants.REMOVE_VAS.VAS_TYPE);
                            Logging.LogHtmlFile(UserID, HTMLEditPage);
                            continue;
                        }
                    }
                } //if in case of edit button disabled
                else  // If Edit Button Enabled [removed vas]
                {
                   
                    // Edit Enable
                    //Verify if VAS is already Unlimited then kickout and log 
                    var itemTableARPUDetails = doc.DocumentNode.SelectNodes("//table[@class='listing']");
                    if (itemTableARPUDetails == null)   // incase of Additional Services empty and edit enabled means no Temp vas exist so no need to remove
                    {
                        TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.RemoveVAS_UpdateStatus(UserID, Constants.REMOVE_VAS.Status.__NO_VAS_EXIST);
                        Logging.Add(UserID, Constants.LOG.UNKNOWN, "[code 0004] " + Constants.LOG.RemoveVAS_ErrorDescription.EDIT_ENABLE_NO_TEMP_VAS, Constants.REMOVE_VAS.VAS_TYPE);
                        Logging.LogHtmlFile(UserID, HTMLEditPage);
                        continue;       
                    }
                    else   // edit Enabled and Additional Services exist check unlimited temp UVAS exist or not
                    {

                        HtmlNode node = doc.DocumentNode.SelectNodes("//table[@class='listing']")[0];
                        String s = node.InnerText;

                        if (s.ToUpper().Contains("UNLIMITED USAGE PLAN") && s.ToUpper().Contains("TEMP $0 WITH PLAN CHANGE")) //temp uvas exist
                        {
                            //temp uvas exist
                            // ************* CODE TO REMOVE VAS *******************
                            if (RemoveTempVas_UsingEditPage(UserID, inEDW_HOST, inEDW_USER, inEDW_PASS))
                                REMOVE_VAS_COUNT++;
                            else
                                REMOVE_ERROR_COUNT++;

                            //TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.AddVAS_UpdateStatus(UserID, Constants.ADD_VAS.Status.__EXISTING_UVAS);
                            //Logging.Add(UserID, Constants.LOG.UNKNOWN, "[code 0005] " + Constants.LOG.ErrorDescription.EDIT_UNLIMITED_DISABLED);
                            //Logging.LogHtmlFile(UserID, HTMLEditPage);

                            continue;
                        }
                        else // if temp UVAS not exist and button enabled
                        {
                            TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.RemoveVAS_UpdateStatus(UserID, Constants.REMOVE_VAS.Status.__NO_VAS_EXIST);
                            Logging.Add(UserID, Constants.LOG.UNKNOWN, "[code 00048] " + Constants.LOG.RemoveVAS_ErrorDescription.EDIT_ENABLE_NO_TEMP_VAS, Constants.REMOVE_VAS.VAS_TYPE);
                            Logging.LogHtmlFile(UserID, HTMLEditPage);
                            continue;  
                        }
                    }

                } //else, in case of edit button enabled
            } //userid for each
              
        }

        #region Authentication

        private bool GetLoginSessionID()
        {
            try
            {
                String firstSession = Login_POST_REQUEST();
                System.Threading.Thread.Sleep(4000);
                String secondSession = Temp_GetRequestForLogin(firstSession);
                System.Threading.Thread.Sleep(4000);
                if (secondSession.Trim() == "")
                    secondSession = Temp_GetRequestForLogin(firstSession);
                LoginSessionID = secondSession;
                System.Threading.Thread.Sleep(3000);
                Login_POST_REQUEST(false, LoginSessionID);
                Console.WriteLine(LoginSessionID);

                if (LoginSessionID.Trim() == "") return false;
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        private String Login_POST_REQUEST(Boolean IsFirst = true, String inSession = "")
        {
            String FuncName = "SimpleWebScraper.Login_POST_REQUEST";
           
            // LOGIN First Request
            String resultFromServer = "";
            String strPost = "formids=inputUserName%2CinputPassword%2CinputLanguage%2ChasMessages_0%2CdisplayRedirectMsg_0&seedids=ZH4sIAAAAAAAAAFvzloG1vJ6hVqc4tagsMzlVxUCnIDEdRCXn5xbk56XmlYDZeSWJmXmpRUB2cWpxcWZ%2BHohVACR88tMz83wSK%2FNLQQqLM1JzcsCKispSi%2FwSc1N9U0sSQxLTgWJO%2BSmVQCooNS8ltQjKScsvylUxAAD7bj%2FThQAAAA%3D%3D&component=form&page=Login&service=direct&session=T&submitmode=&submitname=&hasMessages_0=F&displayRedirectMsg_0=F&inputUserName="+ SIMPLE_USER +"&inputPassword="+SIMPLE_PASS+"&inputLanguage=0&submit_button=Login";
            StreamWriter myWriter = null;
            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(SIMPLE_URL);
            objRequest.Method = "POST";
            objRequest.ContentLength = strPost.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";
            objRequest.Host = "simple.int.bell.ca";

            if (IsFirst == false)
            {
                CookieContainer cookies = new CookieContainer();
                cookies.Add(new Cookie("JSESSIONID", inSession.Trim()) { Domain = "simple.int.bell.ca" });
                objRequest.CookieContainer = cookies;
            }

            objRequest.AllowAutoRedirect = true;
            try
            {
                myWriter = new StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(strPost);
            }
            catch (Exception ex)
            {
                Logging.Log_Exception(FuncName, ex.Message);
                throw new Exception(ex.Message);
            }
            finally
            {
                myWriter.Close();
            }

            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
            {
                resultFromServer = sr.ReadToEnd();
                sr.Close();
            }

            if (IsFirst == true)
            {
                // Capture SessionID 
                HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                doc.LoadHtml(resultFromServer);
                var itemCheckSessionExist = doc.DocumentNode.SelectNodes("//a[@id='restart']");

                if (itemCheckSessionExist != null)
                {
                    HtmlNode node = doc.DocumentNode.SelectNodes("//a[@id='restart']")[0];
                    HtmlAttribute att = node.Attributes["href"];
                    int End = att.Value.IndexOf("service=restart");
                    int start = 19;
                    String SessionId = att.Value.Substring(start, End - start - 1);
                    //txtSessionID.Text = SessionId;
                    return SessionId;
                }
            }
            return "";
        }

        private String Temp_GetRequestForLogin(String inSessionID)
        {
            // Send Another Get Request for Login
            String result = "";
            String NewSessionID;
            CookieContainer cookies = new CookieContainer();
            HttpWebRequest objRequest1 = (HttpWebRequest)WebRequest.Create(SIMPLE_URL);
            cookies.Add(new Cookie("JSESSIONID", inSessionID.Trim()) { Domain = "simple.int.bell.ca" });
            objRequest1.CookieContainer = cookies;
            objRequest1.Method = "GET";
            objRequest1.Host = "simple.int.bell.ca";
            HttpWebResponse objResponse1 = (HttpWebResponse)objRequest1.GetResponse();
            CookieCollection cook = objResponse1.Cookies;

            using (StreamReader sr =
               new StreamReader(objResponse1.GetResponseStream()))
            {
                result = sr.ReadToEnd();
                sr.Close();
            }

            if (cook.Count != 0)
            {
                NewSessionID = cook[0].Value;
                return NewSessionID;
            }
            else
                return "";
        }


        #endregion

        #region Edit User VAS


        private String GeneratePOSTMessage_ADD_VAS_TEST(String insHTMLPage)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(insHTMLPage);
            //formid
            var formidCheck = doc.DocumentNode.SelectNodes("//input[@name='formids']");
            if (formidCheck == null) return "";
            HtmlNode node = null;
            if (formidCheck.Count > 1)
                node = doc.DocumentNode.SelectNodes("//input[@name='formids']")[1];
            else
                return "";

            String tempFormID = node.Attributes["value"].Value;
            tempFormID = "formids=" + tempFormID.Replace(",", "%2C");

            //seedid
            var seedsidCheck = doc.DocumentNode.SelectNodes("//input[@name='seedids']");
            if (seedsidCheck == null) return "";
            node = null;
            if (formidCheck.Count > 1)
                node = doc.DocumentNode.SelectNodes("//input[@name='seedids']")[1];
            else
                return "";


            String tempSeedID = node.Attributes["value"].Value;
            tempSeedID = tempSeedID.Replace("/", "%2F");
            tempSeedID = tempSeedID.Replace("=", "%3D");
            tempSeedID = tempSeedID.Replace("+", "%2B");
            tempSeedID = tempSeedID.Replace("!", "%21");
            tempSeedID = tempSeedID.Replace("#", "%23");
            tempSeedID = tempSeedID.Replace("$", "%24");
            tempSeedID = tempSeedID.Replace("*", "%2A");
            tempSeedID = tempSeedID.Replace(",", "%2C");
            tempSeedID = tempSeedID.Replace("-", "%2D");
            tempSeedID = tempSeedID.Replace(".", "%2E");
            tempSeedID = tempSeedID.Replace(":", "%3A");
            tempSeedID = tempSeedID.Replace(";", "%3B");
            tempSeedID = tempSeedID.Replace("<", "%3C");
            tempSeedID = tempSeedID.Replace(">", "%3E");
            tempSeedID = tempSeedID.Replace("?", "%3F");
            tempSeedID = tempSeedID.Replace("@", "%40");
            tempSeedID = tempSeedID.Replace("^", "%5E");
            tempSeedID = tempSeedID.Replace("_", "%5F");
            tempSeedID = "&seedids=" + tempSeedID;

            node = doc.DocumentNode.SelectNodes("//div[@id='vasSubmitFormhidden']")[0];
            
            doc.LoadHtml(node.InnerHtml);

            node = doc.DocumentNode;

            
            // String checkedIDs = GenerateExistingService_POST_String(insHTMLPage);
          //  tempFormID = "formids=editModeIf_0%2CsubmitVASLink%2CeditModeIf1_0%2CVASLoop2_0%2CVASLoop1_0%2CcurrentVasCheckedIf_0%2CcurrentVasCheckedIf_2%2CcurrentVasCheckedIf_4%2CcurrentVasCheckedIf_6%2CcurrentVasCheckedIf_8%2CcurrentVasCheckedIf_10%2CcurrentVasCheckedIf_12%2CVASLoop3_0%2CVASLoop_0%2CshowHeaderIf_0%2CterminateHeaderIf_0%2CopenRequiredChoice1If_0%2CcloseRequiredChoice1If_0%2CBWUNL%2CemailServiceNameNotUnlimited_12%2CdisplayVasPromotionsIf_12%2Cpromotions_BWUNL%2CshowMessageIf_12%2CoutputServiceCharDiv_12";
            String finalstring = tempFormID;// +tempSeedID + T; // +checkedIDs;
            finalstring = "formids=editModeIf_0%2CsubmitVASLink%2CeditModeIf1_0%2CVASLoop2_0%2CVASLoop1_0%2CcurrentVasCheckedIf_0%2CcurrentVasCheckedIf_2%2CcurrentVasCheckedIf_4%2CcurrentVasCheckedIf_6%2CcurrentVasCheckedIf_8%2CcurrentVasCheckedIf_10%2CcurrentVasCheckedIf_12%2CVASLoop3_0%2CVASLoop_0%2CshowHeaderIf_0%2CterminateHeaderIf_0%2CopenRequiredChoice1If_0%2CcloseRequiredChoice1If_0%2CINETC%2CemailServiceNameNotUnlimited_0%2CdisplayVasPromotionsIf_0%2Cpromotions_INETC%2CshowMessageIf_0%2CoutputServiceCharDiv_0%2CshowHeaderIf_2%2CopenRequiredChoice1If_2%2CcloseRequiredChoice1If_2%2CMCFBS%2CemailServiceNameNotUnlimited_2%2CdisplayVasPromotionsIf_2%2CshowMessageIf_2%2CoutputServiceCharDiv_2%2CserviceCharLoop_0%2CshowHeaderIf_4%2CopenRequiredChoice1If_4%2CcloseRequiredChoice1If_4%2CMCFBT%2CemailServiceNameNotUnlimited_4%2CdisplayVasPromotionsIf_4%2CshowMessageIf_4%2CoutputServiceCharDiv_4%2CserviceCharLoop_2%2CshowHeaderIf_6%2CopenRequiredChoice1If_6%2CcloseRequiredChoice1If_6%2CMCFGD%2CemailServiceNameNotUnlimited_6%2CdisplayVasPromotionsIf_6%2CshowMessageIf_6%2CoutputServiceCharDiv_6%2CshowHeaderIf_8%2CopenRequiredChoice1If_8%2CcloseRequiredChoice1If_8%2CPWEB%2CemailServiceNameNotUnlimited_8%2CdisplayVasPromotionsIf_8%2CshowMessageIf_8%2CoutputServiceCharDiv_8%2CshowHeaderIf_10%2CterminateHeaderIf_2%2CopenRequiredChoice1If_10%2CcloseRequiredChoice1If_10%2CBWGBB%2CemailServiceNameNotUnlimited_10%2CdisplayVasPromotionsIf_10%2Cpromotions_BWGBB%2CshowMessageIf_10%2CoutputServiceCharDiv_10%2CserviceCharLoop_4%2CnotHiddenSC_0%2CshowCharCodeIf_0%2CifShowDescription_0%2CshowLabel_0%2CisConfiguredValue_0%2CisEditMode_0%2CisDistinct_0%2CisDistinctBool_0%2CBWGBB_BWQTY_1%2CisCSCAddress_0%2CisMainAccessNumber_0%2CisCusAccessNumber_0%2CisUserAccountNumber_0%2CisExtCarrier_0%2CisShowEffectiveDate_0%2CshowHeaderIf_12%2CopenRequiredChoice1If_12%2CcloseRequiredChoice1If_12%2CBWUNL%2CemailServiceNameNotUnlimited_12%2CdisplayVasPromotionsIf_12%2Cpromotions_BWUNL%2CshowMessageIf_12%2CoutputServiceCharDiv_12%2CserviceCharLoop_7&seedids=ZH4sIAAAAAAAAAG1VzW7jNhAO0BZ9kRx9qNMW6NWxHdRbxXYi7xY9jsWRRYQiBQ7prfaZ%2Bmp9hx2SMiUnvtjz%2B80PZ0b%2F%2FX%2F307e7ux%2Fvft7NCO1ZVnj%2Fy6yDU%2FirTNsZjdpFWjuQGi3ThETS6EB1%2FLPCGrxyBfTGB1NqUKngQlRI%2FXaAU2KeGKKU3%2FBK%2BOnFo%2B3XdY2VGzA5DbRbaPEZHSTDN8QOlDzjRjtWg5rKDrLFFPnRiJ7%2FFOiT5xJCoMCGeOZP02Jm9hZrukjMKXmDUuZr6XqFy4YRgk5gZREIQ%2BrMWiR0Ay31lUpGuDJX0YLmDBZVZbx2lAM%2FvxczpPG2wvcmOyvQBmGI4i4e2Wj5TprC7cHya03QJS1EK%2FWYFqcphcArSWzGKHhFzaFjKx9mA1aJYKsmPk4gnoxtw5hUUjh6ReLXXxoR0mHTZJh0B%2FOG%2BqO4MKetcTfs19Ya%2B8zjxcVQVLOqAbqI7n%2Bf4cQk%2B316%2Bbx%2B%2FefzazFKyt12yu%2BVP0mdJJWVPEUSSlRc9ViVRCUyNwwPNebrI1Rvj945bs98dmRm0K0V4f38YQbiDLpCUU4dU9il0eRbtJtVFi26TskKQrcn0nGIh3yB6C%2FsM1%2F25LA9WNAEVfa9PI9vW7B9HNi4i0ujFHSEK6DmaMCGsmqvVNgqJj3v2EYM1RXGdIeej8APLx1u6nkIaY3wlQvW87ghwaKL9AVlnmHmVzgBYkQIU3GNN4EL6V7yyw%2FMA1lvdK3kqXFxB3Kchzisy%2FVCizTncdPpglpLm2lsQYYTsZLUKejLdNgWQvC2UcyPnEV0W98e0ZbFbnltMhow3k31R%2BGiczzTW25PDPDR4InrtlOTzpADFVp0M0QlXX9Twd088%2FW57bUqC2M3%2B8MXWcezIMP6sSqVGq5cGpnhAQaudOB8DLoeb8qgy57c%2FEYqMRrwt%2BFRcjGa32fK1yOdgFd8q4bLzsmyzLpBJOtdh%2Fry0A6OFLP%2B98BUGCg%2BQRrOBZ5R%2FRoQ%2FDFr5iGjL5LkUeH9H4MqxItcBzqecqZl%2FAyV%2FthKlw3jlmYGjqiY4wXsvAunj2uY58i%2FhXqiap8Gmf5uwB3MyoRk0%2BQWkkILzkApUDqR3wHnnFhrXwcAAA%3D%3D&component=vasSubmitForm&page=accounts_ViewAdditionalServices&service=direct&session=T&submitmode=&submitname=submitVASLink&editModeIf_0=T&editModeIf1_0=T&VASLoop2_0=PSINETC&VASLoop2_0=PSMCFBS&VASLoop2_0=PSMCFBT&VASLoop2_0=PSMCFGD&VASLoop2_0=PSPWEB&VASLoop2_0=PSBWGBB&VASLoop2_0=PSBWUNL&VASLoop1_0=PSINETC&currentVasCheckedIf_0=F&VASLoop1_0=PSMCFBS&currentVasCheckedIf_2=F&VASLoop1_0=PSMCFBT&currentVasCheckedIf_4=F&VASLoop1_0=PSMCFGD&currentVasCheckedIf_6=F&VASLoop1_0=PSPWEB&currentVasCheckedIf_8=F&VASLoop1_0=PSBWGBB&currentVasCheckedIf_10=F&VASLoop1_0=PSBWUNL&currentVasCheckedIf_12=F&VASLoop3_0=PSINETC&VASLoop3_0=PSMCFBS&VASLoop3_0=PSMCFBT&VASLoop3_0=PSMCFGD&VASLoop3_0=PSPWEB&VASLoop3_0=PSBWGBB&VASLoop3_0=PSBWUNL&VASLoop_0=PSINETC&showHeaderIf_0=T&terminateHeaderIf_0=F&openRequiredChoice1If_0=F&closeRequiredChoice1If_0=F&emailServiceNameNotUnlimited_0=T&displayVasPromotionsIf_0=T&showMessageIf_0=F&outputServiceCharDiv_0=F&VASLoop_0=PSMCFBS&showHeaderIf_2=F&openRequiredChoice1If_2=F&closeRequiredChoice1If_2=F&emailServiceNameNotUnlimited_2=T&displayVasPromotionsIf_2=F&showMessageIf_2=F&outputServiceCharDiv_2=T&VASLoop_0=PSMCFBT&showHeaderIf_4=F&openRequiredChoice1If_4=F&closeRequiredChoice1If_4=F&emailServiceNameNotUnlimited_4=T&displayVasPromotionsIf_4=F&showMessageIf_4=F&outputServiceCharDiv_4=T&VASLoop_0=PSMCFGD&showHeaderIf_6=F&openRequiredChoice1If_6=F&closeRequiredChoice1If_6=F&emailServiceNameNotUnlimited_6=T&displayVasPromotionsIf_6=F&showMessageIf_6=F&outputServiceCharDiv_6=F&VASLoop_0=PSPWEB&showHeaderIf_8=F&openRequiredChoice1If_8=F&closeRequiredChoice1If_8=F&emailServiceNameNotUnlimited_8=T&displayVasPromotionsIf_8=F&showMessageIf_8=F&outputServiceCharDiv_8=F&VASLoop_0=PSBWGBB&showHeaderIf_10=T&terminateHeaderIf_2=T&openRequiredChoice1If_10=F&closeRequiredChoice1If_10=F&emailServiceNameNotUnlimited_10=T&displayVasPromotionsIf_10=T&showMessageIf_10=F&outputServiceCharDiv_10=T&serviceCharLoop_4=V0&notHiddenSC_0=T&showCharCodeIf_0=F&ifShowDescription_0=F&showLabel_0=F&isConfiguredValue_0=T&isEditMode_0=T&isDistinct_0=T&isDistinctBool_0=F&isCSCAddress_0=F&isMainAccessNumber_0=F&isCusAccessNumber_0=F&isUserAccountNumber_0=F&isExtCarrier_0=F&isShowEffectiveDate_0=F&VASLoop_0=PSBWUNL&showHeaderIf_12=F&openRequiredChoice1If_12=F&closeRequiredChoice1If_12=F&emailServiceNameNotUnlimited_12=F&displayVasPromotionsIf_12=T&showMessageIf_12=F&outputServiceCharDiv_12=T";
            String checkedIDs = GenerateExistingService_POST_String(insHTMLPage);

            return finalstring + checkedIDs;
        }

        private String GeneratePOSTMessage_REMOVE_VAS(String insHTMLPage)
        { 
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(insHTMLPage);
   //formid
            var formidCheck = doc.DocumentNode.SelectNodes("//input[@name='formids']");
            if (formidCheck == null) return "";
            HtmlNode node = null;
            if(formidCheck.Count>1 )
              node = doc.DocumentNode.SelectNodes("//input[@name='formids']")[1];
            else
                return "";

            String tempFormID = node.Attributes["value"].Value;
            tempFormID= "formids=" +  tempFormID.Replace(",", "%2C");

  //seedid
            var seedsidCheck = doc.DocumentNode.SelectNodes("//input[@name='seedids']");
            if (seedsidCheck == null) return "";
             node = null;
            if(formidCheck.Count>1 )
                node = doc.DocumentNode.SelectNodes("//input[@name='seedids']")[1];
            else
                return "";

            String tempSeedID = node.Attributes["value"].Value;
            tempSeedID =  tempSeedID.Replace("/", "%2F");
            tempSeedID = tempSeedID.Replace("=", "%3D");
            tempSeedID = tempSeedID.Replace("+", "%2B");
            tempSeedID = tempSeedID.Replace("!", "%21");
            tempSeedID = tempSeedID.Replace("#", "%23");
            tempSeedID = tempSeedID.Replace("$", "%24");
            tempSeedID = tempSeedID.Replace("*", "%2A");
            tempSeedID = tempSeedID.Replace(",", "%2C");
            tempSeedID = tempSeedID.Replace("-", "%2D");
            tempSeedID = tempSeedID.Replace(".", "%2E");
            tempSeedID = tempSeedID.Replace(":", "%3A");
            tempSeedID = tempSeedID.Replace(";", "%3B");
            tempSeedID = tempSeedID.Replace("<", "%3C");
            tempSeedID = tempSeedID.Replace(">", "%3E");
            tempSeedID = tempSeedID.Replace("?", "%3F");
            tempSeedID = tempSeedID.Replace("@", "%40");
            tempSeedID = tempSeedID.Replace("^", "%5E");
            tempSeedID = tempSeedID.Replace("_", "%5F");
            tempSeedID = "&seedids=" + tempSeedID;

            node = doc.DocumentNode.SelectNodes("//div[@id='vasSubmitFormhidden']")[0];

            doc.LoadHtml(node.InnerHtml);

            node = doc.DocumentNode;


            var CheckedBoxes = node.SelectNodes("//input[@type='hidden']");
            if (CheckedBoxes == null) return "";

            String CheckedID;
            String T = "";
            String elename, elevalue= "";
           
            for (int i = 2; i < CheckedBoxes.Count; i++) {
                elename= node.SelectNodes("//input[@type='hidden']")[i].Attributes["name"].Value;
                elevalue = node.SelectNodes("//input[@type='hidden']")[i].Attributes["value"].Value;

                T += "&" + elename + "="+ elevalue;
            }


             String checkedIDs = GenerateExistingService_POST_String(insHTMLPage);

             String finalstring = tempFormID + tempSeedID + T + checkedIDs;

             return finalstring;
        }

        private String GeneratePOSTMessage_ADD_VAS(String insHTMLPage)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(insHTMLPage);
            //formid
            var formidCheck = doc.DocumentNode.SelectNodes("//input[@name='formids']");
            if (formidCheck == null) return "";
            HtmlNode node = null;
            if (formidCheck.Count > 1)
                node = doc.DocumentNode.SelectNodes("//input[@name='formids']")[1];
            else
                return "";

            String tempFormID = node.Attributes["value"].Value;
            tempFormID = "formids=" + tempFormID.Replace(",", "%2C");

            //seedid
            var seedsidCheck = doc.DocumentNode.SelectNodes("//input[@name='seedids']");
            if (seedsidCheck == null) return "";
            node = null;
            if (formidCheck.Count > 1)
                node = doc.DocumentNode.SelectNodes("//input[@name='seedids']")[1];
            else
                return "";

            String tempSeedID = node.Attributes["value"].Value;
            tempSeedID = tempSeedID.Replace("/", "%2F");
            tempSeedID = tempSeedID.Replace("=", "%3D");
            tempSeedID = tempSeedID.Replace("+", "%2B");
            tempSeedID = tempSeedID.Replace("!", "%21");
            tempSeedID = tempSeedID.Replace("#", "%23");
            tempSeedID = tempSeedID.Replace("$", "%24");
            tempSeedID = tempSeedID.Replace("*", "%2A");
            tempSeedID = tempSeedID.Replace(",", "%2C");
            tempSeedID = tempSeedID.Replace("-", "%2D");
            tempSeedID = tempSeedID.Replace(".", "%2E");
            tempSeedID = tempSeedID.Replace(":", "%3A");
            tempSeedID = tempSeedID.Replace(";", "%3B");
            tempSeedID = tempSeedID.Replace("<", "%3C");
            tempSeedID = tempSeedID.Replace(">", "%3E");
            tempSeedID = tempSeedID.Replace("?", "%3F");
            tempSeedID = tempSeedID.Replace("@", "%40");
            tempSeedID = tempSeedID.Replace("^", "%5E");
            tempSeedID = tempSeedID.Replace("_", "%5F");
            tempSeedID = "&seedids=" + tempSeedID;

            node = doc.DocumentNode.SelectNodes("//div[@id='vasSubmitFormhidden']")[0];

            doc.LoadHtml(node.InnerHtml);

            node = doc.DocumentNode;


            var CheckedBoxes = node.SelectNodes("//input[@type='hidden']");
            if (CheckedBoxes == null) return "";

            String CheckedID;
            String T = "";
            String elename, elevalue = "";

            for (int i = 2; i < CheckedBoxes.Count; i++)
            {
                elename = node.SelectNodes("//input[@type='hidden']")[i].Attributes["name"].Value;
                elevalue = node.SelectNodes("//input[@type='hidden']")[i].Attributes["value"].Value;

                T += "&" + elename + "=" + elevalue;
            }


            String checkedIDs = GenerateExistingService_POST_String(insHTMLPage);

            String finalstring = tempFormID + tempSeedID + T + checkedIDs;

            return finalstring;
        }

        private Boolean ApplyTempVas_UsingEditPage(String inUSER_ID, String inEDW_HOST, String inEDW_USER, String inEDW_PASS)
        {
            return false;
        }
          private Boolean ApplyTempVas_UsingEditPage(String inUSER_ID, String folderName, String tick)
        {
         
         String  result = GetRequest_Read("https://simple.int.bell.ca/pegasus?component=editVASLink&page=accounts_ViewAdditionalServices&service=direct&session=T");
        //************ Verify correct page return or not *****************//
    
          HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
          SimpleVAS_DBTable TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY = new SimpleVAS_DBTable(folderName,tick);
          /// Check Edit Button Enabled or Disabled
         
          doc.LoadHtml(result);
        
           var UnlimitedPlanTable = doc.DocumentNode.SelectNodes("//table[@id='table_BWUNL']");  // check for unlimted plan table
           if (UnlimitedPlanTable == null) //if not exist then return error
           {
               
               TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.AddVAS_UpdateStatus(inUSER_ID, Constants.ADD_VAS.Status.__KICKOUT);
               Logging.Add(inUSER_ID, Constants.LOG.ERROR, "[code 1008] " +  Constants.LOG.ErrorDescription.NEW____NO_UNLIMITED_CHECKBOX);
               Logging.LogHtmlFile(inUSER_ID, result);  // store screen shot of page
               return false;    // Work again
               //      continue;
           }
            else  // check is unlimited plan check box checked or not if not then return error
            {
                
                try
                {
                    var st = UnlimitedPlanTable[0].InnerHtml;
                    bool unlimitedPlanChecked = true;
                    doc.LoadHtml(st);
                    var nodet = doc.DocumentNode.SelectNodes("//input[@id='BWUNL']");
                    if (nodet != null)
                    {
                        if (!nodet[0].Attributes["checked"].Value.Contains("checked"))
                        {
                            //mean unchecked
                            unlimitedPlanChecked = false;
                        }
                    }
                    else
                    {
                        unlimitedPlanChecked = false;
                    }

                    if (unlimitedPlanChecked == false)
                    {
                        TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.AddVAS_UpdateStatus(inUSER_ID, Constants.ADD_VAS.Status.__KICKOUT);
                        Logging.Add(inUSER_ID, Constants.LOG.ERROR, "[code 1009] " + Constants.LOG.ErrorDescription.NEW____NotChecked_UNLIMITED_PLAN);
                        Logging.LogHtmlFile(inUSER_ID, result);  // store screen shot of page
                        return false;    // Work again
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

        // ***************************************************************
        //   Verify_Additional_Services_Changes(result, inUSER_ID,Constants.ADD_VAS.VAS_TYPE);   // verify is any service change
        //*****************************************************************
          //////////////// GENERATE POST REQUEST MESSAGE FROM EDIT RETURN PAGE
           String POST_Message  = GeneratePOSTMessage_ADD_VAS(result);


            if(POST_Message.Length < 10) // measn POST Message generation Error
            {
                TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.AddVAS_UpdateStatus(inUSER_ID, Constants.ADD_VAS.Status.__ERROR);
                Logging.Add(inUSER_ID, Constants.LOG.ERROR, "[code 0009] " + Constants.LOG.ErrorDescription.POST_MESSAGE_GENERATION_ERROR);
                Logging.LogHtmlFile(inUSER_ID, result);  // store screen shot of page
                return false;    // Work again
            }


            //LITTLE CHANGE

            POST_Message = POST_Message.Replace("submitmode=&submitname=&editModeIf_0", "submitmode=&submitname=submitVASLink&editModeIf_0");


          result= Submit_VAS_ADD_POST_REQUEST(inUSER_ID, POST_Message);  // submit Edit post request on server  
         
            // Check EDIT Successfully done or not by reading thre result html
          doc = new HtmlAgilityPack.HtmlDocument();
          doc.LoadHtml(result);

          
          var itemTablePWEBEnabled = doc.DocumentNode.SelectNodes("//table[@class='listing']");
            if(itemTablePWEBEnabled == null) // not added successfully
            {
              
                TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.AddVAS_UpdateStatus(inUSER_ID, Constants.ADD_VAS.Status.__KICKOUT);
                Logging.Add(inUSER_ID, Constants.LOG.ERROR, "[code 0008] " + Constants.LOG.ErrorDescription.APPLY_VAS_ERROR);
                Logging.LogHtmlFile(inUSER_ID, result);
                return false;
            }
          HtmlNode node = doc.DocumentNode.SelectNodes("//table[@class='listing']")[0];


          String s = node.InnerText;
         
          if (s.ToUpper().Contains("UNLIMITED USAGE PLAN"))  // means Successfully Add VAS - now add in Database 
          {
              //update table left
             
              TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.AddVAS_UpdateStatus(inUSER_ID, Constants.ADD_VAS.Status.__COMPLETE);
              Logging.Add(inUSER_ID, Constants.LOG.SUCCESS, Constants.LOG.SUCCESS);
              return true;
          }
          else  // means not add vas successfully - now add in logs and update status ERROR  
          {
             
              TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.AddVAS_UpdateStatus(inUSER_ID, Constants.ADD_VAS.Status.__ERROR);
              Logging.Add(inUSER_ID, Constants.LOG.ERROR, "[code 0007] " + Constants.LOG.ErrorDescription.APPLY_VAS_ERROR);
              return false;
          }

        }

        private String Submit_VAS_ADD_POST_REQUEST(String inUSER_ID, String insPOST_Message)
        {
            String FunctionName = "SimpleVAS_WebScraper.Submit_VAS_ADD_POST_REQUEST";
            String result = "";
    //Anis      //  String strPost = "formids=editModeIf_0%2CsubmitVASLink%2CeditModeIf1_0%2CVASLoop2_0%2CVASLoop1_0%2CcurrentVasCheckedIf_0%2CcurrentVasCheckedIf_2%2CcurrentVasCheckedIf_4%2CcurrentVasCheckedIf_6%2CcurrentVasCheckedIf_8%2CcurrentVasCheckedIf_10%2CcurrentVasCheckedIf_12%2CVASLoop3_0%2CVASLoop_0%2CshowHeaderIf_0%2CterminateHeaderIf_0%2CopenRequiredChoice1If_0%2CcloseRequiredChoice1If_0%2CINETC%2CemailServiceNameNotUnlimited_0%2CdisplayVasPromotionsIf_0%2Cpromotions_INETC%2CshowMessageIf_0%2CoutputServiceCharDiv_0%2CshowHeaderIf_2%2CopenRequiredChoice1If_2%2CcloseRequiredChoice1If_2%2CMCFBS%2CemailServiceNameNotUnlimited_2%2CdisplayVasPromotionsIf_2%2CshowMessageIf_2%2CoutputServiceCharDiv_2%2CserviceCharLoop_0%2CshowHeaderIf_4%2CopenRequiredChoice1If_4%2CcloseRequiredChoice1If_4%2CMCFBT%2CemailServiceNameNotUnlimited_4%2CdisplayVasPromotionsIf_4%2CshowMessageIf_4%2CoutputServiceCharDiv_4%2CserviceCharLoop_2%2CshowHeaderIf_6%2CopenRequiredChoice1If_6%2CcloseRequiredChoice1If_6%2CMCFGD%2CemailServiceNameNotUnlimited_6%2CdisplayVasPromotionsIf_6%2CshowMessageIf_6%2CoutputServiceCharDiv_6%2CshowHeaderIf_8%2CopenRequiredChoice1If_8%2CcloseRequiredChoice1If_8%2CPWEB%2CemailServiceNameNotUnlimited_8%2CdisplayVasPromotionsIf_8%2CshowMessageIf_8%2CoutputServiceCharDiv_8%2CshowHeaderIf_10%2CterminateHeaderIf_2%2CopenRequiredChoice1If_10%2CcloseRequiredChoice1If_10%2CBWGBB%2CemailServiceNameNotUnlimited_10%2CdisplayVasPromotionsIf_10%2Cpromotions_BWGBB%2CshowMessageIf_10%2CoutputServiceCharDiv_10%2CserviceCharLoop_4%2CnotHiddenSC_0%2CshowCharCodeIf_0%2CifShowDescription_0%2CshowLabel_0%2CisConfiguredValue_0%2CisEditMode_0%2CisDistinct_0%2CisDistinctBool_0%2CBWGBB_BWQTY_1%2CisCSCAddress_0%2CisMainAccessNumber_0%2CisCusAccessNumber_0%2CisUserAccountNumber_0%2CisExtCarrier_0%2CisShowEffectiveDate_0%2CshowHeaderIf_12%2CopenRequiredChoice1If_12%2CcloseRequiredChoice1If_12%2CBWUNL%2CemailServiceNameNotUnlimited_12%2CdisplayVasPromotionsIf_12%2Cpromotions_BWUNL%2CshowMessageIf_12%2CoutputServiceCharDiv_12%2CserviceCharLoop_7&seedids=ZH4sIAAAAAAAAAG1VzW7jNhAO0BZ9kRx9qNMW6NWxHdRbxXYi7xY9jsWRRYQiBQ7prfaZ%2Bmp9hx2SMiUnvtjz%2B80PZ0b%2F%2FX%2F307e7ux%2Fvft7NCO1ZVnj%2Fy6yDU%2FirTNsZjdpFWjuQGi3ThETS6EB1%2FLPCGrxyBfTGB1NqUKngQlRI%2FXaAU2KeGKKU3%2FBK%2BOnFo%2B3XdY2VGzA5DbRbaPEZHSTDN8QOlDzjRjtWg5rKDrLFFPnRiJ7%2FFOiT5xJCoMCGeOZP02Jm9hZrukjMKXmDUuZr6XqFy4YRgk5gZREIQ%2BrMWiR0Ay31lUpGuDJX0YLmDBZVZbx2lAM%2FvxczpPG2wvcmOyvQBmGI4i4e2Wj5TprC7cHya03QJS1EK%2FWYFqcphcArSWzGKHhFzaFjKx9mA1aJYKsmPk4gnoxtw5hUUjh6ReLXXxoR0mHTZJh0B%2FOG%2BqO4MKetcTfs19Ya%2B8zjxcVQVLOqAbqI7n%2Bf4cQk%2B316%2Bbx%2B%2FefzazFKyt12yu%2BVP0mdJJWVPEUSSlRc9ViVRCUyNwwPNebrI1Rvj945bs98dmRm0K0V4f38YQbiDLpCUU4dU9il0eRbtJtVFi26TskKQrcn0nGIh3yB6C%2FsM1%2F25LA9WNAEVfa9PI9vW7B9HNi4i0ujFHSEK6DmaMCGsmqvVNgqJj3v2EYM1RXGdIeej8APLx1u6nkIaY3wlQvW87ghwaKL9AVlnmHmVzgBYkQIU3GNN4EL6V7yyw%2FMA1lvdK3kqXFxB3Kchzisy%2FVCizTncdPpglpLm2lsQYYTsZLUKejLdNgWQvC2UcyPnEV0W98e0ZbFbnltMhow3k31R%2BGiczzTW25PDPDR4InrtlOTzpADFVp0M0QlXX9Twd088%2FW57bUqC2M3%2B8MXWcezIMP6sSqVGq5cGpnhAQaudOB8DLoeb8qgy57c%2FEYqMRrwt%2BFRcjGa32fK1yOdgFd8q4bLzsmyzLpBJOtdh%2Fry0A6OFLP%2B98BUGCg%2BQRrOBZ5R%2FRoQ%2FDFr5iGjL5LkUeH9H4MqxItcBzqecqZl%2FAyV%2FthKlw3jlmYGjqiY4wXsvAunj2uY58i%2FhXqiap8Gmf5uwB3MyoRk0%2BQWkkILzkApUDqR3wHnnFhrXwcAAA%3D%3D&component=vasSubmitForm&page=accounts_ViewAdditionalServices&service=direct&session=T&submitmode=&submitname=submitVASLink&editModeIf_0=T&editModeIf1_0=T&VASLoop2_0=PSINETC&VASLoop2_0=PSMCFBS&VASLoop2_0=PSMCFBT&VASLoop2_0=PSMCFGD&VASLoop2_0=PSPWEB&VASLoop2_0=PSBWGBB&VASLoop2_0=PSBWUNL&VASLoop1_0=PSINETC&currentVasCheckedIf_0=F&VASLoop1_0=PSMCFBS&currentVasCheckedIf_2=F&VASLoop1_0=PSMCFBT&currentVasCheckedIf_4=F&VASLoop1_0=PSMCFGD&currentVasCheckedIf_6=T&VASLoop1_0=PSPWEB&currentVasCheckedIf_8=F&VASLoop1_0=PSBWGBB&currentVasCheckedIf_10=F&VASLoop1_0=PSBWUNL&currentVasCheckedIf_12=F&VASLoop3_0=PSINETC&VASLoop3_0=PSMCFBS&VASLoop3_0=PSMCFBT&VASLoop3_0=PSMCFGD&VASLoop3_0=PSPWEB&VASLoop3_0=PSBWGBB&VASLoop3_0=PSBWUNL&VASLoop_0=PSINETC&showHeaderIf_0=T&terminateHeaderIf_0=F&openRequiredChoice1If_0=F&closeRequiredChoice1If_0=F&emailServiceNameNotUnlimited_0=T&displayVasPromotionsIf_0=T&showMessageIf_0=F&outputServiceCharDiv_0=F&VASLoop_0=PSMCFBS&showHeaderIf_2=F&openRequiredChoice1If_2=F&closeRequiredChoice1If_2=F&emailServiceNameNotUnlimited_2=T&displayVasPromotionsIf_2=F&showMessageIf_2=F&outputServiceCharDiv_2=T&VASLoop_0=PSMCFBT&showHeaderIf_4=F&openRequiredChoice1If_4=F&closeRequiredChoice1If_4=F&emailServiceNameNotUnlimited_4=T&displayVasPromotionsIf_4=F&showMessageIf_4=F&outputServiceCharDiv_4=T&VASLoop_0=PSMCFGD&showHeaderIf_6=F&openRequiredChoice1If_6=F&closeRequiredChoice1If_6=F&emailServiceNameNotUnlimited_6=T&displayVasPromotionsIf_6=F&showMessageIf_6=F&outputServiceCharDiv_6=F&VASLoop_0=PSPWEB&showHeaderIf_8=F&openRequiredChoice1If_8=F&closeRequiredChoice1If_8=F&emailServiceNameNotUnlimited_8=T&displayVasPromotionsIf_8=F&showMessageIf_8=F&outputServiceCharDiv_8=F&VASLoop_0=PSBWGBB&showHeaderIf_10=T&terminateHeaderIf_2=T&openRequiredChoice1If_10=F&closeRequiredChoice1If_10=F&emailServiceNameNotUnlimited_10=T&displayVasPromotionsIf_10=T&showMessageIf_10=F&outputServiceCharDiv_10=T&serviceCharLoop_4=V0&notHiddenSC_0=T&showCharCodeIf_0=F&ifShowDescription_0=F&showLabel_0=F&isConfiguredValue_0=T&isEditMode_0=T&isDistinct_0=T&isDistinctBool_0=F&isCSCAddress_0=F&isMainAccessNumber_0=F&isCusAccessNumber_0=F&isUserAccountNumber_0=F&isExtCarrier_0=F&isShowEffectiveDate_0=F&VASLoop_0=PSBWUNL&showHeaderIf_12=F&openRequiredChoice1If_12=F&closeRequiredChoice1If_12=F&emailServiceNameNotUnlimited_12=F&displayVasPromotionsIf_12=T&showMessageIf_12=F&outputServiceCharDiv_12=T&MCFGD=on&BWGBB_BWQTY_1=0&BWUNL=on&promotions_BWUNL=PBW23";
            String strPost = insPOST_Message + "&BWUNL=on&promotions_BWUNL=PBW28"; 
           // String strPost = "formids=editModeIf_0%2CsubmitVASLink%2CeditModeIf1_0%2CVASLoop2_0%2CVASLoop1_0%2CcurrentVasCheckedIf_0%2CcurrentVasCheckedIf_2%2CcurrentVasCheckedIf_4%2CcurrentVasCheckedIf_6%2CcurrentVasCheckedIf_8%2CcurrentVasCheckedIf_10%2CVASLoop3_0%2CVASLoop_0%2CshowHeaderIf_0%2CterminateHeaderIf_0%2CopenRequiredChoice1If_0%2CcloseRequiredChoice1If_0%2CINETC%2CemailServiceNameNotUnlimited_0%2CdisplayVasPromotionsIf_0%2Cpromotions_INETC%2CshowMessageIf_0%2CoutputServiceCharDiv_0%2CshowHeaderIf_2%2CopenRequiredChoice1If_2%2CcloseRequiredChoice1If_2%2CMCFBS%2CemailServiceNameNotUnlimited_2%2CdisplayVasPromotionsIf_2%2CshowMessageIf_2%2CoutputServiceCharDiv_2%2CserviceCharLoop_0%2CshowHeaderIf_4%2CopenRequiredChoice1If_4%2CcloseRequiredChoice1If_4%2CMCFBT%2CemailServiceNameNotUnlimited_4%2CdisplayVasPromotionsIf_4%2CshowMessageIf_4%2CoutputServiceCharDiv_4%2CshowHeaderIf_6%2CopenRequiredChoice1If_6%2CcloseRequiredChoice1If_6%2CPWEB%2CemailServiceNameNotUnlimited_6%2CdisplayVasPromotionsIf_6%2CshowMessageIf_6%2CoutputServiceCharDiv_6%2CshowHeaderIf_8%2CterminateHeaderIf_2%2CopenRequiredChoice1If_8%2CcloseRequiredChoice1If_8%2CBWGBB%2CemailServiceNameNotUnlimited_8%2CdisplayVasPromotionsIf_8%2Cpromotions_BWGBB%2CshowMessageIf_8%2CoutputServiceCharDiv_8%2CserviceCharLoop_2%2CnotHiddenSC_0%2CshowCharCodeIf_0%2CifShowDescription_0%2CshowLabel_0%2CisConfiguredValue_0%2CisEditMode_0%2CisDistinct_0%2CisDistinctBool_0%2CBWGBB_BWQTY_1%2CisCSCAddress_0%2CisMainAccessNumber_0%2CisCusAccessNumber_0%2CisUserAccountNumber_0%2CisExtCarrier_0%2CisShowEffectiveDate_0%2CshowHeaderIf_10%2CopenRequiredChoice1If_10%2CcloseRequiredChoice1If_10%2CBWUNL%2CemailServiceNameNotUnlimited_10%2CdisplayVasPromotionsIf_10%2Cpromotions_BWUNL%2CshowMessageIf_10%2CoutputServiceCharDiv_10%2CserviceCharLoop_5&seedids=ZH4sIAAAAAAAAAG1VzW7jNhAO0BZ9kRx9qNMW6NWxHdRbxXYi7xY9jsWRRYQiBQ7prfaZ%2Bmp9hx2SMiUnvtjz%2B80PZ0b%2F%2FX%2F307e7ux%2Fvft7NCO1ZVnj%2Fy6yDU%2FirTNsZjdpFWjuQGi3ThETS6EB1%2FLPCGrxyBfTGB1NqUKngQlRI%2FXaAU2KeGKKU3%2FBK%2BOnFo%2B3XdY2VGzA5DbRbaPEZHSTDN8QOlDzjRjtWg5rKDrLFFPnRiJ7%2FFOiT5xJCoMCGeOZP02Jm9hZrukjMKXmDUuZr6XqFy4YRgk5gZREIQ%2BrMWiR0Ay31lUpGuDJX0YLmDBZVZbx2lAM%2FvxczpPG2wvcmOyvQBmGI4i4e2Wj5TprC7cHya03QJS1EK%2FWYFqcphcArSWzGKHhFzaFjKx9mA1aJYKsmPk4gnoxtw5hUUjh6ReLXXxoR0mHTZJh0B%2FOG%2BqO4MKetcTfs19Ya%2B8zjxcVQVLOqAbqI7n%2Bf4cQk%2B316%2Bbx%2B%2FefzazFKyt12yu%2BVP0mdJJWVPEUSSlRc9ViVRCUyNwwPNebrI1Rvj945bs98dmRm0K0V4f38YQbiDLpCUU4dU9il0eRbtJtVFi26TskKQrcn0nGIh3yB6C%2FsM1%2F25LA9WNAEVfa9PI9vW7B9HNi4i0ujFHSEK6DmaMCGsmqvVNgqJj3v2EYM1RXGdIeej8APLx1u6nkIaY3wlQvW87ghwaKL9AVlnmHmVzgBYkQIU3GNN4EL6V7yyw%2FMA1lvdK3kqXFxB3Kchzisy%2FVCizTncdPpglpLm2lsQYYTsZLUKejLdNgWQvC2UcyPnEV0W98e0ZbFbnltMhow3k31R%2BGiczzTW25PDPDR4InrtlOTzpADFVp0M0QlXX9Twd088%2FW57bUqC2M3%2B8MXWcezIMP6sSqVGq5cGpnhAQaudOB8DLoeb8qgy57c%2FEYqMRrwt%2BFRcjGa32fK1yOdgFd8q4bLzsmyzLpBJOtdh%2Fry0A6OFLP%2B98BUGCg%2BQRrOBZ5R%2FRoQ%2FDFr5iGjL5LkUeH9H4MqxItcBzqecqZl%2FAyV%2FthKlw3jlmYGjqiY4wXsvAunj2uY58i%2FhXqiap8Gmf5uwB3MyoRk0%2BQWkkILzkApUDqR3wHnnFhrXwcAAA%3D%3D&component=vasSubmitForm&page=accounts_ViewAdditionalServices&service=direct&session=T&submitmode=&submitname=submitVASLink&editModeIf_0=T&editModeIf1_0=T&VASLoop2_0=PSINETC&VASLoop2_0=PSMCFBS&VASLoop2_0=PSMCFBT&VASLoop2_0=PSPWEB&VASLoop2_0=PSBWGBB&VASLoop2_0=PSBWUNL&VASLoop1_0=PSINETC&currentVasCheckedIf_0=F&VASLoop1_0=PSMCFBS&currentVasCheckedIf_2=F&VASLoop1_0=PSMCFBT&currentVasCheckedIf_4=T&VASLoop1_0=PSPWEB&currentVasCheckedIf_6=F&VASLoop1_0=PSBWGBB&currentVasCheckedIf_8=F&VASLoop1_0=PSBWUNL&currentVasCheckedIf_10=F&VASLoop3_0=PSINETC&VASLoop3_0=PSMCFBS&VASLoop3_0=PSMCFBT&VASLoop3_0=PSPWEB&VASLoop3_0=PSBWGBB&VASLoop3_0=PSBWUNL&VASLoop_0=PSINETC&showHeaderIf_0=T&terminateHeaderIf_0=F&openRequiredChoice1If_0=F&closeRequiredChoice1If_0=F&emailServiceNameNotUnlimited_0=T&displayVasPromotionsIf_0=T&showMessageIf_0=F&outputServiceCharDiv_0=F&VASLoop_0=PSMCFBS&showHeaderIf_2=F&openRequiredChoice1If_2=F&closeRequiredChoice1If_2=F&emailServiceNameNotUnlimited_2=T&displayVasPromotionsIf_2=F&showMessageIf_2=F&outputServiceCharDiv_2=T&VASLoop_0=PSMCFBT&showHeaderIf_4=F&openRequiredChoice1If_4=F&closeRequiredChoice1If_4=F&emailServiceNameNotUnlimited_4=T&displayVasPromotionsIf_4=F&showMessageIf_4=F&outputServiceCharDiv_4=F&VASLoop_0=PSPWEB&showHeaderIf_6=F&openRequiredChoice1If_6=F&closeRequiredChoice1If_6=F&emailServiceNameNotUnlimited_6=T&displayVasPromotionsIf_6=F&showMessageIf_6=F&outputServiceCharDiv_6=F&VASLoop_0=PSBWGBB&showHeaderIf_8=T&terminateHeaderIf_2=T&openRequiredChoice1If_8=F&closeRequiredChoice1If_8=F&emailServiceNameNotUnlimited_8=T&displayVasPromotionsIf_8=T&showMessageIf_8=F&outputServiceCharDiv_8=T&serviceCharLoop_2=V0&notHiddenSC_0=T&showCharCodeIf_0=F&ifShowDescription_0=F&showLabel_0=F&isConfiguredValue_0=T&isEditMode_0=T&isDistinct_0=T&isDistinctBool_0=F&isCSCAddress_0=F&isMainAccessNumber_0=F&isCusAccessNumber_0=F&isUserAccountNumber_0=F&isExtCarrier_0=F&isShowEffectiveDate_0=F&VASLoop_0=PSBWUNL&showHeaderIf_10=F&openRequiredChoice1If_10=F&closeRequiredChoice1If_10=F&emailServiceNameNotUnlimited_10=F&displayVasPromotionsIf_10=T&showMessageIf_10=F&outputServiceCharDiv_10=T";
            StreamWriter myWriter = null;
            CookieContainer cookies = new CookieContainer();
            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(SIMPLE_URL);
            cookies.Add(new Cookie("JSESSIONID", LoginSessionID.Trim()) { Domain = "simple.int.bell.ca" });
            cookies.Add(new Cookie("userName", SIMPLE_USER) { Domain = "simple.int.bell.ca" });
            objRequest.CookieContainer = cookies;
            objRequest.Method = "POST";
            objRequest.Referer = "https://simple.int.bell.ca/accounts_LandingPage.html";
            objRequest.ContentLength = strPost.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";
            objRequest.Host = "simple.int.bell.ca";
            try
            {
                myWriter = new StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(strPost);
            }
            catch (Exception e)
            {
                Logging.Log_Exception(FunctionName, e.Message, inUSER_ID);
                throw new Exception(e.Message);
            }
            finally
            {
 
               myWriter.Close();
            }
            try { 
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader sr =
               new StreamReader(objResponse.GetResponseStream()))
            {
                result = sr.ReadToEnd();
                sr.Close();
            }

            return result;
                }//try
            catch (Exception ex)
            {
                Logging.Log_Exception(FunctionName, ex.Message, inUSER_ID);
                throw new Exception(ex.Message);
            }
        }

        private string ParseSPCodefromPage(string HtmlPage)
        {
            //
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(HtmlPage);

            var getSpNode = doc.DocumentNode.SelectNodes("//div[@id='productLoop']");

            // return "";
            if (getSpNode != null)
            {
                try
                {
                    var s = getSpNode[0].InnerHtml;
                    doc.LoadHtml(s);
                    var node = doc.DocumentNode.SelectNodes("//div[@class='hidden']");
                    if (node != null)
                    {
                        if (!node[0].Attributes["id"].Value.Contains("_expanded"))
                            throw new Exception();

                        string SPCode = node[0].Attributes["id"].Value.Replace("_expanded", "");
                        return "S" + SPCode;

                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            return "";

        }


        private string ParseSPCodeForUnlimited(string HtmlPage)
        {
            //
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(HtmlPage);
           // var getSpNode = B

            for (int i = 0; i < 9; i++)
            {
                doc = new HtmlAgilityPack.HtmlDocument();
                doc.LoadHtml(HtmlPage);
                var getSpNode = doc.DocumentNode.SelectNodes("//div[@id='servicesForLoop_"+i+"']");
                if (getSpNode == null)
                    break;
                if (getSpNode != null)
                {
                    try
                    {
                        var s = getSpNode[0].InnerHtml;
                        doc.LoadHtml(s);
                        var node = doc.DocumentNode.SelectNodes("//a[@class='expanded']");
                        if (node != null)
                        {
                            //if (!node[0].Attributes["href"].Value.Contains("accounts_ViewProductInfo"))
                            //    throw new Exception();


                            if (node[0].InnerText != "Unlimited Usage Plan")
                                continue;
                            else { 
                            string href = node[0].Attributes["href"].Value;
                            string[] stringSeparators = new string[] { "sp=" };
                            string[] split = href.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                            if (split.Length > 2)
                                return split[2];
                            else
                                return "";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

            }



           // var getSpNode = doc.DocumentNode.SelectNodes("//div[@id='servicesForLoop_3']");

            // return "";
            //if (getSpNode != null)
            //{
            //    try
            //    {
            //        var s = getSpNode[0].InnerHtml;
            //        doc.LoadHtml(s);
            //        var node = doc.DocumentNode.SelectNodes("//a[@id='serviceLink_3']");
            //        if (node != null)
            //        {
            //            if (!node[0].Attributes["href"].Value.Contains("accounts_ViewProductInfo"))
            //                throw new Exception();

            //            string href = node[0].Attributes["href"].Value;
            //            string[] stringSeparators = new string[] { "sp=" };
            //            string[] split = href.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
            //            if (split.Length > 2)
            //                return split[2];
            //            else
            //                return "";

            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        throw ex;
            //    }
            //}

            return "";

        }


        private String GetEdit_UserVASPage(String USER_ID)
        {
            String FunctionName = "SimpleVAS_WebScraper.Edit_UserVAS";
            String result = "";
            String strPost = "formids=pcidtsResultCodeAccntSrch%2CpcidtsTokenAccntSrch%2CpcidtsLogNoteAccntSrch%2ChasMessages_0%2ChasMessages_2%2CcriteriaSelect%2CsearchField%2CsearchLink%2CshowBackButton_0&seedids=ZH4sIAAAAAAAAAF1RW27CMBBESL0Jn3xQ9QSUFpUKWtpwgZUzSSz8iNYOVThTr9Y7dJ1AePx4d2d3Z8b279%2Fo4TgajZ%2FGj9MAPmiFyWxaU5mC8rb2Di52uYukHVjygBC0dymr5XhBQY2Ja2p9k0ZDBWPSSghr7fY7KvtiKRSZPuIGfP9qwO1rUUDFE6fYAH%2BQxQaR%2BsE9UJPRB6xclDaZa2ynLXrlZ5%2B3Egy5spErJKFUJj3%2F5i2GYssowhnxZb9NxvifLLYGi0oYUi%2BHYlBAsi4lIyCecu1uWrqjy4ZbWHLiYK6Ub1wMg%2FDmHhZK37DC%2Fcgn5%2BAEJpV43hiGFndoL7cllt%2B6YtdhnlvtLrbEps5z3CDdY1yAbziRPj0l9VwZiFXVfU5Klp7tZPYP3zgpwDwCAAA%3D&component=accountSearch.searchForm&page=accounts_SearchResults&service=direct&session=T&submitmode=&submitname=searchLink&pcidtsResultCodeAccntSrch=empty&pcidtsTokenAccntSrch=&pcidtsLogNoteAccntSrch=&hasMessages_0=F&hasMessages_2=F&showBackButton_0=F&criteriaSelect=0&searchField=" + USER_ID;   
            StreamWriter myWriter = null;
            CookieContainer cookies = new CookieContainer();
            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(SIMPLE_URL);
            cookies.Add(new Cookie("JSESSIONID", LoginSessionID.Trim()) { Domain = "simple.int.bell.ca" });
            cookies.Add(new Cookie("userName", SIMPLE_USER) { Domain = "simple.int.bell.ca" });
            objRequest.CookieContainer = cookies;
            objRequest.Method = "POST";
            objRequest.Referer = "https://simple.int.bell.ca/accounts_LandingPage.html";
            objRequest.ContentLength = strPost.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";
            objRequest.Host = "simple.int.bell.ca";
            try
            {
                myWriter = new StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(strPost);
            }
            catch (Exception e)
            {
                Logging.Log_Exception(FunctionName, e.Message);
                throw new Exception(e.Message);
            }
            finally
            {
 
               myWriter.Close();
            }

       
            try
            {
                HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
                using (StreamReader sr =
                   new StreamReader(objResponse.GetResponseStream()))
                {
                    result = sr.ReadToEnd();
                    sr.Close();
                }

                String OldResult = result;
                // GoTo CusomerLandingPage
                result = GetRequest_Read("https://simple.int.bell.ca/accounts_ViewCustomerInfo.html");
                if (result == "") return OldResult;
                // GoTo ViewProductPage
                result = GetRequest_Read("https://simple.int.bell.ca/accounts_ViewProductInfo.html");

                var spCode = ParseSPCodefromPage(result);
                var spCode2 = ParseSPCodeForUnlimited(result);
                if (string.IsNullOrEmpty(spCode))
                    return "SP NOT FOUND";

                result = GetRequest_Read("https://simple.int.bell.ca/pegasus?component=serviceLink1&page=accounts_ViewProductInfo&service=direct&session=T&sp=" + spCode +"&sp=" + spCode2);

                if (!result.Contains("PBW27"))
                {
                    if (result.Contains("PBW28")){
                        return "already PBW28";
                    }
                    else
                    return "NOT (PBW27 or PBW28)";
                }

                // GoTo Product Value Addded Service Page 
                result = GetRequest_Read("https://simple.int.bell.ca/accounts_ViewAdditionalServices.html");

                /********** Case: Check Edit Button is Enable or not ******/

                // Done Editing if no Error
                //     result = GetRequest_Read("https://simple.int.bell.ca/pegasus?component=editVASLink&page=accounts_ViewAdditionalServices&service=direct&session=T");

                return result;   // retunr HTML page where edit button is display in Product Page
               
            }//try
            catch (Exception ex)
            {
                Logging.Log_Exception(FunctionName, ex.Message, USER_ID);
                throw new Exception(ex.Message);
            }
               
        }

        private String GetRequest_Read(string url)
        {

            String result = "";
           // StreamWriter myWriter = null;
            CookieContainer cookies = new CookieContainer();
            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(url);
            cookies.Add(new Cookie("JSESSIONID", LoginSessionID.Trim()) { Domain = "simple.int.bell.ca" });
            cookies.Add(new Cookie("userName", SIMPLE_USER) { Domain = "simple.int.bell.ca" });
            objRequest.CookieContainer = cookies;
            objRequest.Method = "GET";
            objRequest.Host = "simple.int.bell.ca";
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader sr =
               new StreamReader(objResponse.GetResponseStream()))
            {
                result = sr.ReadToEnd();
                sr.Close();
            }
            return result;
        } 

        #endregion

        #region Remove VAS

        private Boolean RemoveTempVas_UsingEditPage(String inUSER_ID, String inEDW_HOST, String inEDW_USER, String inEDW_PASS)
        {

            String result = GetRequest_Read("https://simple.int.bell.ca/pegasus?component=editVASLink&page=accounts_ViewAdditionalServices&service=direct&session=T");
            //************ Verify correct page return or not *****************//

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            SimpleVAS_DBTable TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY = new SimpleVAS_DBTable(inEDW_HOST, inEDW_USER, inEDW_PASS);
            doc.LoadHtml(result);

            var itemEditButtonEnabled = doc.DocumentNode.SelectNodes("//table[@id='table_PWEB']");
            if (itemEditButtonEnabled == null) // means wrong page return
            {
                TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.RemoveVAS_UpdateStatus(inUSER_ID, Constants.REMOVE_VAS.Status.__ERROR);
                Logging.Add(inUSER_ID, Constants.LOG.ERROR, "[code 0006] " + Constants.LOG.RemoveVAS_ErrorDescription.EDIT_PAGE_RETURN_ERROR,Constants.REMOVE_VAS.VAS_TYPE);
                Logging.LogHtmlFile(inUSER_ID, result);  // store screen shot of page
                return false;    // Work again
                //      continue;
            }


            // ***************************************************************
            Verify_Additional_Services_Changes(result, inUSER_ID, Constants.REMOVE_VAS.VAS_TYPE);   // verify is any service change
            //*****************************************************************

            //*****************************************************************
            //////////////// GENERATE POST REQUEST MESSAGE FROM EDIT RETURN PAGE
            String POST_Message = GeneratePOSTMessage_REMOVE_VAS(result);

            if (POST_Message.Length < 10) // measn POST Message generation Error
            {
                TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.RemoveVAS_UpdateStatus(inUSER_ID, Constants.REMOVE_VAS.Status.__ERROR);
                Logging.Add(inUSER_ID, Constants.LOG.ERROR, "[code 0009] " + Constants.LOG.RemoveVAS_ErrorDescription.POST_MESSAGE_GENERATION_ERROR, Constants.REMOVE_VAS.VAS_TYPE);
                Logging.LogHtmlFile(inUSER_ID, result);  // store screen shot of page
                return false;    // Work again
            }

            result = Submit_VAS_REMOVE_POST_REQUEST(inUSER_ID, POST_Message);  // submit Edit post request on server  

            // Check EDIT Successfully done or not by reading thre result html
            doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(result);

            var itemTablePWEBEnabled = doc.DocumentNode.SelectNodes("//table[@class='listing']");
            if (itemTablePWEBEnabled == null) // not added successfully
            {

                var itemVerifyResultEnabled = doc.DocumentNode.SelectNodes("//span[@class='accountIdentifier']");
                if (itemVerifyResultEnabled == null) // means wrong page return
                {
                    TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.RemoveVAS_UpdateStatus(inUSER_ID, Constants.REMOVE_VAS.Status.__ERROR);
                    Logging.Add(inUSER_ID, Constants.LOG.ERROR, "[code 0006] " + Constants.LOG.RemoveVAS_ErrorDescription.REMOVE_VAS_ERROR, Constants.REMOVE_VAS.VAS_TYPE);
                    Logging.LogHtmlFile(inUSER_ID, result);  // store screen shot of page
                    return false;    
                    //      continue;
                }

                TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.RemoveVAS_UpdateStatus(inUSER_ID, Constants.REMOVE_VAS.Status.__REMOVED);
                Logging.Add(inUSER_ID, Constants.LOG.SUCCESS, "[code 0008] " + Constants.LOG.SUCCESS, Constants.REMOVE_VAS.VAS_TYPE);
             // Logging.LogHtmlFile(inUSER_ID, result);
                return true;
            }
            HtmlNode node = doc.DocumentNode.SelectNodes("//table[@class='listing']")[0];
            String s = node.InnerText;
            if (s.ToUpper().Contains("UNLIMITED USAGE PLAN") && s.ToUpper().Contains("TEMP $0 WITH PLAN CHANGE") )  // means TEMP VAS not successfully removed  
            {
                //update table left

                TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.RemoveVAS_UpdateStatus(inUSER_ID, Constants.REMOVE_VAS.Status.__ERROR);
                Logging.Add(inUSER_ID, Constants.LOG.ERROR, Constants.LOG.RemoveVAS_ErrorDescription.REMOVE_VAS_ERROR);
                Logging.LogHtmlFile(inUSER_ID, result);
                return false;
            }
            else  // means vas successfully removed - now add in logs and update status REMOVED  
            {

                TABLE_HHPM_PERM_TEMP_USG_VAS_DAILY.RemoveVAS_UpdateStatus(inUSER_ID, Constants.REMOVE_VAS.Status.__REMOVED);
                Logging.Add(inUSER_ID, Constants.LOG.SUCCESS, "[code 0007] " + Constants.LOG.SUCCESS, Constants.REMOVE_VAS.VAS_TYPE);
                return true;
            }

        }

        private String GenerateExistingService_POST_String(String insHtml){

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(insHtml);
            var DivBordered = doc.DocumentNode.SelectNodes("//div[@class='borderedContent']");

            if (DivBordered == null) return "";

            HtmlNode node = doc.DocumentNode.SelectNodes("//div[@class='borderedContent']")[0];
           
            doc.LoadHtml(node.InnerHtml);

            node = doc.DocumentNode;

            var CheckedBoxes = node.SelectNodes("//input[@checked='checked']");
            if (CheckedBoxes == null) return "";

            String CheckedID;
            String FinalString = "";
           
            for (int i = 0; i < CheckedBoxes.Count; i++) {
                CheckedID = node.SelectNodes("//input[@checked='checked']")[i].Attributes["id"].Value;
                if (CheckedID == "BWUNL") continue;
                FinalString += "&" + CheckedID + "=on";
            }

            return FinalString;
        }

        private Boolean Verify_Additional_Services_Changes(String inHTML, String inAcctNo, String inOperation_Type){

            // This function will close the application and generate email in case of Additional Services change

            ////*********** IMRORTANT *****************
            // If any additional services changes, then we have to change one function called GenerateExistingService_POST_String(String insHtml);
            // Current Process expected following checkboxes and Submit controls with following IDs
            // input type="checkbox" name="INETC" id="INETC"    (Bell Tech Expert)
            // input type="checkbox" name="MCFBS" id="MCFBS"    (McAfee Security Service - Best)
            // input type="checkbox" name="MCFBT" id="MCFBT"    (McAfee Security Service - Better)
            // input type="checkbox" name="MCFGD" id="MCFGD"    (McAfee Security Service - Good)
            // input type="checkbox" name="PWEB" id="PWEB"      (Personal Web Space)
            // input type="checkbox" name="BWGBB" id="BWGBB"    (25 GB Extra Usage Plan)
            // input type="checkbox" name="BWUNL" id="BWUNL"    (Unlimited Usage Plan)
            // select name="promotions_BWUNL" id="promotions_BWUNL"   (Unlimited Usage Plan)
            //******************************************

            Boolean IsServiceChange = false; //in case of new vas found otherthan our expectation then close the application and send alert
            String NewAdditionalService_Details = "";
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(inHTML);

            var checkForm = doc.DocumentNode.SelectNodes("//div[@class='borderedContent']"); 

            if (checkForm == null) return false;

            HtmlNode node = doc.DocumentNode.SelectNodes("//div[@class='borderedContent']")[0];
            doc.LoadHtml(node.InnerHtml);
            node = doc.DocumentNode;
            // Verify CheckBox
            var CheckedBoxesList = node.SelectNodes("//input[@type='checkbox']");
            if (CheckedBoxesList == null) return false;

            String CheckedID;
           // String FinalString = "";
            
            for (int i = 0; i < CheckedBoxesList.Count; i++) ///verify checkBox with  DB list
            {
                CheckedID = CheckedBoxesList[i].Attributes["id"].Value;
                if (Additonal_Services_IDList.Contains(CheckedID) == false)
                {
                    NewAdditionalService_Details += " CheckBox [" + CheckedID + "]";
                    IsServiceChange = true;
                }
            }
            // Verify DropBox
            var SelectBox = node.SelectNodes("//select");
            if (SelectBox == null) return false;


            String SelectBoxIDs;
            // String FinalString = "";

            for (int i = 0; i < SelectBox.Count; i++) ///verify checkBox with  DB list
            {
                SelectBoxIDs = node.SelectNodes("//select")[i].Attributes["id"].Value;
                if (Additonal_Services_IDList.Contains(SelectBoxIDs.ToUpper().Trim()) == false)
                {
                    NewAdditionalService_Details += " SelectBox [" + SelectBoxIDs + "]";
                    IsServiceChange = true;
                }
            }

            // select name="promotions_BWUNL" id="promotions_BWUNL"   (Unlimited Usage Plan)
            var promotions_BWUNL = doc.DocumentNode.SelectNodes("//select[@id='promotions_BWUNL']");
            if (promotions_BWUNL == null)
            {
                NewAdditionalService_Details += " Unlimited Temp VAS DropBox Missing [promotions_BWUNL]";
                IsServiceChange = true;
            }

        
            // Verify all input controls other than Hidden type and checkBox

            var InputControlList = node.SelectNodes("//input");
            if (InputControlList == null) return false;


            String inputControlID="";
            String inputControlType="";
            // String FinalString = "";

            for (int i = 0; i < InputControlList.Count; i++) ///verify checkBox with  DB list
            {
                
                if (node.SelectNodes("//input")[i].Attributes["id"] != null)
                {
                    inputControlID = node.SelectNodes("//input")[i].Attributes["name"].Value;
                }
                else if (node.SelectNodes("//input")[i].Attributes["name"] != null)
                {
                    inputControlID = node.SelectNodes("//input")[i].Attributes["name"].Value;
                }
                else
                {
                    NewAdditionalService_Details += " New Input Control[ " + node.SelectNodes("//input")[i].OuterHtml + "]";
                    IsServiceChange = true;
                }

                if (node.SelectNodes("//input")[i].Attributes["type"] != null)
                {
                    inputControlType = "TYPE_" + node.SelectNodes("//input")[i].Attributes["type"].Value;  // append TYPE keyword in type because type add in DB with TYPE keyword
                }
                else
                {
                    NewAdditionalService_Details += " New Input Control[ " + node.SelectNodes("//input")[i].OuterHtml + "]";
                    IsServiceChange = true;
                }



                if (Additonal_Services_IDList.Contains(inputControlType.ToUpper().Trim()) == false)
                {
                    NewAdditionalService_Details += " New Input Control[ " + inputControlType + " ID:" + inputControlID + "]";
                    IsServiceChange = true;
                }
            }

            if (IsServiceChange == true) // means service has been changed , alert every one and no need to process further
            {
                Logging.Log_Exception("Verify_Additional_Services_Changes", "New VAS Services Found on Edit Page! Verify Log for ID[" + inAcctNo + "] During [" + inOperation_Type + "] ");
                Logging.LogHtmlFile(inAcctNo, inHTML);
                Email.Generate_ServiceChangeEmail(NewAdditionalService_Details);
                Console.WriteLine("Error: New VAS Services found on Edit Page!, Please refere User guide document to add new service in configuration");
                System.Environment.Exit(-1);
            }



            return true;
        }

        private String Submit_VAS_REMOVE_POST_REQUEST(String inUSER_ID,String insPOST_Message )
        {
            String FunctionName = "SimpleVAS_WebScraper.Submit_VAS_REMOVE_POST_REQUEST";
            String result = "";
           // String strPost = "formids=editModeIf_0%2CsubmitVASLink%2CeditModeIf1_0%2CVASLoop2_0%2CVASLoop1_0%2CcurrentVasCheckedIf_0%2CcurrentVasCheckedIf_2%2CcurrentVasCheckedIf_4%2CcurrentVasCheckedIf_6%2CcurrentVasCheckedIf_8%2CcurrentVasCheckedIf_10%2CcurrentVasCheckedIf_12%2CVASLoop3_0%2CVASLoop_0%2CshowHeaderIf_0%2CterminateHeaderIf_0%2CopenRequiredChoice1If_0%2CcloseRequiredChoice1If_0%2CINETC%2CemailServiceNameNotUnlimited_0%2CdisplayVasPromotionsIf_0%2Cpromotions_INETC%2CshowMessageIf_0%2CoutputServiceCharDiv_0%2CshowHeaderIf_2%2CopenRequiredChoice1If_2%2CcloseRequiredChoice1If_2%2CMCFBS%2CemailServiceNameNotUnlimited_2%2CdisplayVasPromotionsIf_2%2CshowMessageIf_2%2CoutputServiceCharDiv_2%2CserviceCharLoop_0%2CshowHeaderIf_4%2CopenRequiredChoice1If_4%2CcloseRequiredChoice1If_4%2CMCFBT%2CemailServiceNameNotUnlimited_4%2CdisplayVasPromotionsIf_4%2CshowMessageIf_4%2CoutputServiceCharDiv_4%2CserviceCharLoop_2%2CshowHeaderIf_6%2CopenRequiredChoice1If_6%2CcloseRequiredChoice1If_6%2CMCFGD%2CemailServiceNameNotUnlimited_6%2CdisplayVasPromotionsIf_6%2CshowMessageIf_6%2CoutputServiceCharDiv_6%2CshowHeaderIf_8%2CopenRequiredChoice1If_8%2CcloseRequiredChoice1If_8%2CPWEB%2CemailServiceNameNotUnlimited_8%2CdisplayVasPromotionsIf_8%2CshowMessageIf_8%2CoutputServiceCharDiv_8%2CshowHeaderIf_10%2CterminateHeaderIf_2%2CopenRequiredChoice1If_10%2CcloseRequiredChoice1If_10%2CBWGBB%2CemailServiceNameNotUnlimited_10%2CdisplayVasPromotionsIf_10%2Cpromotions_BWGBB%2CshowMessageIf_10%2CoutputServiceCharDiv_10%2CserviceCharLoop_4%2CnotHiddenSC_0%2CshowCharCodeIf_0%2CifShowDescription_0%2CshowLabel_0%2CisConfiguredValue_0%2CisEditMode_0%2CisDistinct_0%2CisDistinctBool_0%2CBWGBB_BWQTY_1%2CisCSCAddress_0%2CisMainAccessNumber_0%2CisCusAccessNumber_0%2CisUserAccountNumber_0%2CisExtCarrier_0%2CisShowEffectiveDate_0%2CshowHeaderIf_12%2CopenRequiredChoice1If_12%2CcloseRequiredChoice1If_12%2CBWUNL%2CemailServiceNameNotUnlimited_12%2CdisplayVasPromotionsIf_12%2Cpromotions_BWUNL%2CshowMessageIf_12%2CoutputServiceCharDiv_12&seedids=ZH4sIAAAAAAAAAH1VzZLbNgzemTSTF9mjD%2FU2nenVa3unTrReZ%2BWk0yMsQRZnKZLDH6fKM%2FXV%2Bg4FSJmSdz252MAH8ANAAtC%2F%2F928%2F3Fz88vNBz9zaE%2BiwttfZwaO%2FFfpzmiFykdZeRAKLckOnRNasWToZ4UNBOkL6HVgV9eilHzEuUKolz0ck%2FJAFKX4gRfgpy8Bbb9uGqz8wElpoN1Ch4%2FoITm%2BIBqQ4oQb5ckMcortRYcp8r2ue%2FqToI6BSuBArHI8%2FafuMCs7i407I%2FqYToOU%2Bnvpe4nLlhjYVmNlERxy6qRadOgHWagLk4h0Za6iA0UZLKpKB%2BVdDvz4GiZKHWyFr12ebI2WQY7izyey0%2FIVmsLtwNJrTdiFW9SdUGNalKaoa7xA4mWMwDMqCh2v8m42cJUItmrj47DwoG3HbVKJ2rtndPT6S11zOuSaHJNtr19QvYULfdxqf8V%2Fba22j9ReVIyLZjK14M7Q7e8znLjkc5%2B%2BfF0%2F%2F%2F31uRiR8mk71XcyHIVKSGUFdZGAEiVVPVYlUNZZG5rHtfr7PVQv98F7up757EDKYFtLh7fzuxnUJ1AV1uX0YAq71MqFDu1mlaGFMVJUwLc9QccmHvIF5z5jn%2FWydx67vQXloMpnz88Tug5sHxs2zuJSSwnG4Qpce9BguawmSMlTRWKgGdvUQ3WF1mbf0xJ4dzK4aeYc0uo6VJ6953FC2MNE%2BcwyzzTzCx6mGBm4Ky75JnSc7jm%2F%2FMDUkM1GNVIcWx9nIMe5i826XC9Unfo8Tro7szbCZhk7ELwiVsIZCX2ZFtuirmnaXMzPeYvot6E7oC2Lp%2BWly%2BhAfFfNb8GF8dTTW7qeGACMAes7SvMnQd6CD3Q5dspjtPMg%2BR6vMlTC91cNdOUnWlHXT63KQtvNbv9NNHF3CJ5RMqVUOfvUV8MrDVrpwYcYdD0unsGWT9ILtULWowN9QO4FFaPoEad6M8qJeEULbVj%2FlCxh1g%2BQaJ4MqnM3eDi4mPU%2Fe5K462hPKTgVeEL5GzOEQ7bMOaNvwomDxNs%2FBhPHi5oBFfc9ySJ%2Bq8pw6ITPjnGUswIHlKTRlJrgeT9SDfMc%2BSPXE0271O3urxb8Xq80J5vauxCOr%2BAELgVKe%2FR%2FCCLMY4QHAAA%3D&component=vasSubmitForm&page=accounts_ViewAdditionalServices&service=direct&session=T&submitmode=&submitname=submitVASLink&editModeIf_0=T&editModeIf1_0=T&VASLoop2_0=PSINETC&VASLoop2_0=PSMCFBS&VASLoop2_0=PSMCFBT&VASLoop2_0=PSMCFGD&VASLoop2_0=PSPWEB&VASLoop2_0=PSBWGBB&VASLoop2_0=PSBWUNL&VASLoop1_0=PSINETC&currentVasCheckedIf_0=F&VASLoop1_0=PSMCFBS&currentVasCheckedIf_2=F&VASLoop1_0=PSMCFBT&currentVasCheckedIf_4=F&VASLoop1_0=PSMCFGD&currentVasCheckedIf_6=T&VASLoop1_0=PSPWEB&currentVasCheckedIf_8=F&VASLoop1_0=PSBWGBB&currentVasCheckedIf_10=F&VASLoop1_0=PSBWUNL&currentVasCheckedIf_12=T&VASLoop3_0=PSINETC&VASLoop3_0=PSMCFBS&VASLoop3_0=PSMCFBT&VASLoop3_0=PSMCFGD&VASLoop3_0=PSPWEB&VASLoop3_0=PSBWGBB&VASLoop3_0=PSBWUNL&VASLoop_0=PSINETC&showHeaderIf_0=T&terminateHeaderIf_0=F&openRequiredChoice1If_0=F&closeRequiredChoice1If_0=F&emailServiceNameNotUnlimited_0=T&displayVasPromotionsIf_0=T&showMessageIf_0=F&outputServiceCharDiv_0=F&VASLoop_0=PSMCFBS&showHeaderIf_2=F&openRequiredChoice1If_2=F&closeRequiredChoice1If_2=F&emailServiceNameNotUnlimited_2=T&displayVasPromotionsIf_2=F&showMessageIf_2=F&outputServiceCharDiv_2=T&VASLoop_0=PSMCFBT&showHeaderIf_4=F&openRequiredChoice1If_4=F&closeRequiredChoice1If_4=F&emailServiceNameNotUnlimited_4=T&displayVasPromotionsIf_4=F&showMessageIf_4=F&outputServiceCharDiv_4=T&VASLoop_0=PSMCFGD&showHeaderIf_6=F&openRequiredChoice1If_6=F&closeRequiredChoice1If_6=F&emailServiceNameNotUnlimited_6=T&displayVasPromotionsIf_6=F&showMessageIf_6=F&outputServiceCharDiv_6=F&VASLoop_0=PSPWEB&showHeaderIf_8=F&openRequiredChoice1If_8=F&closeRequiredChoice1If_8=F&emailServiceNameNotUnlimited_8=T&displayVasPromotionsIf_8=F&showMessageIf_8=F&outputServiceCharDiv_8=F&VASLoop_0=PSBWGBB&showHeaderIf_10=T&terminateHeaderIf_2=T&openRequiredChoice1If_10=F&closeRequiredChoice1If_10=F&emailServiceNameNotUnlimited_10=T&displayVasPromotionsIf_10=T&showMessageIf_10=F&outputServiceCharDiv_10=T&serviceCharLoop_4=V0&notHiddenSC_0=T&showCharCodeIf_0=F&ifShowDescription_0=F&showLabel_0=F&isConfiguredValue_0=T&isEditMode_0=T&isDistinct_0=T&isDistinctBool_0=F&isCSCAddress_0=F&isMainAccessNumber_0=F&isCusAccessNumber_0=F&isUserAccountNumber_0=F&isExtCarrier_0=F&isShowEffectiveDate_0=F&VASLoop_0=PSBWUNL&showHeaderIf_12=F&openRequiredChoice1If_12=F&closeRequiredChoice1If_12=F&emailServiceNameNotUnlimited_12=F&displayVasPromotionsIf_12=T&showMessageIf_12=F&outputServiceCharDiv_12=F&MCFGD=on&BWGBB_BWQTY_1=0";
            String strPost = insPOST_Message;
            StreamWriter myWriter = null;
            CookieContainer cookies = new CookieContainer();
            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(SIMPLE_URL);
            cookies.Add(new Cookie("JSESSIONID", LoginSessionID.Trim()) { Domain = "simple.int.bell.ca" });
            cookies.Add(new Cookie("userName", SIMPLE_USER) { Domain = "simple.int.bell.ca" });
            objRequest.CookieContainer = cookies;
            objRequest.Method = "POST";
            objRequest.Referer = "https://simple.int.bell.ca/accounts_LandingPage.html";
            objRequest.ContentLength = strPost.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";
            objRequest.Host = "simple.int.bell.ca";
            try
            {
                myWriter = new StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(strPost);
            }
            catch (Exception e)
            {
                Logging.Log_Exception(FunctionName, e.Message, inUSER_ID);
                throw new Exception(e.Message);
            }
            finally
            {

                myWriter.Close();
            }
            try
            {
                HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
                using (StreamReader sr =
                   new StreamReader(objResponse.GetResponseStream()))
                {
                    result = sr.ReadToEnd();
                    sr.Close();
                }

                return result;
            }//try
            catch (Exception ex)
            {
                Logging.Log_Exception(FunctionName, ex.Message, inUSER_ID);
                throw new Exception(ex.Message);
            }
        }

        #endregion





    }
}
