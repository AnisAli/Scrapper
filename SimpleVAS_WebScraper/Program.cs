﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Web;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using Serilog;
using SimpleVAS_WebScraper.GeneralFuncations;
using SimpleVAS_WebScraper.Contracts;

using SimpleWebScrapper.Engine;

namespace SimpleVAS_WebScraper
{
    public class Program
    {
        static void Main(string[] args)
        {
            IUnityContainer container = new UnityContainer()
                .RegisterType<IConfigurationFactory, ConfigurationFactory>()
                .RegisterType<ISimpleWebScrapperEngine, SimpleWebScrapperEngine>();
                
            container.RegisterType<ILogger>(new ContainerControlledLifetimeManager(), new InjectionFactory((ctr, type, name) =>
            {
                ILogger log = new LoggerConfiguration()
                    .WriteTo.Console() //Your serilog config here
                    .WriteTo.ColoredConsole()
                     .MinimumLevel.Debug()
                    .WriteTo.RollingFile("c://vms//log-{Date}.txt")
                    .CreateLogger();

                return log;
            }));

            var simpleEngine = container.Resolve<ISimpleWebScrapperEngine>();
            // simpleEngine.GetCustomerPromotionStatus();
            //simpleEngine.UncheckUnlimitedVas();
            simpleEngine.ProcessDarkTVMigration();
            Console.WriteLine("ALL DONE !!");
        }




        static ArrayList RemoveAlreadDone(ArrayList org, ArrayList alreaduDone)
        {
            ArrayList newList = new ArrayList();
            bool found = false;
            foreach(var orgUser in org)
            {
                found = false;
                foreach(var alreadyUser in alreaduDone)
                {
                    
                    if(string.Equals(alreadyUser, orgUser))
                    {
                        found = true;
                        break;
                    }
                }

                if (found == false)
                    newList.Add(orgUser);
            }

            return newList;
        }




    }
}
