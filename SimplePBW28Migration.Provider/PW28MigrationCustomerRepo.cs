﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Serilog;
using CsvHelper;
using CsvHelper.Configuration;
using SimplePBW28Migration.Provider.Configuration;
using SimpleWebScrapper.Common;

namespace SimplePBW28Migration.Provider
{
   public class Pw28MigrationCustomerRepo : CustomerRepository
    {
        public List<Customer> CustomerList;
        
        readonly string _inputFile;
  
        public Pw28MigrationCustomerRepo(ILogger logger):base(logger)
        {
          
            var config = ConfigurationManager.GetSection("pbw28ProviderSetting") as ProviderConfigurationSection;
            if (config != null)
            {
                _inputFile = config.RepositorySetting.InputFile;
            }
            else
            {
                throw  new Exception("Provider Setting is missing , Please add setting in AppSetting for section 'pbw28ProviderSetting'");
            }
          //  _logger = logger;
            Initialize();
        }

        public override List<Customer> Customers => CustomerList;

       public override void Initialize()
        {
             CustomerList = LoadCsvFileforToday();
            _logger.Information("Total Repo Customers [{0}]", CustomerList.Count);
        }

        private List<Customer> LoadCsvFileforToday()
        {
            try
            {
                var fileName = _inputFile;
                var customerList = new List<Customer>();
                using (var fs = File.OpenRead(fileName))
                using (var reader = new StreamReader(fs))
                {

                    CsvConfiguration config = new CsvConfiguration {HasHeaderRecord = false};
                    var csv = new CsvReader(reader, config);
                    while (csv.Read())
                    {
                        var customerId = csv.GetField<string>(0); //cusotmerId
                        var billDate = csv.GetField<string>(3); // billdate
                        var todayDate = DateTime.Now.Day.ToString();
                        var tempCust = new Customer
                        {
                            CustomerB1Number = customerId.Trim(),
                            BillDate = billDate
                        };
                        if (billDate == todayDate)
                            customerList.Add(tempCust);
                    }
                }
                return customerList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Load Input File Error");
                throw new Exception("Input File Read Error");
            }

        }

      // public ILogger _logger { get; set; }
   }

 
}
