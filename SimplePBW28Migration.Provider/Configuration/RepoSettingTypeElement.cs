using System.Configuration;

namespace SimplePBW28Migration.Provider.Configuration
{
    public class RepoSettingTypeElement : ConfigurationElement
    {
        [ConfigurationProperty("inputFile", IsRequired = false, IsKey = true)]
        public string InputFile
        {
            get { return (string)base["inputFile"]; }
            set { base["inputFile"] = value; }
        }

    }
}