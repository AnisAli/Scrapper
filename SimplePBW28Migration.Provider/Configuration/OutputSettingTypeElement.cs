using System.Configuration;

namespace SimplePBW28Migration.Provider.Configuration
{
    public class OutputSettingTypeElement : ConfigurationElement
    {
        [ConfigurationProperty("successFilePath", IsRequired = true, IsKey = true)]
        public string SuccessFilePath
        {
            get { return (string)base["successFilePath"]; }
            set { base["successFilePath"] = value; }
        }

        [ConfigurationProperty("failureFilePath", IsRequired = true, IsKey = true)]
        public string FailureFilePath
        {
            get { return (string)base["failureFilePath"]; }
            set { base["failureFilePath"] = value; }
        }

        [ConfigurationProperty("dateFileFormat", IsRequired = false, IsKey = true)]
        public string DateFileFormat
        {
            get { return (string)base["dateFileFormat"]; }
            set { base["dateFileFormat"] = value; }
        }
    }
}