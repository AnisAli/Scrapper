﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration;
using Serilog;
using SimplePBW28Migration.Provider.Configuration;
using SimpleWebScrapper.Common;
namespace SimplePBW28Migration.Provider
{
    // here wer are handling all success and failure in text file , we can use Database as well or any other king of method.. we read configuration from appconfig using ProviderSetting
    public class PBW28EngineOutputHandler:IEngineOutputHandler
    {
        private readonly string _sucessFile;
        private readonly string _failureFile;
        private readonly ILogger _logger;

        #region ctor
        public PBW28EngineOutputHandler(ILogger logger)
        {
            _logger = logger;
            var config = ConfigurationManager.GetSection("pbw28ProviderSetting") as ProviderConfigurationSection;
            if (config != null)
            {
                if (config.OutputSetting.DateFileFormat != null)
                {

                    var format = DateTime.Now.ToString(config.OutputSetting.DateFileFormat);
                   
                    _sucessFile = config.OutputSetting.SuccessFilePath + "success_" + format +
                                 ".txt";
                    _failureFile = config.OutputSetting.FailureFilePath + "failure" + format +
                                  ".txt";
                }
                else
                {
                    _sucessFile = config.OutputSetting.SuccessFilePath + "success.txt";
                    _failureFile = config.OutputSetting.FailureFilePath + "failure.txt";
                }
                   
            }
            else
            {
                throw new Exception("Provider Setting is missing , Please add setting in AppSetting for section 'pbw28ProviderSetting'");
            }
            
        }
        #endregion

        public  bool Sucess(Customer customer)
        {
            try
            {
                using (var txtWriter = new StreamWriter(_sucessFile, append: true))
                {
                    var csv = new CsvWriter(txtWriter);
           
                     csv.WriteField(customer.CustomerB1Number);
                    csv.NextRecord();
              
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Not able to write Success output - " + ex.Message);
               
            }
           
        }

        public bool Failure(Customer customer, string reason)
        {
            try
            {
                using (var txtWriter = new StreamWriter(_failureFile, append: true))
                {
                    var csv = new CsvWriter(txtWriter);
                    csv.WriteField(customer.CustomerB1Number + "- " + reason); csv.NextRecord();
                  //  csv.WriteField(reason);
                    //csv.NextRecord();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Not able to write Failure output - " + ex.Message);

            }
        }

        public bool Failure(Customer customer, Exception exception)
        {
            try
            {
                using (var txtWriter = new StreamWriter(_failureFile, append: true))
                {
                    var csv = new CsvWriter(txtWriter);
                    csv.WriteField(customer.CustomerB1Number + " - " + exception.Message);
                    csv.NextRecord();
                  //  csv.WriteField(exception.Message);
                   // csv.NextRecord();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Not able to write Failure output - " + ex.Message);

            }
        }

        public List<Customer> GetSuccessCustomerList()
        {
            return ReadCustomerList(_sucessFile);
        }

        public List<Customer> GetFailureCustomerList()
        {
            return ReadCustomerList(_failureFile);
        }

        #region private methods
        private List<Customer> ReadCustomerList(string fileName)
        {
            try
            {

                if (!File.Exists(fileName))
                        return null;
                
                var customerList = new List<Customer>();
                using (var fs = File.OpenRead(fileName))
                using (var reader = new StreamReader(fs))
                {

                    CsvConfiguration config = new CsvConfiguration { HasHeaderRecord = false };
                    var csv = new CsvReader(reader, config);
                    while (csv.Read())
                    {
                        var customerId = csv.GetField<string>(0); //cusotmerId
                        var tempCust = new Customer
                        {
                            CustomerB1Number = customerId.Trim()
                        };
                       customerList.Add(tempCust);
                    }
                }
                return customerList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Load Input File Error {0}", fileName);
                throw new Exception("OutputHandler - Read Error");
            }
        }

        public bool Sucess(Customer customer, string note)
        {
            throw new NotImplementedException();
        }

        public bool LogHtmlFile(Customer customer, string HtmlPage)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
